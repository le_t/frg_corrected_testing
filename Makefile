## core makefile
#--------------------------------------General------------------------------------------
CXX     := g++ # This is the main compiler
SRCDIR  := src
HEADDIR := include
BUILDDIR := build
TARGET  := bin/run


#--------------------------------------Sources and header files------------------------------------------
SRCEXT  := cpp
HEADEXT := h
SOURCES := $(shell ls -1 $(SRCDIR)/*.$(SRCEXT))
HEADERS := $(shell ls -1 $(HEADDIR)/*.$(HEADEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))

#--------------------------------------Settings for fRG flow------------------------------------------

CFLAGS += -D COMPRESS #                 Compress HDF5 datafile 
#CFLAGS += -D POSTPROC_LAM
# -- Choose cuttoff scheme
CFLAGS += -D INT_FLOW #                 Interaction Flow
# -- Higher order corrections
#CFLAGS += -D TWOLOOP #                 Use twoloop corrections
CFLAGS += -D UNIFORM_GRID
CFLAGS += -D LOCAL
#CFLAGS += -D FIRST_NN
#--------------------------------------Compiler settings------------------------------------------

CFLAGS += -m64 -std=c++11 -D OPENMP -fopenmp #                       General compiler flags
CFLAGS += -D BOOST_DISABLE_ASSERTS #                            Disable boost assert checks
CFLAGS += -D NDEBUG #                                            Put in case of no debug
RUNFLAGS := -O3 #                                               Compiler flags for quick compile and run
H5LIB := -D_LARGEFILE64_SOURCE -D_LARGEFILE_SOURCE -Wall -lhdf5_hl_cpp -lhdf5_cpp -lhdf5_hl -lhdf5 -lpthread -lz -ldl -lm -Wl,-rpath -Wl,/usr/lib64
LIB := -lgsl -lgslcblas -lm $(H5LIB) -fopenmp -lfftw3  #                Library flags
INC += -I include                       #Additional include paths
INC += -I$(EIGEN_ROOTDIR)/include/eigen3                #Additional include paths
INC += -I$(BOOST_INC_DIR)
#INC += -L/usr/lib64    # needed when compiled on 40coreold

#common, safe optimizations
CFLAGS_COMMON := $(CFLAGS) -O3 -malign-data=cacheline
#architectural and potentially unsafe optimizations
ARCHOPT := -march=broadwell -mfma -ffunction-sections -fomit-frame-pointer
#unsafe optimizations, potent but reliant on commutativity and associativity
UNSAFE := -ffast-math -funroll-loops

#optimized libraries
OPTLIB := -lopenblas -lm $(H5LIB) -fopenmp -lfftw3 


#--------------------------------------Targets ------------------------------------------
$(TARGET): $(OBJECTS)
	@echo " Linking..."
	@mkdir -p bin
	@mkdir -p dat
	@echo " $(CXX) $^ -o $(TARGET) $(LIB)"; $(CXX) $^ -o $(TARGET) $(LIB)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT) $(HEADERS)
	@mkdir -p $(BUILDDIR)
	@echo " $(CXX) $(CFLAGS) $(INC) -c -o $@ $<"; $(CXX) $(CFLAGS) $(INC) -c -o $@ $<

run:    CFLAGS += $(RUNFLAGS)
run:    $(TARGET)

##########################################################################################
##### Optimization compilation options
##########################################################################################
# 1. Baseline: Original code and original compiler options.
baseline: $(TARGET)

# 2. Extended baseline.
extended_baseline: TARGET := bin/run_extended_baseline
extended_baseline: run


global_safe: TARGET := bin/run_global_safe
global_safe: LIB := $(OPTLIB)
global_safe: CFLAGS := $(CFLAGS_COMMON)
global_safe: baseline

# 10. 9 - Without call by reference but with const and with temporaries. Also mymath.h changed.
reference:	SRCDIR := src
reference:	HEADDIR := include
reference:	TARGET := bin/run_reference
reference:	CFLAGS := $(CFLAGS_COMMON)
reference:	INC := -I $(HEADDIR) -I$(EIGEN_ROOTDIR)/include/eigen3 -I$(BOOST_INC_DIR)
reference:	LIB := $(OPTLIB)
reference:
	$(CXX) $(CFLAGS) $(INC) $(ARCHOPT) -c $(SRCDIR)/def.cpp
	$(CXX) $(CFLAGS) $(INC) $(ARCHOPT) -c $(SRCDIR)/frg.cpp
	$(CXX) $(CFLAGS) $(INC) $(ARCHOPT) -c $(SRCDIR)/grid.cpp
	$(CXX) $(CFLAGS) $(INC) $(ARCHOPT) -c $(SRCDIR)/mymath.cpp
	$(CXX) $(CFLAGS) $(INC) $(ARCHOPT) -c $(SRCDIR)/observables.cpp
	$(CXX) $(CFLAGS) $(INC) $(ARCHOPT) -c $(SRCDIR)/observer.cpp
	$(CXX) $(CFLAGS) $(INC) $(ARCHOPT) -c $(SRCDIR)/ode.cpp
	$(CXX) $(CFLAGS) $(INC) -c $(SRCDIR)/params.cpp
	$(CXX) $(CFLAGS) $(INC) $(ARCHOPT) -c $(SRCDIR)/projection.cpp
	$(CXX) $(CFLAGS) $(INC) -c $(SRCDIR)/rhs.cpp
	$(CXX) $(CFLAGS) $(INC) $(ARCHOPT) -c $(SRCDIR)/state.cpp
	$(CXX) $(CFLAGS) $(INC) $(ARCHOPT) -c $(SRCDIR)/symmetries.cpp
	$(CXX) $(CFLAGS) $(INC) $(ARCHOPT) -c $(SRCDIR)/symmetries_lattice.cpp
	$(CXX) $(CFLAGS) $(INC) $(ARCHOPT) -c $(SRCDIR)/translate.cpp
	@echo "Linking .."
	$(CXX) def.o frg.o grid.o mymath.o observables.o observer.o ode.o params.o projection.o rhs.o state.o symmetries.o symmetries_lattice.o translate.o -o $(TARGET) $(LIB)
	rm *.o
	

###################################################################################


clean:
	@echo " Cleaning..."; 
	@echo " $(RM) -r $(BUILDDIR) $(TARGET)"; $(RM) -r $(BUILDDIR) $(TARGET)
	@echo " $(RM) -r $(BUILDDIR) $(TARGET_COMMON)"; $(RM) -r $(BUILDDIR) $(TARGET_COMMON)

.PHONY: clean
