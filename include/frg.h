
/*******************************************************************************************//** @file
 *  		
 * 	file: 		frg.h
 * 	contents:  	Definition of Green function ( i.e. system ) and fRG specific functions 
 * 
 ****************************************************************************************************/


#pragma once

#include <def.h>
#include <params.h>

#ifdef NO_MOMENTA
MatQN Gam( double w );				///< Hybridization function according to the compile flags in makefile
#endif

MatQN G0inv( double w, double kx, double ky);					///< Inverse non-interacting Greens function ( scale-independent! )

MatQN G( double w, double kx, double ky, double Lam, const MatQN& selfEn );	///< Scale-dependent Greens function, introduce regulator here!
MatQN G_latt( double w, double kx, double ky, double Lam, const MatQN& selfEn );	///< Scale-dependent Greens function, introduce regulator here!
MatQN S( double w, double kx, double ky, double Lam, const MatQN& selfEn ); 	///< Single-scale propagator 
//MatQN G( double w, double kx, double ky,double kz, double Lam);
//MatQN S( double w, double kx, double ky,double kz, double Lam);
dcomplex vert_bare( int s1_in, int s2_in, int s1_out, int s2_out );  	///< Initial vertex values in Orbital basis
dcomplex vert_bare( const idx_2p_t& idx );				///< Return initial vertex value for given index object

dcomplex Sig_init( const idx_1p_t& idx );					///< Return initial self-energy value for given index object
dcomplex phi_init( const idx_phi_t& idx ); 
dcomplex P_init( const idx_P_t& idx ); 
dcomplex chi_init( const idx_chi_t& idx );				///< Return initial vertex value for chiasch functions

dcomplex tri_bare( int n_in, int n_out, int s1_in, int s2_in, int s1_out, int s2_ou ); 
dcomplex tri_init( const idx_tri_t& idx ); 
dcomplex asytri_init( const idx_asytri_t& idx ); 
dcomplex susc_init( const idx_susc_t& idx );				///< Return initial vertex value for chiasch functions
dcomplex susc_postproc_init( const idx_susc_postproc_t& idx );				///< Return initial vertex value for chiasch functions

dcomplex Sig_zero( const idx_1p_t& idx );				///< returns 0
dcomplex phi_zero( const idx_phi_t& idx ); 
dcomplex P_zero( const idx_P_t& idx ); 
dcomplex chi_zero( const idx_chi_t& idx );				
dcomplex tri_zero( const idx_tri_t& idx ); 
dcomplex asytri_zero( const idx_asytri_t& idx ); 
dcomplex susc_zero( const idx_susc_t& idx );				

dcomplex asympt_SG_pp( int W, double Lam );	///< Give estimate for the integral 1/2/PI * S(W/2-w-1,Lam) * G(W/2+w,Lam) for the outside region N \ [ -POS_INT_RANGE, POS_INT_RANGE )
dcomplex asympt_SGpGS_pp( int W, double Lam );	///< Give estimate for the integral 1/2/PI * [ S(w-W/2,Lam) * G(w+W/2,Lam) + S(w-W/2,Lam) * G(w+W/2,Lam) ] for the outside region N \ [ -POS_INT_RANGE, POS_INT_RANGE )

dcomplex asympt_SG_ph( int W, double Lam );	///< Give estimate for the integral 1/2/PI * S(W/2-w-1,Lam) * G(W/2+w,Lam) for the outside region N \ [ -POS_INT_RANGE, POS_INT_RANGE )
dcomplex asympt_SGpGS_ph( int W, double Lam );	///< Give estimate for the integral 1/2/PI * [ S(w-W/2,Lam) * G(w+W/2,Lam) + S(w-W/2,Lam) * G(w+W/2,Lam) ] for the outside region N \ [ -POS_INT_RANGE, POS_INT_RANGE )

dcomplex asympt_GG_pp( int W, double Lam );	///< Give estimate for the integral 1/2/PI * G(W/2-w-1,Lam) * G(W/2+w,Lam) for the outside region N \ [ -POS_INT_RANGE, POS_INT_RANGE )
dcomplex asympt_GG_ph( int W, double Lam );	///< Give estimate for the integral 1/2/PI * G(w-W/2,Lam) * G(w+W/2,Lam) for the outside region N \ [ -POS_INT_RANGE, POS_INT_RANGE )

inline double w_val( int w_idx )					///< Return value of fermionic matsubara frequency for a given index
{
   return PI / BETA * ( 2 * w_idx + 1 ); 
}

inline double W_val( int W_idx )					///< Return value of bosonic matsubara frequency for a given index
{
   return 2.0 * PI / BETA * W_idx; 
}
