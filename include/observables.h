
/*******************************************************************************************//** @file
 *  		
 * 	file: 		observalbes.h
 * 	contents:  	Define functions to calculate observables from results
 * 
 ****************************************************************************************************/


#pragma once

#include <state.h>
#include <const.h>

#ifdef POSTPROC_LAM
typedef double (*obs_func_t )( const state_t&, const postproc_t&, double Lam); 	///< Symmetry function that acts on an idx_2p_t object and alters it 
#else
typedef double (*obs_func_t )( const state_t&, double Lam); 	///< Symmetry function that acts on an idx_2p_t object and alters it 
#endif
typedef std::vector< std::pair<  std::string, obs_func_t > >	flow_obs_func_lst_t; 

extern flow_obs_func_lst_t flow_obs_func_lst; 	///< List to contain all observable functions to be tracked during the flow


double jos_curr( const state_t& state_vec ); 	///< Calculate the average occupation number summing the diagonal part of all quantum numbers
double get_max_cpl( const state_t& state_vec ); 	///< Find the coupling with the largest absolute value. Returns the absolute value and the position
double get_max_susc_cpl( const state_t& state_vec ); 	///< Find the coupling with the largest absolute value. Returns the absolute value and the position

#ifdef POSTPROC_LAM
double eff_mass( const state_t& state_vec, const postproc_t& postproc_vec, double Lam ); 		///< Return effective mass based on self-energy slope at frequency zero
double err_eff_mass( const state_t& state_vec, const postproc_t& postproc_vec, double Lam ); 	///< Return an error estimate on the effective mass by using the first and third matsubara frequency
double filling( const state_t& state_vec, const postproc_t& postproc_vec, double Lam ); 	///< Calculate the average occupation number summing the diagonal part of all quantum numbers
double susc_postproc_sc_00( const state_t& state_vec, const postproc_t& postproc_vec, double Lam );
double susc_postproc_sc_0pi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam );
double susc_postproc_sc_pipi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam );
double susc_postproc_d_00( const state_t& state_vec, const postproc_t& postproc_vec, double Lam );
double susc_postproc_d_0pi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam );
double susc_postproc_d_pipi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam );
double susc_postproc_m_00( const state_t& state_vec, const postproc_t& postproc_vec, double Lam );
double susc_postproc_m_0pi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam );
double susc_postproc_m_pipi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam );
double susc_flow_sc_00( const state_t& state_vec, const postproc_t& postproc_vec, double Lam );
double susc_flow_sc_0pi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam );
double susc_flow_sc_pipi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam );
double susc_flow_d_00( const state_t& state_vec, const postproc_t& postproc_vec, double Lam );
double susc_flow_d_0pi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam );
double susc_flow_d_pipi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam );
double susc_flow_m_00( const state_t& state_vec, const postproc_t& postproc_vec, double Lam );
double susc_flow_m_0pi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam );
double susc_flow_m_pipi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam );
#else
double eff_mass( const state_t& state_vec, double Lam ); 		///< Return effective mass based on self-energy slope at frequency zero
double err_eff_mass( const state_t& state_vec, double Lam ); 	///< Return an error estimate on the effective mass by using the first and third matsubara frequency
double filling( const state_t& state_vec, double Lam ); 	///< Calculate the average occupation number summing the diagonal part of all quantum numbers
#endif
