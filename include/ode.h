
/*******************************************************************************************//** @file
 *  		
 * 	file: 		ode.h
 * 	contents:  	Adaptive stepper implementation that aborts for large coupling ( see const.h )
 * 			Also define norm for state_t
 * 
 ****************************************************************************************************/


#pragma once

//#include <boost/numeric/odeint/integrate/integrate_const.hpp>
#include <boost/numeric/odeint/integrate/integrate_adaptive.hpp>
#include <boost/numeric/odeint/integrate/detail/integrate_adaptive.hpp>
#include <boost/numeric/odeint.hpp>
#include <def.h>

namespace boost {
namespace numeric {
namespace odeint {
namespace detail {
/************************EULER ODE SOLVER **************************************
 integrate_const without observer calls 
template< class System >
   error_stepper::do_step( System sys , state_type &x , time_type t , time_type dt )
   {
          state_type xerr;
	      do_step( sys , x , t , dt , xerr );
   }

 template<typename Stepper, typename System, typename State, typename Time> 
 size_t integrate_const(
       Stepper stepper, System system,
       State & start_state, Time start_time, 
       Time end_time, Time dt
       );
*******************************************************************************/

 /*
 * Changed integrate adaptive to consider abort
 */


template< class Stepper, class System, class State, class Time, class Observer >
bool integrate_adaptive_check(
	Stepper stepper, System system, State &start_state,
	Time &start_time, Time end_time, Time &dt,
	Observer observer //, controlled_stepper_tag
)
{
    typename odeint::unwrap_reference< Observer >::type &obs = observer;

    const size_t max_attempts = 1000;
    const char *error_string = "Integrate adaptive : Maximal number of iterations reached. A step size could not be found.";
    size_t count = 0;
#ifdef MPI_PARALLEL
    int nranks; MPI_Comm_size(MPI_COMM_WORLD, &nranks);
    int myrank; MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    if(myrank==0){
#endif
    while( less_with_sign( start_time, end_time, dt ) )
    {
	if( !obs( start_state, start_time ) )
	{
	   std::cout << std::endl << " !!!! Vertex exceeded MAX_COUPLING at scale " << start_time << ", ODE solver stopped !!!! " << std::endl << std::endl; 
           double break_time=end_time+dt;
#ifdef MPI_PARALLEL
           MPI_Bcast(&break_time,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
#endif
	   return 0; 
	}	   
	if( less_with_sign( end_time, start_time + dt, dt ) )
	{
	    dt = end_time - start_time;
	}

	size_t trials = 0;
	controlled_step_result res = success;
	do
	{
	    std::cout << "trial is " << trials << std::endl;
	    res = stepper.try_step( system, start_state, start_time, dt );
	    ++trials;
	}
	while( ( res == fail ) && ( trials < max_attempts ) );
	if( trials == max_attempts ) throw std::overflow_error( error_string );

	++count;
    }
#ifdef MPI_PARALLEL
    double break_time=end_time+dt;
    MPI_Bcast(&break_time,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
    }
    else{
    while( less_with_sign( start_time, end_time, dt )  )  //TODO: hoping that dt does not change sign in one specific flow
    {
       double Lam_w;
       State  state_w, dfdl_w;
       MPI_Bcast(&Lam_w,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
       if(!less_eq_with_sign( Lam_w, end_time, dt)) {
	  std::cout << "rank " << myrank << " is exiting the while loop " << std::endl;
	  break;} // this is called if the last sending of master outside the while loop was executed
       MPI_Bcast(&state_w.gf_Sig()(0)	 	, state_w.gf_Sig().num_elements()      , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
       MPI_Bcast(&state_w.gf_phi_pp()(0) 	, state_w.gf_phi_pp().num_elements()   , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
       MPI_Bcast(&state_w.gf_phi_ph()(0) 	, state_w.gf_phi_ph().num_elements()   , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
       MPI_Bcast(&state_w.gf_phi_xph()(0)	, state_w.gf_phi_xph().num_elements()  , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
       MPI_Bcast(&state_w.gf_P_pp()(0)	 	, state_w.gf_P_pp().num_elements()     , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
       MPI_Bcast(&state_w.gf_P_ph()(0)	 	, state_w.gf_P_ph().num_elements()     , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
       MPI_Bcast(&state_w.gf_P_xph()(0)	 	, state_w.gf_P_xph().num_elements()    , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
       MPI_Bcast(&state_w.gf_chi_pp()(0) 	, state_w.gf_chi_pp().num_elements()   , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
       MPI_Bcast(&state_w.gf_chi_ph()(0) 	, state_w.gf_chi_ph().num_elements()   , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
       MPI_Bcast(&state_w.gf_chi_xph()(0)	, state_w.gf_chi_xph().num_elements()  , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
       MPI_Bcast(&state_w.gf_tri_sc()(0) 	, state_w.gf_tri_sc().num_elements()   , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
       MPI_Bcast(&state_w.gf_tri_d()(0)	 	, state_w.gf_tri_d().num_elements()    , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
       MPI_Bcast(&state_w.gf_tri_m()(0)	 	, state_w.gf_tri_m().num_elements()    , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
       MPI_Bcast(&state_w.gf_susc_sc()(0)	, state_w.gf_susc_sc().num_elements()  , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
       MPI_Bcast(&state_w.gf_susc_d()(0) 	, state_w.gf_susc_d().num_elements()   , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
       MPI_Bcast(&state_w.gf_susc_m()(0) 	, state_w.gf_susc_m().num_elements()   , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
       MPI_Bcast(&state_w.gf_asytri_sc()(0)	, state_w.gf_asytri_sc().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
       MPI_Bcast(&state_w.gf_asytri_d()(0)	, state_w.gf_asytri_d().num_elements() , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
       MPI_Bcast(&state_w.gf_asytri_m()(0)	, state_w.gf_asytri_m().num_elements() , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
       system(state_w,dfdl_w,Lam_w);
    }
    }
    if(myrank==0){
#endif
    obs( start_state, start_time );
    std::cout << " ODE solver finished with " << count << " integration steps." << std::endl << std::endl; 
#ifdef MPI_PARALLEL
    }
#endif
    return 1;
}

} // Namespace detail

template< class Stepper, class System, class State, class Time, class Observer >
bool integrate_adaptive_check(
        Stepper stepper, System system, State &start_state,
        Time start_time, Time end_time, Time dt,
        Observer observer )
{
    return detail::integrate_adaptive_check(
            stepper, system, start_state,
            start_time, end_time, dt,
            observer ); //, typename Stepper::stepper_category() );
}

} // Namespace odeint
} // Namespace numeric
} // Namespace boost
