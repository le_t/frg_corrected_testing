
/*******************************************************************************************//** @file
 *  		
 * 	file: 		output.h
 * 	contents:  	Functions to write output to hdf5 file
 * 
 ****************************************************************************************************/


#pragma once

#include <H5Cpp.h>
#include <rhs.h>

void init( H5::H5File& file );			///< Initialize with time and date
void write_config( H5::H5File& file );		///< Write configuration to output file
void write_params( H5::H5File& file );		///< Write parameters to output file
void write_vert_tensor( H5::H5File& file, const state_t& state_vec );  	///<	Write 1PI two-particle vertex to output file
void write_Sig_tensor( H5::H5File& file, const state_t& state_vec );		///<	Write self-energy to output file
void write_phi_func( H5::H5File& file, const state_t& state_vec ); 		///< 	Write phi functions to output file
void write_vert_func( H5::H5File& file, const state_t& state_vec ); 		///< 	Write phi functions to output file
void write_chi_func( H5::H5File& file, const state_t& state_vec );  	///<	Write Karrasch functions to output file
void write_P_func( H5::H5File& file, const state_t& state_vec ); 		///<	Write P functions to output file
void write_susc_func( H5::H5File& file, const state_t& state_vec );  	///<	Write Karrasch functions to output file
void write_tri_func( H5::H5File& file, const state_t& state_vec ); 		///<	Write P functions to output file
void write_R_func( H5::H5File& file, const state_t& state_vec ); 		///<	Write R functions to output file
void write_Giw_tensor( H5::H5File& file, const state_t& state_vec );	///<	Write Green function to output file
void write_observables( H5::H5File& file, const state_t& state_vec );	///<	Write observables to output file
void write_flow_observables( std::vector<   std::pair< std::string, std::vector<double> >    >&   flow_obs_lst, H5::H5File& file );	///<	Write observables tracked during the flow
void write_bubble( H5::H5File& file, const gf_bubble_mat_t bubble_pp, const gf_bubble_mat_t bubble_ph );	//< 	Write Bubble (pp and ph)  (uncomment part in ode.cpp)
void write_projection( H5::H5File& file );	//< 	Write projection uncomment here, in output.cpp, include state.cpp in output.cpp, write function in ode.cpp
void write_Gfunc( H5::H5File& file, const gf_1p_mat_real_t Gvec_real, const gf_1p_mat_real_t Svec_real );	//< 	Write Greens function (G and S) in real space (uncomment part in ode.cpp)
