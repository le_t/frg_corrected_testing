
/*******************************************************************************************//** @file
 *  		
 * 	file: 		rhs.h
 * 	contents:  	Functor specifying the calculation of the rhs of the flow equation
 * 
 ****************************************************************************************************/


#pragma once

#include <state.h>
#include <grid.h>
#include <symmetry_group.h>
#include <symmetries.h>
#include <fftw3.h>

class rhs_t 		///< Functor to specify the rhs calculation for both the self-energy and the vertex
{
   public:
      void operator() ( const state_t& state_vec, state_t &dfdl, const double Lam ); 	///< Overload call operator to calculate full rhs

      /********************* RHS evaluation ********************/
      static dcomplex eval_rhs_Sig( const idx_1p_t& se_idx, const state_t& state_vec, const gf_1p_mat_t& Svec, double Lam ); 					///< Calculate rhs of self-energy flow for specific index
      static dcomplex eval_rhs_Sig_corr1( const idx_1p_t& se_idx, const state_t& dfdl, const gf_1p_mat_t& Gvec, double Lam ); 					///< Calculate rhs of self-energy flow for specific index
      static dcomplex eval_rhs_Sig_corr2( const idx_1p_t& se_idx, const state_t& state_vec, const gf_1p_mat_t& GdSigG, double Lam ); 					///< Calculate rhs of self-energy flow for specific index
      
      static MatReal eval_Gvec_real( const idx_1p_mat_real_t& idx, const gf_1p_mat_t& Gvec, fftw_plan p_g, double Lam );		///< Calculate particle-hole diagram for the flow
      static MatReal eval_Svec_real( const idx_1p_mat_real_t& idx, const gf_1p_mat_t& Svec, fftw_plan p_s, double Lam );		///< Calculate particle-hole diagram for the flow

      static MatPatch eval_diag_bubble_pp( const idx_bubble_mat_t& idx, const gf_1p_mat_real_t& Svec_real, const gf_1p_mat_real_t& Gvec_real, fftw_plan p_b, double Lam );		///< Calculate particle-hole diagram for the flow
      static MatPatch eval_diag_bubble_ph( const idx_bubble_mat_t& idx, const gf_1p_mat_real_t& Svec_real, const gf_1p_mat_real_t& Gvec_real, fftw_plan p_b, double Lam );		///< Calculate particle-hole diagram for the flow
      static MatPatch eval_diag_bubble_GG_pp( const idx_bubble_mat_t& idx, const gf_1p_mat_real_t& Gvec_real, fftw_plan p_b, double Lam );		///< Calculate particle-hole diagram for the flow
      static MatPatch eval_diag_bubble_GG_ph( const idx_bubble_mat_t& idx, const gf_1p_mat_real_t& Gvec_real, fftw_plan p_b, double Lam );		///< Calculate particle-hole diagram for the flow
      
      
      static dcomplex eval_diag_pp( const idx_phi_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_pp, double Lam );		///< Calculate particle-particle diagram for the flow
      static dcomplex eval_diag_ph( const idx_phi_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_ph, double Lam );		///< Calculate particle-hole diagram for the flow
      static dcomplex eval_diag_xph( const idx_phi_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_ph, double Lam );		///< Calculate particle-hole diagram for the flow
      
      static dcomplex eval_diag_tri_sc( const idx_tri_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_pp, double Lam );		///< Calculate particle-particle diagram for the flow
      static dcomplex eval_diag_tri_d( const idx_tri_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_ph, double Lam );		///< Calculate particle-hole diagram for the flow
      static dcomplex eval_diag_tri_m( const idx_tri_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_ph, double Lam );		///< Calculate particle-hole diagram for the flow
      
      static dcomplex eval_diag_susc_sc( const idx_susc_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_pp, double Lam );		///< Calculate particle-particle diagram for the flow
      static dcomplex eval_diag_susc_d( const idx_susc_t& idx, const state_t&  state_vec, const gf_bubble_mat_t& bubble_ph, double Lam );		///< Calculate particle-hole diagram for the flow
      static dcomplex eval_diag_susc_m( const idx_susc_t& idx, const state_t&  state_vec, const gf_bubble_mat_t& bubble_ph, double Lam );		///< Calculate particle-hole diagram for the flow
      
      static dcomplex eval_diag_pp_C( const idx_phi_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_pp, double Lam );		///< Calculate particle-particle diagram for the flow
      static dcomplex eval_diag_ph_C( const idx_phi_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam );		///< Calculate particle-hole diagram for the flow
      static dcomplex eval_diag_xph_C( const idx_phi_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam );		///< Calculate particle-hole diagram for the flow
      
      static dcomplex eval_diag_tri_sc_C( const idx_tri_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_pp, double Lam );		///< Calculate particle-particle diagram for the flow
      static dcomplex eval_diag_tri_d_C( const idx_tri_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam );		///< Calculate particle-hole diagram for the flow
      static dcomplex eval_diag_tri_m_C( const idx_tri_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam );		///< Calculate particle-hole diagram for the flow
      
      static dcomplex eval_diag_susc_sc_C( const idx_susc_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_pp, double Lam );		///< Calculate particle-particle diagram for the flow
      static dcomplex eval_diag_susc_d_C( const  idx_susc_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam );		///< Calculate particle-hole diagram for the flow
      static dcomplex eval_diag_susc_m_C( const  idx_susc_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam );		///< Calculate particle-hole diagram for the flow
      
      static dcomplex eval_diag_pp_R( const idx_phi_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_pp, double Lam );		///< Calculate particle-particle diagram for the flow
      static dcomplex eval_diag_ph_R( const idx_phi_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam );		///< Calculate particle-hole diagram for the flow
      static dcomplex eval_diag_xph_R( const idx_phi_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam );		///< Calculate particle-hole diagram for the flow

      static dcomplex eval_diag_tri_sc_R( const idx_tri_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_pp, double Lam );		///< Calculate particle-particle diagram for the flow
      static dcomplex eval_diag_tri_d_R( const idx_tri_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam );		///< Calculate particle-hole diagram for the flow
      static dcomplex eval_diag_tri_m_R( const idx_tri_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam );		///< Calculate particle-hole diagram for the flow
      
      static dcomplex eval_diag_pp_L( const idx_phi_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_pp, double Lam );		///< Calculate particle-particle diagram for the flow
      static dcomplex eval_diag_ph_L( const idx_phi_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam );		///< Calculate particle-hole diagram for the flow
      static dcomplex eval_diag_xph_L( const idx_phi_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam );		///< Calculate particle-hole diagram for the flow
      

      static dcomplex eval_diag_susc_postproc_sc( const idx_susc_postproc_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_GG_pp, double Lam );		///< Calculate particle-particle diagram for the flow
      static dcomplex eval_diag_susc_postproc_d(  const idx_susc_postproc_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_GG_ph, double Lam );		///< Calculate particle-hole diagram for the flow
      static dcomplex eval_diag_susc_postproc_m(  const idx_susc_postproc_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_GG_ph, double Lam );		///< Calculate particle-hole diagram for the flow
   
      static symmetry_grp_t<MatReal,3> symm_grp_Gvec_real;  	///< symmetry group used inside rhs and in observer.cpp
      
      static symmetry_grp_t<MatPatch,8> symm_grp_bubble_pp;
      static symmetry_grp_t<MatPatch,8> symm_grp_bubble_ph;
      
      static symmetry_grp_t<dcomplex,1> symm_grp_susc_postproc; 
   private:
      static symmetry_grp_t<dcomplex,4> symm_grp_sig; 

      static symmetry_grp_t<dcomplex,10> symm_grp_phi_pp; 
      static symmetry_grp_t<dcomplex,10> symm_grp_phi_ph; 
      static symmetry_grp_t<dcomplex,10> symm_grp_phi_xph; 

      static symmetry_grp_t<dcomplex,8> symm_grp_P_pp; 
      static symmetry_grp_t<dcomplex,8> symm_grp_P_ph; 
      static symmetry_grp_t<dcomplex,8> symm_grp_P_xph; 

      static symmetry_grp_t<dcomplex,6> symm_grp_chi_pp; 
      static symmetry_grp_t<dcomplex,6> symm_grp_chi_ph; 
      static symmetry_grp_t<dcomplex,6> symm_grp_chi_xph; 
      
      static symmetry_grp_t<dcomplex,9> symm_grp_tri_sc; 
      static symmetry_grp_t<dcomplex,9> symm_grp_tri_d; 
      static symmetry_grp_t<dcomplex,9> symm_grp_tri_m; 
      
      static symmetry_grp_t<dcomplex,7> symm_grp_asytri_sc; 
      static symmetry_grp_t<dcomplex,7> symm_grp_asytri_d; 
      static symmetry_grp_t<dcomplex,7> symm_grp_asytri_m; 
      
      static symmetry_grp_t<dcomplex,8> symm_grp_susc_sc; 
      static symmetry_grp_t<dcomplex,8> symm_grp_susc_d; 
      static symmetry_grp_t<dcomplex,8> symm_grp_susc_m; 
      
      
      static symmetry_grp_t<dcomplex,10> symm_grp_phi_pp_L; 
      static symmetry_grp_t<dcomplex,10> symm_grp_phi_ph_L; 
      static symmetry_grp_t<dcomplex,10> symm_grp_phi_xph_L; 
      
      static gf_weight_vec_t weight_vec; 
      static gf<double, 2> weight_vec_2d; 
};
