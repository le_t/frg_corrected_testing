
/************************************************************************************************//**
 *  		
 * 	file: 		ode.cpp
 * 	contents:   	main routine, sets up and runs ODE solver, then outputs results
 * 
 ****************************************************************************************************/


#include <ode.h>
#include <boost/numeric/odeint.hpp>
#include <rhs.h>
#include <params.h>
#include <frg.h>
#include <observables.h>
#include <state.h>
#include <observer.h>
#include <projection.h>
#include <grid.h>
#include <complex.h>

#include <cmath> //only debugging
using namespace boost::numeric::odeint;
using namespace std; 

int main ( int argc, char** argv)
{
	
   // ------ MPI parallelization
   int nranks=1; 
   int myrank=0; 
#ifdef MPI_PARALLEL
   MPI_Init(&argc, &argv);
   MPI_Comm_size(MPI_COMM_WORLD, &nranks);
   MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
#endif
   // ------ Set output format

   cout << scientific << setprecision(7); 

   // ------ Read in parameters from file

   if( argc == readIn_lst.size() + 1 )
   {
      for( int i = 0; i < readIn_lst.size(); i++ )
	 *readIn_lst[i] = atof( argv[i+1]);
      update_dep_params(); 
   }
   else
   {
      if(myrank==0)
      cout << "Wrong amount of arguments, using defaults." << endl << endl; 
   }

   // ------ Initialize rhs object and observer list
   if(myrank==0)
   cout << " Initializing ODE solver... " << endl << endl;

   rhs_t rhs;
   
   vector< pair< string, vector<double> >  >   flow_obs_lst;		///< Vector to contain the values of observables tracked during the flow

   // ------ Write initial values

   cout << " Writing initial conditions... " << endl << endl; 
   state_t state_vec; 
   
   // Write initial values for Sig, phi's, P's and chi's
   state_vec.gf_Sig().init( Sig_init ); 

   state_vec.gf_phi_pp().init( phi_init ); 
   state_vec.gf_phi_ph().init( phi_init ); 
   state_vec.gf_phi_xph().init( phi_init ); 

   state_vec.gf_P_pp().init( P_init ); 
   state_vec.gf_P_ph().init( P_init ); 
   state_vec.gf_P_xph().init( P_init ); 

   state_vec.gf_chi_pp().init( chi_init ); 
   state_vec.gf_chi_ph().init( chi_init ); 
   state_vec.gf_chi_xph().init( chi_init );
   
   state_vec.gf_tri_sc().init( tri_init ); 
   state_vec.gf_tri_d().init( tri_init ); 
   state_vec.gf_tri_m().init( tri_init ); 
   
   state_vec.gf_asytri_sc().init( asytri_init ); 
   state_vec.gf_asytri_d().init( asytri_init ); 
   state_vec.gf_asytri_m().init( asytri_init ); 

   state_vec.gf_susc_sc().init( susc_init ); 
   state_vec.gf_susc_d().init( susc_init ); 
   state_vec.gf_susc_m().init( susc_init );

   //--------------------------------------------------------------------------------------------------------
   // Integrate ODE                                                                                                                                                                                                                      

   //----- this part is a debugging help, doing only one euler step ----
   euler< state_t, double, state_t, double, vector_space_algebra > stepper;
   observer_t obs(flow_obs_lst) ;
   double Lam=1;
      uint RKsteps(5);
   //restart profiler
   for(uint i=0; i< RKsteps; ++i){
	if(myrank==0)  {	stepper.do_step(rhs, state_vec, 1, 1.0);
	                        obs( state_vec, 1.0 );} 
#ifdef MPI_PARALLEL
	else{
	double Lam_w;
	state_t  state_w, dfdl_w;
	MPI_Bcast(&Lam_w,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
	MPI_Bcast(&state_w.gf_Sig()(0)	 	, state_w.gf_Sig().num_elements()      , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	MPI_Bcast(&state_w.gf_phi_pp()(0) 	, state_w.gf_phi_pp().num_elements()   , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	MPI_Bcast(&state_w.gf_phi_ph()(0) 	, state_w.gf_phi_ph().num_elements()   , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	MPI_Bcast(&state_w.gf_phi_xph()(0)	, state_w.gf_phi_xph().num_elements()  , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	MPI_Bcast(&state_w.gf_P_pp()(0)	 	, state_w.gf_P_pp().num_elements()     , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	MPI_Bcast(&state_w.gf_P_ph()(0)	 	, state_w.gf_P_ph().num_elements()     , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	MPI_Bcast(&state_w.gf_P_xph()(0)	, state_w.gf_P_xph().num_elements()    , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	MPI_Bcast(&state_w.gf_chi_pp()(0) 	, state_w.gf_chi_pp().num_elements()   , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	MPI_Bcast(&state_w.gf_chi_ph()(0) 	, state_w.gf_chi_ph().num_elements()   , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	MPI_Bcast(&state_w.gf_chi_xph()(0)	, state_w.gf_chi_xph().num_elements()  , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	MPI_Bcast(&state_w.gf_tri_sc()(0) 	, state_w.gf_tri_sc().num_elements()   , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	MPI_Bcast(&state_w.gf_tri_d()(0)	, state_w.gf_tri_d().num_elements()    , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	MPI_Bcast(&state_w.gf_tri_m()(0)	, state_w.gf_tri_m().num_elements()    , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	MPI_Bcast(&state_w.gf_susc_sc()(0)	, state_w.gf_susc_sc().num_elements()  , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	MPI_Bcast(&state_w.gf_susc_d()(0) 	, state_w.gf_susc_d().num_elements()   , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	MPI_Bcast(&state_w.gf_susc_m()(0) 	, state_w.gf_susc_m().num_elements()   , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	MPI_Bcast(&state_w.gf_asytri_sc()(0)	, state_w.gf_asytri_sc().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	MPI_Bcast(&state_w.gf_asytri_d()(0)	, state_w.gf_asytri_d().num_elements() , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	MPI_Bcast(&state_w.gf_asytri_m()(0)	, state_w.gf_asytri_m().num_elements() , MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	rhs(state_w,dfdl_w,Lam_w);
	}
#endif
	}
	//stop profiling after the core job has been done

   return EXIT_SUCCESS;

}
