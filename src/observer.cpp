
/************************************************************************************************//**
 *  		
 * 	file: 		observer.cpp
 * 	contents:   	See observer.h
 * 
 ****************************************************************************************************/


#include <observer.h>
#include <observables.h>
#include <rhs.h>
//#include <fftw3.h>

using namespace std;

bool observer_t::operator() ( const state_t& state_vec, const double Lam )
{

   // Write the observables to be tracked during the flow
   auto flow_obs_lst_itr = flow_obs_lst.begin(); 
   ( *flow_obs_lst_itr ).second.push_back( Lam ); 
   
   // Calculate maximal coupling
   double max_cpl = get_max_susc_cpl( state_vec ); 
    max_cpl = max_cpl/4.0/PI/PI; 
   ++flow_obs_lst_itr; 
   ( *flow_obs_lst_itr ).second.push_back( max_cpl ); 
   
#ifdef POSTPROC_LAM
   // Calculate further observables to be tracked, which depend also on Lam

   postproc_t postproc_vec;

   // Precalculate Green function and single-scale propagator on frequency grid
   gf_1p_mat_t Gvec( POS_1P_RANGE, FFT_DIM*FFT_DIM ); 
   gf_1p_mat_t Svec( POS_1P_RANGE, FFT_DIM*FFT_DIM );
#ifdef MPI_PARALLEL
   Gvec.init( bind( &state_t::GMat, boost::cref(state_vec), _1, Lam ),1,0 ); // Initialize big Green function vector 
   Svec.init( bind( &state_t::SMat, boost::cref(state_vec), _1, Lam ),1,0 ); // Initialize big Single scale vector 
#else  //MPI_PARALLEL
   Gvec.init( bind( &state_t::GMat, boost::cref(state_vec), _1, Lam ) ); // Initialize big Green function vector 
   Svec.init( bind( &state_t::SMat, boost::cref(state_vec), _1, Lam ) ); // Initialize big Single scale vector 
#endif //MPI_PARALLEL
   // Green function in real lattice grid and bubbles GG (not the same as SG+GS)
   gf_1p_mat_real_t Gvec_real;
   gf_1p_mat_real_t Svec_real;   
   gf_bubble_mat_t bubble_GG_pp;
   gf_bubble_mat_t bubble_GG_ph;
   
   cout << " ... bubble PT1 " << endl;
   fftw_complex *input, *output;	// input output functions for the FFTW
   fftw_plan p, p_b; 			// plan for FFTW
   input = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * FFT_DIM * FFT_DIM);
   output = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * FFT_DIM * FFT_DIM);
   p = fftw_plan_dft_2d(FFT_DIM, FFT_DIM, input, output, FFTW_BACKWARD, FFTW_ESTIMATE); //plan for FFTW, same for Gvec and Svec (BACKWARD), different for bubble (FORWARD)
  
#ifdef MPI_PARALLEL
   rhs_t::symm_grp_Gvec_real.init( Gvec_real, [&Gvec, p, Lam]( const idx_1p_mat_real_t& idx ){ return rhs_t::eval_Gvec_real( idx, Gvec, p, Lam ); },1,0 );
   rhs_t::symm_grp_Gvec_real.init( Svec_real, [&Svec, p, Lam]( const idx_1p_mat_real_t& idx ){ return rhs_t::eval_Svec_real( idx, Svec, p, Lam ); },1,0 ); // Same symm as Gvec_real
#else //MPI_PARALLEL
   rhs_t::symm_grp_Gvec_real.init( Gvec_real, [&Gvec, p, Lam]( const idx_1p_mat_real_t& idx ){ return rhs_t::eval_Gvec_real( idx, Gvec, p, Lam ); } );
   rhs_t::symm_grp_Gvec_real.init( Svec_real, [&Svec, p, Lam]( const idx_1p_mat_real_t& idx ){ return rhs_t::eval_Svec_real( idx, Svec, p, Lam ); } ); // Same symm as Gvec_real
#endif //MPI_PARALLEL
   
   
   cout << " ... bubble PT2 " << endl;
   p_b = fftw_plan_dft_2d(FFT_DIM, FFT_DIM, input, output, FFTW_FORWARD, FFTW_ESTIMATE); //plan for FFTW, same for Gvec and Svec (BACKWARD), different for bubble (FORWARD)
   rhs_t::symm_grp_bubble_pp.init( bubble_GG_pp, [&Gvec_real, p_b, Lam]( const idx_bubble_mat_t& idx ){ return rhs_t::eval_diag_bubble_GG_pp( idx, Gvec_real, p_b, Lam ); } ); 
   rhs_t::symm_grp_bubble_ph.init( bubble_GG_ph, [&Gvec_real, p_b, Lam]( const idx_bubble_mat_t& idx ){ return rhs_t::eval_diag_bubble_GG_ph( idx, Gvec_real, p_b, Lam ); } ); 

   fftw_destroy_plan(p);
   fftw_destroy_plan(p_b);
   fftw_free(input); fftw_free(output);
   
   cout << " ... susc postproc " << endl << endl;
   rhs_t::symm_grp_susc_postproc.init( postproc_vec.gf_susc_postproc_sc(), [&state_vec, &bubble_GG_pp, Lam]( const idx_susc_postproc_t& idx ){ return rhs_t::eval_diag_susc_postproc_sc( idx, state_vec, bubble_GG_pp, Lam ); } ); 
   rhs_t::symm_grp_susc_postproc.init( postproc_vec.gf_susc_postproc_d(), [&state_vec, &bubble_GG_ph, Lam]( const idx_susc_postproc_t& idx ){ return rhs_t::eval_diag_susc_postproc_d( idx, state_vec, bubble_GG_ph, Lam ); } ); 
   rhs_t::symm_grp_susc_postproc.init( postproc_vec.gf_susc_postproc_m(), [&state_vec, &bubble_GG_ph, Lam]( const idx_susc_postproc_t& idx ){ return rhs_t::eval_diag_susc_postproc_m( idx, state_vec, bubble_GG_ph, Lam ); } ); 
   
#endif   //POSTPROC_LAM
   
   // Calculate further observables to be tracked
   for( auto flow_obs_func : flow_obs_func_lst )
   {
      ++flow_obs_lst_itr; 
#ifdef POSTPROC_LAM
      ( *flow_obs_lst_itr ).second.push_back( flow_obs_func.second( state_vec, postproc_vec, Lam ) ); 
#else
      ( *flow_obs_lst_itr ).second.push_back( flow_obs_func.second( state_vec, Lam ) ); 
#endif
   }

   return ( max_cpl < MAX_COUPLING ) ? 1 : 0; 

}

observer_t::observer_t( std::vector<   std::pair< std::string, std::vector<double> >    >&   flow_obs_lst_ ): flow_obs_lst( flow_obs_lst_ )
{

   // Track lambda, abs_max_cpl 
   flow_obs_lst.push_back( { "LAM", {} } ); 
   flow_obs_lst.push_back( { "ABS_MAX_CPL", {} } ); 

   // Fill rest of flow observables into list, COMPARE observables.cpp
   for( auto flow_obs_func : flow_obs_func_lst )
      flow_obs_lst.push_back( { flow_obs_func.first, {} } ); 

}
