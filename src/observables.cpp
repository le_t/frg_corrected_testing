
/************************************************************************************************//**
 *  		
 * 	file: 		observables.cpp
 * 	contents:   	See observables.h
 * 
 ****************************************************************************************************/


#include <observables.h>
#include <complex>
#include <params.h>
#include <frg.h>
#include <mymath.h>
#include <grid.h>

using namespace std; 

#ifdef POSTPROC_LAM
flow_obs_func_lst_t  flow_obs_func_lst = { { "EFF_MASS", eff_mass }, { "ERR_EFF_MASS", err_eff_mass }, { "SUSC_FL_SC_00" , susc_flow_sc_00}, { "SUSC_FL_SC_0PI"  , susc_flow_sc_0pi}, { "SUSC_FL_SC_PIPI"  , susc_flow_sc_pipi}, { "SUSC_FL_D_00" , susc_flow_d_00}, { "SUSC_FL_D_0PI" , susc_flow_d_0pi}, { "SUSC_FL_D_PIPI" , susc_flow_d_pipi}, { "SUSC_FL_M_00"  , susc_flow_m_00}, { "SUSC_FL_M_0PI" , susc_flow_m_0pi}, { "SUSC_FL_M_PIPI" , susc_flow_m_pipi}, { "SUSC_PP_SC_00" , susc_postproc_sc_00}, { "SUSC_PP_SC_0PI"  , susc_postproc_sc_0pi}, { "SUSC_PP_SC_PIPI"  , susc_postproc_sc_pipi}, { "SUSC_PP_D_00" , susc_postproc_d_00}, { "SUSC_PP_D_0PI" , susc_postproc_d_0pi}, { "SUSC_PP_D_PIPI"  , susc_postproc_d_pipi}, { "SUSC_PP_M_00" , susc_postproc_m_00}, { "SUSC_PP_M_0PI" , susc_postproc_m_0pi}, { "SUSC_PP_M_PIPI" , susc_postproc_m_pipi} };
#else
flow_obs_func_lst_t  flow_obs_func_lst = { { "EFF_MASS", eff_mass }, { "ERR_EFF_MASS", err_eff_mass } }; 	///< List to contain all observable functions to be tracked during the flow
#endif

#ifdef POSTPROC_LAM
double eff_mass( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   return 1.0 - BETA/2.0/PI * ( imag( state_vec.Sig( 1, 0, 0, 0 ) - state_vec.Sig( 0, 0, 0, 0 ) ) ); 
}
#else
double eff_mass( const state_t& state_vec, double Lam )
{
   return 1.0 - BETA/2.0/PI * ( imag( state_vec.Sig( 1, 0, 0, 0 ) - state_vec.Sig( 0, 0, 0, 0 ) ) ); 
}
#endif

#ifdef POSTPROC_LAM
double err_eff_mass( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   return eff_mass( state_vec, postproc_vec, Lam ) - ( 1.0 - BETA/4.0/PI * ( imag( state_vec.Sig( 2, 0, 0, 0 ) - state_vec.Sig( 0, 0, 0, 0 ) ) ) ) ; 
}
#else
double err_eff_mass( const state_t& state_vec, double Lam )
{
   return eff_mass( state_vec, Lam ) - ( 1.0 - BETA/4.0/PI * ( imag( state_vec.Sig( 2, 0, 0, 0 ) - state_vec.Sig( 0, 0, 0, 0 ) ) ) ) ; 
}
#endif

#ifdef POSTPROC_LAM
double filling( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   dcomplex val( 0.0, 0.0 );

   gf_1p_mat_t Gvec( POS_1P_RANGE, FFT_DIM*FFT_DIM*FFT_DIM );
#ifdef MPI_PARALLEL
   Gvec.init( boost::bind( &state_t::GMat_latt, boost::ref(state_vec), _1, Lam ),1,0 ); // Initialize big Green function vector
#else 
   Gvec.init( boost::bind( &state_t::GMat_latt, boost::ref(state_vec), _1, Lam ) ); // Initialize big Green function vector
#endif

   for( int w = -POS_1P_RANGE; w < POS_1P_RANGE; ++w )
      for( int k = 0; k < FFT_DIM*FFT_DIM; ++k )
	for(int s_in =0; s_in < QN_COUNT; ++s_in)
	   for(int s_out =0; s_out < QN_COUNT; ++s_out){
	    val += Gvec[w][k]( s_out, s_in )/BETA/double(FFT_DIM)/double(FFT_DIM);
           
	    //if( s_in == s_out )
	    //  val += 0.5; // Large frequency contribution
	   }
   
   if( abs( imag( val )) > 0.00001 )
     cout << " CAUTION, nonvanishing imaginary part of filling: " << imag( val ) << endl; 
   
   return real( val ) + 0.5; 
}
#else
double filling( const state_t& state_vec, double Lam )
{
   dcomplex val( 0.0, 0.0 );

   gf_1p_mat_t Gvec( POS_1P_RANGE, FFT_DIM*FFT_DIM*FFT_DIM );
#ifdef MPI_PARALLEL
   Gvec.init( boost::bind( &state_t::GMat_latt, boost::ref(state_vec), _1, Lam ),1,0 ); // Initialize big Green function vector 
#else 
   Gvec.init( boost::bind( &state_t::GMat_latt, boost::ref(state_vec), _1, Lam ) ); // Initialize big Green function vector 
#endif

   for( int w = -POS_1P_RANGE; w < POS_1P_RANGE; ++w )
      for( int k = 0; k < FFT_DIM*FFT_DIM; ++k )
	for(int s_in =0; s_in < QN_COUNT; ++s_in)
	   for(int s_out =0; s_out < QN_COUNT; ++s_out){
	    val += Gvec[w][k]( s_out, s_in )/BETA/double(FFT_DIM)/double(FFT_DIM);
           
	    //if( s_in == s_out )
	    //  val += 0.5; // Large frequency contribution
	   }
   
   if( abs( imag( val )) > 0.00001 )
     cout << " CAUTION, nonvanishing imaginary part of filling: " << imag( val ) << endl; 
   
   return real( val ) + 0.5; 
}
#endif

#ifdef POSTPROC_LAM
double susc_postproc_sc_00( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   return real(postproc_vec.gf_susc_postproc_sc()[0]);
}
double susc_postproc_sc_0pi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   return real(postproc_vec.gf_susc_postproc_sc()[1]);
}
double susc_postproc_sc_pipi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   return real(postproc_vec.gf_susc_postproc_sc()[2]);
}
double susc_postproc_d_00( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   return real(postproc_vec.gf_susc_postproc_d()[0]);
}
double susc_postproc_d_0pi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   return real(postproc_vec.gf_susc_postproc_d()[1]);
}
double susc_postproc_d_pipi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   return real(postproc_vec.gf_susc_postproc_d()[2]);
}
double susc_postproc_m_00( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   return real(postproc_vec.gf_susc_postproc_m()[0]);
}
double susc_postproc_m_0pi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   return real(postproc_vec.gf_susc_postproc_m()[1]);
}
double susc_postproc_m_pipi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   return real(postproc_vec.gf_susc_postproc_m()[2]);
}

double susc_flow_sc_00( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   int K=get_k_patch(0,0);
   return real(state_vec.gf_susc_sc()[0][K][0][0][0][0][0][0]);
}
double susc_flow_sc_0pi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   int K=get_k_patch(0,MAX_KPOS);
   return real(state_vec.gf_susc_sc()[0][K][0][0][0][0][0][0]);
}
double susc_flow_sc_pipi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   int K=get_k_patch(MAX_KPOS,MAX_KPOS);
   return real(state_vec.gf_susc_sc()[0][K][0][0][0][0][0][0]);
}
double susc_flow_d_00( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   int K=get_k_patch(0,0);
   return real(state_vec.gf_susc_d()[0][K][0][0][0][0][0][0]);
}
double susc_flow_d_0pi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   int K=get_k_patch(0,MAX_KPOS);
   return real(state_vec.gf_susc_d()[0][K][0][0][0][0][0][0]);
}
double susc_flow_d_pipi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   int K=get_k_patch(MAX_KPOS,MAX_KPOS);
   return real(state_vec.gf_susc_d()[0][K][0][0][0][0][0][0]);
}
double susc_flow_m_00( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   int K=get_k_patch(0,0);
   return real(state_vec.gf_susc_m()[0][K][0][0][0][0][0][0]);
}
double susc_flow_m_0pi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   int K=get_k_patch(0,MAX_KPOS);
   return real(state_vec.gf_susc_m()[0][K][0][0][0][0][0][0]);
}
double susc_flow_m_pipi( const state_t& state_vec, const postproc_t& postproc_vec, double Lam )
{
   int K=get_k_patch(MAX_KPOS,MAX_KPOS);
   return real(state_vec.gf_susc_m()[0][K][0][0][0][0][0][0]);
}
#endif

double jos_curr( const state_t& state_vec )
{
   double val( 0.0 );

   gf_1p_mat_t Gvec( POS_1P_RANGE , FFT_DIM*FFT_DIM*FFT_DIM ); 
#ifdef MPI_PARALLEL
   Gvec.init( boost::bind( &state_t::GMat, boost::ref(state_vec), _1, LAM_FIN ),1,0 ); // Initialize big Green function vector 
#else
   Gvec.init( boost::bind( &state_t::GMat, boost::ref(state_vec), _1, LAM_FIN ) ); // Initialize big Green function vector 
#endif

//#pragma omp parallel for schedule(dynamic) reduction(+:val)
   for( int w = -POS_1P_RANGE; w < POS_1P_RANGE; ++w ) 
      for( int k = 0; k < PATCH_COUNT; ++k )
      {
	 double w_val = ( 0.5 + w ) * 2*PI / BETA; 
	 val += 4.0 * GAM_L * imag( polar(DEL, PHI_L) / sqrt(w_val*w_val+DEL*DEL) * Gvec[w][k]( 1, 0 ) );
      }

   val *= 1.0 / BETA / PATCH_COUNT; 
		  
   return val; 
}

double  get_max_cpl( const state_t& state_vec )
{
   const dcomplex* max_pp_ptr = max_element( state_vec.gf_phi_pp().data(), state_vec.gf_phi_pp().data() + state_vec.gf_phi_pp().num_elements(), []( const dcomplex& a, const dcomplex& b )->bool{ return std::abs(a) < std::abs(b); } ); 
   int max_pp_distance = distance( state_vec.gf_phi_pp().data(), max_pp_ptr ); 
   cout  << " Max Phi pp " << *max_pp_ptr << " at idx " << state_vec.gf_phi_pp().get_idx( max_pp_distance ) << endl; 

   const dcomplex* max_ph_ptr = max_element( state_vec.gf_phi_ph().data(), state_vec.gf_phi_ph().data() + state_vec.gf_phi_ph().num_elements(), []( const dcomplex& a, const dcomplex& b )->bool{ return std::abs(a) < std::abs(b); } ); 
   int max_ph_distance = distance( state_vec.gf_phi_ph().data(), max_ph_ptr ); 
   cout  << " Max Phi ph " << *max_ph_ptr << " at idx " << state_vec.gf_phi_ph().get_idx( max_ph_distance ) << endl; 

   const dcomplex* max_xph_ptr = max_element( state_vec.gf_phi_xph().data(), state_vec.gf_phi_xph().data() + state_vec.gf_phi_xph().num_elements(), []( const dcomplex& a, const dcomplex& b )->bool{ return std::abs(a) < std::abs(b); } ); 
   int max_xph_distance = distance( state_vec.gf_phi_xph().data(), max_xph_ptr ); 
   cout << " Max Phi xph " << *max_xph_ptr << " at idx " << state_vec.gf_phi_xph().get_idx( max_xph_distance ) << endl; 

   const dcomplex* min_pp_ptr = min_element( state_vec.gf_phi_pp().data(), state_vec.gf_phi_pp().data() + state_vec.gf_phi_pp().num_elements(), []( const dcomplex& a, const dcomplex& b )->bool{ return std::abs(a) < std::abs(b); } ); 
   int min_pp_distance = distance( state_vec.gf_phi_pp().data(), min_pp_ptr ); 
   cout  << " Max Phi pp " << *min_pp_ptr << " at idx " << state_vec.gf_phi_pp().get_idx( min_pp_distance ) << endl; 

   const dcomplex* min_ph_ptr = min_element( state_vec.gf_phi_ph().data(), state_vec.gf_phi_ph().data() + state_vec.gf_phi_ph().num_elements(), []( const dcomplex& a, const dcomplex& b )->bool{ return std::abs(a) < std::abs(b); } ); 
   int min_ph_distance = distance( state_vec.gf_phi_ph().data(), min_ph_ptr ); 
   cout  << " Max Phi ph " << *min_ph_ptr << " at idx " << state_vec.gf_phi_ph().get_idx( min_ph_distance ) << endl; 

   const dcomplex* min_xph_ptr = min_element( state_vec.gf_phi_xph().data(), state_vec.gf_phi_xph().data() + state_vec.gf_phi_xph().num_elements(), []( const dcomplex& a, const dcomplex& b )->bool{ return std::abs(a) < std::abs(b); } ); 
   int min_xph_distance = distance( state_vec.gf_phi_xph().data(), min_xph_ptr ); 
   cout << " Max Phi xph " << *min_xph_ptr << " at idx " << state_vec.gf_phi_xph().get_idx( min_xph_distance ) << endl; 

   return max( max( abs(*max_pp_ptr), abs(*max_ph_ptr) ), abs(*max_xph_ptr) );
}

double  get_max_susc_cpl( const state_t& state_vec )
{
   const dcomplex* max_sc_ptr = max_element( state_vec.gf_susc_sc().data(), state_vec.gf_susc_sc().data() + state_vec.gf_susc_sc().num_elements(), []( const dcomplex& a, const dcomplex& b )->bool{ return std::abs(a) < std::abs(b); } ); 
   int max_sc_distance = distance( state_vec.gf_susc_sc().data(), max_sc_ptr ); 
   cout  << " Max Susc superco " << *max_sc_ptr << " at idx " << state_vec.gf_susc_sc().get_idx( max_sc_distance ) << endl; 

   const dcomplex* max_d_ptr = max_element( state_vec.gf_susc_d().data(), state_vec.gf_susc_d().data() + state_vec.gf_susc_d().num_elements(), []( const dcomplex& a, const dcomplex& b )->bool{ return std::abs(a) < std::abs(b); } ); 
   int max_d_distance = distance( state_vec.gf_susc_d().data(), max_d_ptr ); 
   cout  << " Max Susc density " << *max_d_ptr << " at idx " << state_vec.gf_susc_d().get_idx( max_d_distance ) << endl; 

   const dcomplex* max_m_ptr = max_element( state_vec.gf_susc_m().data(), state_vec.gf_susc_m().data() + state_vec.gf_susc_m().num_elements(), []( const dcomplex& a, const dcomplex& b )->bool{ return std::abs(a) < std::abs(b); } ); 
   int max_m_distance = distance( state_vec.gf_susc_m().data(), max_m_ptr ); 
   cout << " Max Susc magnetic " << *max_m_ptr << " at idx " << state_vec.gf_susc_m().get_idx( max_m_distance ) << endl; 

   const dcomplex* min_sc_ptr = min_element( state_vec.gf_susc_sc().data(), state_vec.gf_susc_sc().data() + state_vec.gf_susc_sc().num_elements(), []( const dcomplex& a, const dcomplex& b )->bool{ return std::abs(a) < std::abs(b); } ); 
   int min_sc_distance = distance( state_vec.gf_susc_sc().data(), min_sc_ptr ); 
   cout  << " Min Susc superco " << *min_sc_ptr << " at idx " << state_vec.gf_susc_sc().get_idx( min_sc_distance ) << endl; 

   const dcomplex* min_d_ptr = min_element( state_vec.gf_susc_d().data(), state_vec.gf_susc_d().data() + state_vec.gf_susc_d().num_elements(), []( const dcomplex& a, const dcomplex& b )->bool{ return std::abs(a) < std::abs(b); } ); 
   int min_d_distance = distance( state_vec.gf_susc_d().data(), min_d_ptr ); 
   cout  << " Min Susc density " << *min_d_ptr << " at idx " << state_vec.gf_susc_d().get_idx( min_d_distance ) << endl; 

   const dcomplex* min_m_ptr = min_element( state_vec.gf_susc_m().data(), state_vec.gf_susc_m().data() + state_vec.gf_susc_m().num_elements(), []( const dcomplex& a, const dcomplex& b )->bool{ return std::abs(a) < std::abs(b); } ); 
   int min_m_distance = distance( state_vec.gf_susc_m().data(), min_m_ptr ); 
   cout << " Min Susc magnetic " << *min_m_ptr << " at idx " << state_vec.gf_susc_m().get_idx( min_m_distance ) << endl; 

   return max( max( abs(*max_sc_ptr), abs(*max_d_ptr) ), abs(*max_m_ptr) );
}
