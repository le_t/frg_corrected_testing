
/************************************************************************************************//**
 *  		
 * 	file: 		output.cpp
 * 	contents:   	See output.h
 * 
 ****************************************************************************************************/


#include <output.h>
#include <params.h>
#include <const.h>
#include <frg.h>
#include <observables.h>
#include <grid.h>
#include <ctime>
#include <H5Tools.h>
#include <state.h>

//using namespace boost;
using namespace H5; 
using namespace std; 


void init( H5File& file )
{
   time_t curr_time = time( nullptr );
   char* time_str = asctime( localtime(&curr_time )); 
      
   DataSpace dsp = DataSpace( H5S_SCALAR );

   StrType strdatatype( PredType::C_S1, H5T_VARIABLE ); // String type with variable length

   hid_t time_att_id = H5Acreate2( file.getId(), "DATE_TIME", strdatatype.getId(), dsp.getId(), H5P_DEFAULT, H5P_DEFAULT ); // Adds attribute to root group of file - find c++ equivalent! 

   Attribute time_att( time_att_id );
   time_att.write( strdatatype, &time_str );
}

void write_config( H5File& file )
{
   Group group( file.createGroup("/Config"));

   vector<pair<string, double>> config_scalar_list = 
   { 
      { "POS_FFREQ_COUNT_SIG", POS_FFREQ_COUNT_SIG }, 
      { "POS_FFREQ_COUNT_PHI", POS_FFREQ_COUNT_PHI }, 
      { "POS_BFREQ_COUNT_PHI", POS_BFREQ_COUNT_PHI }, 
      { "POS_FFREQ_COUNT_P", POS_FFREQ_COUNT_P }, 
      { "POS_BFREQ_COUNT_P", POS_BFREQ_COUNT_P }, 
      { "POS_BFREQ_COUNT_CHI", POS_BFREQ_COUNT_CHI }, 
      { "POS_INT_RANGE", POS_INT_RANGE }, 
      { "PATCH_COUNT", PATCH_COUNT }, 
      { "QN_COUNT", QN_COUNT }, 
      { "FFACTOR_COUNT", FFACTOR_COUNT},
      { "LAM_START", LAM_START }, 
   };

   for( auto conf : config_scalar_list )
      write( conf.second, group, conf.first); 
   
   vector<pair<string, string>> config_text_list = 
   {
      { "FLOW_SCHEME", FLOW_SCHEME_STRING }, 
   }; 
   
   for( auto conf : config_text_list )
      write( conf.second, group, conf.first); 

   DataSpace config_dsp = DataSpace ( H5S_SCALAR );

   int katanin_int =
#ifdef KATANIN
      1;
#else
      0; 
#endif
   write( katanin_int, group, "KATANIN" ); 
   
   int forced_zeros_int =
#ifdef FORCED_ZEROS
      1;
#else
      0; 
#endif
   write( forced_zeros_int, group, "FORCED_ZEROS" ); 
}

void write_params( H5File& file )
{

   Group group( file.createGroup("/Params"));

vector<pair<string, double>> par_lst = 
{ 
   { "UINT", UINT },
/************** MULTIORBITAL CASE********** 
   { "UINTP", UNITP },
   { "J", J },
   { "JP", JP }, 
*****************************************/
   { "BETA", BETA },
   { "B", B }, 
   { "MU", MU } 
};

   for( auto par : par_lst )
      write( par.second, group, par.first); 

}

void write_vert_tensor( H5File& file, const state_t& state_vec )
{
   Group group( file.createGroup("/Vert") );

   gf_2p_t vert_plot( POS_PLOT_RANGE_VERT ); 
   vert_plot.init( boost::bind( &state_t::vertx, boost::ref(state_vec), _1 ) ); 

   write( vert_plot, group, "" ); 
   write( F_Grid( POS_PLOT_RANGE_VERT, 2.0*PI / BETA ), group );
}

void write_Sig_tensor( H5::H5File& file, const state_t& state_vec )
{
   Group group( file.createGroup("/Sig") );
   write( state_vec.gf_Sig(), group ); 
   write( F_Grid( POS_FFREQ_COUNT_SIG, 2.0*PI / BETA ), group );
}

void write_vert_func( H5File& file, const state_t& state_vec )
{
   Group group( file.createGroup("/vert_func") );

   gf_phi_t gf_vert_pp_plot( POS_PLOT_RANGE_PHI ); 
   gf_phi_t gf_vert_ph_plot( POS_PLOT_RANGE_PHI ); 
   gf_phi_t gf_vert_xph_plot( POS_PLOT_RANGE_PHI ); 

   gf_vert_pp_plot.init( bind( &state_t::vertx_pp, boost::cref(state_vec), _1 ) ); 
   gf_vert_ph_plot.init( bind( &state_t::vertx_ph, boost::cref(state_vec), _1 ) ); 
   gf_vert_xph_plot.init( bind( &state_t::vertx_xph, boost::cref(state_vec), _1 ) ); 

   write( gf_vert_pp_plot, group, "_PP" ); 
   write( gf_vert_ph_plot, group, "_PH" ); 
   write( gf_vert_xph_plot, group, "_XPH" ); 

   write( Bos_Grid( POS_BFREQ_COUNT_PHI, 2.0*PI / BETA ), group );
   write( F_Grid( POS_PLOT_RANGE_PHI, 2.0*PI / BETA ), group );

}

void write_phi_func( H5File& file, const state_t& state_vec )
{
   Group group( file.createGroup("/phi_func") );

   gf_phi_t gf_phi_pp_plot( POS_PLOT_RANGE_PHI ); 
   gf_phi_t gf_phi_ph_plot( POS_PLOT_RANGE_PHI ); 
   gf_phi_t gf_phi_xph_plot( POS_PLOT_RANGE_PHI ); 

   gf_phi_pp_plot.init( bind( &state_t::phi_pp, boost::cref(state_vec), _1 ) ); 
   gf_phi_ph_plot.init( bind( &state_t::phi_ph, boost::cref(state_vec), _1 ) ); 
   gf_phi_xph_plot.init( bind( &state_t::phi_xph, boost::cref(state_vec), _1 ) ); 

   write( gf_phi_pp_plot, group, "_PP" ); 
   write( gf_phi_ph_plot, group, "_PH" ); 
   write( gf_phi_xph_plot, group, "_XPH" ); 

   write( Bos_Grid( POS_BFREQ_COUNT_PHI, 2.0*PI / BETA ), group );
   write( F_Grid( POS_PLOT_RANGE_PHI, 2.0*PI / BETA ), group );

}

void write_chi_func( H5File& file, const state_t& state_vec )
{
   Group group( file.createGroup("/chi_func") );

   write( state_vec.gf_chi_pp(), group, "_PP" ); 
   write( state_vec.gf_chi_ph(), group, "_PH" ); 
   write( state_vec.gf_chi_xph(), group, "_XPH" ); 

   write( Bos_Grid( POS_BFREQ_COUNT_CHI, 2.0*PI / BETA ), group );
}

void write_P_func( H5File& file, const state_t& state_vec )
{
   Group group( file.createGroup("/P_func") );

   write( state_vec.gf_P_pp(), group, "_PP" ); 
   write( state_vec.gf_P_ph(), group, "_PH" ); 
   write( state_vec.gf_P_xph(), group, "_XPH" ); 

   write( Bos_Grid( POS_BFREQ_COUNT_P, 2.0*PI / BETA ), group );
   write( F_Grid( POS_FFREQ_COUNT_P, 2.0*PI / BETA ), group );
}

void write_susc_func( H5File& file, const state_t& state_vec )
{
   Group group( file.createGroup("/susc_func") );

   write( state_vec.gf_susc_sc(), group, "_SC" ); 
   write( state_vec.gf_susc_d(), group, "_D" ); 
   write( state_vec.gf_susc_m(), group, "_M" ); 

   write( Bos_Grid( POS_BFREQ_COUNT_SUSC, 2.0*PI / BETA ), group );
}

void write_tri_func( H5File& file, const state_t& state_vec )
{
   Group group( file.createGroup("/Tri_func") );

   write( state_vec.gf_tri_sc(), group, "_SC" ); 
   write( state_vec.gf_tri_d(), group, "_D" ); 
   write( state_vec.gf_tri_m(), group, "_M" ); 

   write( Bos_Grid( POS_BFREQ_COUNT_TRI, 2.0*PI / BETA ), group );
   write( F_Grid( POS_FFREQ_COUNT_TRI, 2.0*PI / BETA ), group );
}

void write_R_func( H5File& file, const state_t& state_vec )
{
   Group group( file.createGroup("/R_func") );

   gf_phi_t R_pp_plot( POS_PLOT_RANGE_PHI ); 
   gf_phi_t R_ph_plot( POS_PLOT_RANGE_PHI ); 
   gf_phi_t R_xph_plot( POS_PLOT_RANGE_PHI ); 

   R_pp_plot.init( boost::bind( &state_t::R_pp, boost::ref(state_vec), _1 ) ); 
   R_ph_plot.init( boost::bind( &state_t::R_ph, boost::ref(state_vec), _1 ) ); 
   R_xph_plot.init( boost::bind( &state_t::R_xph, boost::ref(state_vec), _1 ) ); 

   write( R_pp_plot, group, "_PP" ); 
   write( R_ph_plot, group, "_PH" ); 
   write( R_xph_plot, group, "_XPH" ); 

   write( Bos_Grid( POS_BFREQ_COUNT_PHI, 2.0*PI / BETA ), group );
   write( F_Grid( POS_PLOT_RANGE_PHI, 2.0*PI / BETA ), group );
}


void write_Giw_tensor( H5::H5File& file, const state_t& state_vec )
{
   Group group( file.createGroup("/Giw") );

   gf_1p_t Gvec(POS_1P_RANGE);
   Gvec.init( bind( &state_t::Gval, boost::cref(state_vec), _1, LAM_FIN ) ); // Initialize big Green function vector 

   write( Gvec, group ); 
   write( F_Grid( POS_1P_RANGE, 2.0 * PI / BETA ), group ); 
}
////TODO IMPLEMENT OBSERVABLES
//void write_observables( H5::H5File& file, const state_t& state_vec )
//{
//   Group group( file.createGroup("/Observables") );
//
//   vector<pair<string, double>> obs_lst = 
//   { 
//      for( int s_in = 0; s_in < QN_COUNT; s_in++)
//         for( int s_out = 0; s_out < QN_COUNT; s_out++){
//            { "EFF_MASS", eff_mass( s_in, s_out, state_vec, LAM_FIN ) }, 
//            { "ERR_EFF_MASS", err_eff_mass(s_in, s_out, state_vec, LAM_FIN ) }, 
//            { "FILLING_" + to_string( ( long long ) s_out ) + to_string( ( long long ) s_in ), filling( s_in, s_out, state_vec, LAM_FIN )};
//	 }
//   };
//
//   // Add all possible fillings
//   //for( int s_in = 0; s_in < QN_COUNT; s_in++)
//   //   for( int s_out = 0; s_out < QN_COUNT; s_out++)
//   //      obs_lst.push_back( { "FILLING_" + to_string( ( long long ) s_out ) + to_string( ( long long ) s_in ), filling( s_in, s_out, state_vec, LAM_FIN ) } );
//   //
//   for( auto obs : obs_lst )
//      write( obs.second, group, obs.first ); 
//}
//
void write_flow_observables( vector<  pair< string, vector<double> >  >&   flow_obs_lst, H5::H5File& file )
{
   Group group( file.createGroup("/Flow_obs") );

   for( auto flow_obs : flow_obs_lst )
      write( flow_obs.second, group, flow_obs.first ); 
}
// write functions for FFT(GG), bubble and Gvec_real ---- not used in normal full run
// if output is wanted, uncomment part in ode.cpp
void write_bubble( H5::H5File& file, const gf_bubble_mat_t bubble_pp, const gf_bubble_mat_t bubble_ph )
{
   gf_bubble_t b_pp;
   gf_bubble_t b_ph;

   for(int W=-POS_BFREQ_COUNT_CHI; W<POS_BFREQ_COUNT_CHI; W++)
      for(int w=-(POS_INT_RANGE+POS_BFREQ_COUNT_CHI/2); w <POS_INT_RANGE+POS_BFREQ_COUNT_CHI/2; w++)
	 for(int K=0; K<PATCH_COUNT; K++)
	    for(int m=0; m<FFACTOR_COUNT; m++)
	       for(int n=0; n<FFACTOR_COUNT; n++)
		  for(int s1=0; s1<QN_COUNT; s1++)
		  for(int s2=0; s2<QN_COUNT; s2++)
		  for(int s3=0; s3<QN_COUNT; s3++)
		  for(int s4=0; s4<QN_COUNT; s4++)
	       {
	       b_pp[W][w][K][m][n][s1][s2][s3][s4] = bubble_pp[W][w][m][n][s1][s2][s3][s4](K);
	       b_ph[W][w][K][m][n][s1][s2][s3][s4] = bubble_ph[W][w][m][n][s1][s2][s3][s4](K);
	       }


   Group group( file.createGroup("/bubble") );

   write( b_pp, group, "_PP" ); 
   write( b_ph, group, "_PH" ); 
}

// write projection matrices ---- not used in normal full run
// if output is wanted, uncomment write in ode.cpp, here and here top: state.h
void write_projection( H5::H5File& file )
{
   state_t state_temp;

   Group group( file.createGroup("/Proj") );

   std::cout << state_temp.proj_matrix_ph_to_xph[5][15][0][4][2][0]<< std::endl;
   std::cout << state_temp.proj_matrix_ph_to_xph[5][15][4][0][2][0]<< std::endl;
   std::cout << state_temp.proj_matrix_ph_to_xph[5][15][0][4][0][2]<< std::endl;
   std::cout << state_temp.proj_matrix_ph_to_xph[5][15][4][0][0][2]<< std::endl;
   std::cout << state_temp.proj_matrix_ph_to_xph[15][5][0][4][2][0]<< std::endl;
   std::cout << state_temp.proj_matrix_ph_to_xph[15][5][4][0][2][0]<< std::endl;
   std::cout << state_temp.proj_matrix_ph_to_xph[15][5][0][4][0][2]<< std::endl;
   std::cout << state_temp.proj_matrix_ph_to_xph[15][5][4][0][0][2]<< std::endl;
   write( state_temp.proj_matrix_ph_to_pp, group, "_PHtoPP" ); 
   write( state_temp.proj_matrix_pp_to_ph, group, "_PPtoPH" ); 
   write( state_temp.proj_matrix_ph_to_xph, group, "_PHtoXPH" ); 
}

void write_Gfunc( H5::H5File& file, const gf_1p_mat_real_t Gvec_real, const gf_1p_mat_real_t Svec_real )
{
   gf_1p_real_t Gvec_real_plot;
   gf_1p_real_t Svec_real_plot;

   for(int W=-POS_1P_RANGE; W<POS_1P_RANGE; W++)
      for(int K=0; K<FFT_DIM*FFT_DIM; K++)
		for(int s1=0; s1<QN_COUNT; s1++)
		for(int s2=0; s2<QN_COUNT; s2++)
	       {
	       Gvec_real_plot[W][K][s1][s2] = Gvec_real[W][s1][s2](K);
	       Svec_real_plot[W][K][s1][s2] = Svec_real[W][s1][s2](K);
	       }


   Group group( file.createGroup("/Gvec") );

   write( Gvec_real_plot, group, "_Gvec" ); 
   write( Svec_real_plot, group, "_Svec" ); 
}
