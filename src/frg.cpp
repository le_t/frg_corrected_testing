
/************************************************************************************************//**
 *  		
 * 	file: 		frg.cpp
 * 	contents:  	for further documentation see frg.h
 * 
 ****************************************************************************************************/


#include <frg.h>
#include <Eigen/Eigenvalues>
#include <cmath>
//#include <params.h>
#include <mymath.h>

using namespace Eigen;
//using namespace std;


/********************* Hybridization function ********************/

#ifdef ED_BATH

MatQN Gam( double w )
{
   dcomplex val = 0.0; 
   for( int ed_idx = 0; ed_idx < energies.size(); ++ed_idx )
      val += hybridizations[ed_idx]*hybridizations[ed_idx] /( I*w - energies[ed_idx]) ; 

   MatQN Gam; 
   Gam <<
      val;
   return Gam; 
}

#elif QMC_BATH

MatQN Gam( double w )
{
   dcomplex val = 0.0;
   val = I / 2.0 * ( w - sgn( w ) * sqrt( 4.0 + w*w ) ); 

   MatQN Gam; 
   Gam <<
      val; 
   return Gam; 
}

#else // Wide band 

MatQN Gam( double w )
{
   double sq = sqrt( w*w + DEL*DEL );
   double dt = DEL / sq * DD;

   MatQN Gam; 
   Gam << 
      -I*w/sq; 
   return Gam; 
}

#endif


/*********************  INTERACTION FlOW  ********************/

#ifdef INT_FLOW

MatQN G( double w, double kx, double ky, double Lam, const MatQN& selfEn )
//MatQN G( double w, double kx, double ky,double kz, double Lam)
{
   return Lam * ( G0inv( w, kx, ky ) - Lam * selfEn ).inverse();
}

MatQN G_latt( double w, double kx, double ky, double Lam, const MatQN& selfEn )
//MatQN G( double w, double kx, double ky,double kz, double Lam)
{
   return ( G0inv( w, kx, ky ) - Lam * selfEn ).inverse();
}

MatQN S( double w, double kx, double ky, double Lam, const MatQN& selfEn )
{
   MatQN G0invMat = G0inv( w, kx, ky );
   MatQN GoLam = ( G0invMat - Lam * selfEn ).inverse(); 
   return GoLam * G0invMat * GoLam; // CAUTION : Sign change arises only in interaction flow, usual definition with minus sign
}

dcomplex asympt_SG_pp( int W_int, double Lam )
{
   double PIR = POS_INT_RANGE + abs(W_int/2); 
   double W = W_int; 

   if( W_int == 0 )
      return (BETA*Lam)/(2.0*pow(PI,2.0)*PIR); 

   return (BETA*Lam*atanh(W/(2.0*PIR)))/(pow(PI,2.0)*W); 
}

dcomplex asympt_GG_pp( int W, double Lam )
{
   return Lam * asympt_SG_pp( W, Lam );  
}

/*********************  EBERLEIN FlOW  ********************/

#elif EBERL_FLOW

MatQN G( double w, double kx, double ky, double t, const MatQN& selfEn )
{
   double Lam = exp( t * LN_10 ); 
   double wt = sgn( w ) * sqrt( w*w + Lam*Lam );
   
   return ( G0inv( wt, kx, ky ) - selfEn ).inverse();
}

MatQN S( double w, double kx, double ky, double t, const MatQN& selfEn )
{
   double Lam = exp( t * LN_10 ); 

   MatQN GMat = G( w, kx, ky, t, selfEn ); 
   return - I * sgn( w ) * Lam / sqrt( w*w + Lam*Lam ) * GMat * GMat * LN_10 * Lam; // Accounting for d/dt Lam( t )
}

dcomplex asympt_SG_pp( int W_int, double t )
{
   double Lam = exp( t * LN_10 ); 
   double PIR = POS_INT_RANGE + abs(W_int/2); 
   double W = W_int; 

   return 0.0;  // TODO
}

dcomplex asympt_GG_pp( int W_int, double t )
{
   double Lam = exp( t * LN_10 ); 
   double PIR = POS_INT_RANGE + abs(W_int/2); 
   double W = W_int; 

   return 0.0;  // TODO
}

/*********************  OMEGA FlOW  ********************/

#elif OMEGA_FLOW

MatQN G( double w, double kx, double ky, double t, const MatQN& selfEn )
{
   double Lam = exp( t * LN_10 ); 

   double regulator = w*w / ( Lam*Lam + w*w ); 
   return ( G0inv( w, kx, ky) / regulator - selfEn ).inverse();
}

MatQN G_latt( double w, double kx, double ky, double t, const MatQN& selfEn )
{
   double Lam = exp( t * LN_10 ); 

   double regulator = w*w / ( Lam*Lam + w*w ); 
   return ( G0inv( w, kx, ky) - regulator*selfEn ).inverse();
}

MatQN S( double w, double kx, double ky, double t, const MatQN& selfEn )
{
   double Lam = exp( t * LN_10 ); 

   MatQN GMat = G( w, kx, ky, t, selfEn );
   return - 2.0 * Lam / w / w * GMat * G0inv( w, kx, ky ) * GMat * LN_10 * Lam; // Accounting for d/dt Lam(t)
}

dcomplex asympt_SG_pp( int W_int, double t )
{
   double Lam = exp( t * LN_10 ); 
   double PIR = POS_INT_RANGE + abs(W_int/2); 
   double W = W_int; 

   // CAUTION: Account for d/dt Lam(t) !
   if ( Lam < 0.1 )
   {
      if ( W_int == 0 )
	 return (
	       -(pow(BETA,3.0)*Lam)/(12.0*pow(PI,4.0)*pow(PIR,3.0))
	       ) * LN_10 * Lam; 
      return (
	 -(pow(BETA,3.0)*Lam*(-16.0*pow(PIR,3.0)*W + 12.0*PIR*pow(W,3.0) + pow(-4.0*pow(PIR,2.0) + pow(W,2.0),2.0)*atanh((4.0*PIR*W)/(4.0*pow(PIR,2.0) + pow(W,2.0)))))/
	    (4.0*pow(PI,4.0)*pow(W,3.0)*pow(-4.0*pow(PIR,2.0) + pow(W,2.0),2.0))
	    ) * LN_10 * Lam; 
   }

   return (
      -(-4.0*PI*PIR*BETA*Lam*(pow(PI,2.0)*(4.0*pow(PIR,2.0) - 5.0*pow(W,2.0)) - pow(BETA,2.0)*pow(Lam,2.0))*(pow(PI,2.0)*pow(W,2.0) + pow(BETA,2.0)*pow(Lam,2.0)) + 
	    (-(PI*W) + BETA*Lam)*(PI*W + BETA*Lam)*(pow(PI,2.0)*pow(-2.0*PIR + W,2.0) + pow(BETA,2.0)*pow(Lam,2.0))*(pow(PI,2.0)*pow(2.0*PIR + W,2.0) + pow(BETA,2.0)*pow(Lam,2.0))*
	    (atan((BETA*Lam)/(2.0*PI*PIR - PI*W)) + atan((BETA*Lam)/(2.0*PI*PIR + PI*W))) + 2.0*PI*W*BETA*Lam*
	    (pow(PI,4.0)*pow(-4.0*pow(PIR,2.0) + pow(W,2.0),2.0) + 2.0*pow(PI,2.0)*(4.0*pow(PIR,2.0) + pow(W,2.0))*pow(BETA,2.0)*pow(Lam,2.0) + pow(BETA,4.0)*pow(Lam,4.0))*
	    atanh((4.0*pow(PI,2.0)*PIR*W)/(pow(PI,2.0)*(4.0*pow(PIR,2.0) + pow(W,2.0)) + pow(BETA,2.0)*pow(Lam,2.0))))/
      (8.0*PI*pow(BETA,2.0)*pow((pow(PI,2.0)*pow(W,2.0))/pow(BETA,2.0) + pow(Lam,2.0),2.0)*(pow(PI,2.0)*pow(-2.0*PIR + W,2.0) + pow(BETA,2.0)*pow(Lam,2.0))*(pow(PI,2.0)*pow(2.0*PIR + W,2.0) + pow(BETA,2.0)*pow(Lam,2.0)))
      ) * LN_10 * Lam;  
}

dcomplex asympt_GG_pp( int W_int, double t )
{
   double Lam = exp( t * LN_10 ); 
   double PIR = POS_INT_RANGE + abs(W_int/2); 
   double W = W_int; 

   if ( W_int == 0 )

   {
      if ( Lam < 0.1 )
	 return BETA/(2.0*pow(PI,2.0)*PIR) - (pow(BETA,3.0)*pow(Lam,2.0))/(12.0*pow(PI,4.0)*pow(PIR,3.0)); 

      return (2.0*PI*PIR*BETA*Lam + (4.0*pow(PI,2.0)*pow(PIR,2.0) + pow(BETA,2.0)*pow(Lam,2.0))*atan((BETA*Lam)/(2.0*PI*PIR)))/(8.0*pow(PI,3.0)*pow(PIR,2.0)*Lam + 2.0*PI*pow(BETA,2.0)*pow(Lam,3.0)); 
   }

   return (BETA*(PI*W*BETA*Lam*(atan((BETA*Lam)/(2.0*PI*PIR - PI*W)) + atan((BETA*Lam)/(2.0*PI*PIR + PI*W))) + 
       (2.0*pow(PI,2.0)*pow(W,2.0) + pow(BETA,2.0)*pow(Lam,2.0))*atanh((4.0*pow(PI,2.0)*PIR*W)/(pow(PI,2.0)*(4.0*pow(PIR,2.0) + pow(W,2.0)) + pow(BETA,2.0)*pow(Lam,2.0)))))/
   (4.0*pow(PI,2.0)*(pow(PI,2.0)*pow(W,3.0) + W*pow(BETA,2.0)*pow(Lam,2.0))); 
}

/*********************  RESERVOIR FlOW  ********************/

#elif RES_FLOW

MatQN G( double w, double kx, double ky, double t, const MatQN& selfEn )
{
   double Lam = exp( t * LN_10 ); 

   return ( G0inv( w, kx, ky ) + I * sgn(w) * Lam * MatQN::Identity() - selfEn ).inverse();
}

MatQN S( double w, double kx, double ky, double t, const MatQN& selfEn )
{
   double Lam = exp( t * LN_10 ); 

   MatQN GMat = G( w, kx, ky, t, selfEn );
   return - I*sgn(w) * GMat * GMat * LN_10 * Lam; // Accounting for d/dt Lam(t)
}

dcomplex asympt_SG_pp( int W_int, double t )
{
   double Lam = exp( t * LN_10 ); 
   double PIR = POS_INT_RANGE + abs(W_int/2); 
   double W = W_int; 

   // CAUTION: Account for d/dt Lam(t) !
   return (
	 -2.0/((-4.0*pow(PI,3.0)*pow(W,2.0))/pow(BETA,2.0) + 4.0*PI*pow((2.0*PI*PIR)/BETA + Lam,2.0))	 
      ) * LN_10 * Lam;  
}

dcomplex asympt_GG_pp( int W_int, double t )
{
   double Lam = exp( t * LN_10 ); 
   double PIR = POS_INT_RANGE + abs(W_int/2); 
   double W = W_int; 

   if( W_int == 0 )
      return BETA/(2.0*pow(PI,2.0)*PIR + PI*BETA*Lam); 

   return (BETA*log((2.0*PI*PIR + PI*W + BETA*Lam)/(2.0*PI*PIR - PI*W + BETA*Lam)))/(2.0*pow(PI,2.0)*W); 
}

#endif

/************************ Common to all implemented cutoff schemes ********************************/

#ifdef NO_MOMENTA

// SIAM / SQDJJ, G0 scale independent for interaction cutoff, Matrix in Nambu basis
MatQN G0inv( double w, double kx, double ky )
{
   MatQN ginv; 
   ginv <<		
      I*w - EPS - B;

   return ginv - Gam( w ); 
}

#else

// Hubbard model, G0 scale independent for interaction cutoff, Matrix in Nambu basis

/*****************MULTIORBITAL MATRIX EXPRESSION****************************
MatQN G0inv( double w, double kx, double ky, double kz )
{
   double cos_kx  = cos( kx );
   double cos_ky  = cos( ky );
   double cos_kz  = cos( kz );

   MatQN G0inv; 
   
   G0inv <<		
	I*w+1.5*(cos_kx + cos_ky)-MU, 			sqrt(3.)*0.5*(cos_ky - cos_kx),
        sqrt(3.)*0.5*(cos_ky - cos_kx),  	I*w+0.5*(cos_kx + cos_ky)+2.*cos_kz-MU;
   
   return G0inv; 
}

****************************************************************************/

// Hubbard model, G0 scale independent for interaction cutoff, Matrix in Nambu basis
MatQN G0inv( double w, double kx, double ky )
{
   double cos_kx  = cos( kx );
   double cos_ky  = cos( ky );

   MatQN G0inv; 
   G0inv <<		
      I*w + 2.0 * ( cos_kx + cos_ky ) + 4.0 * (cos_kx * cos_ky ) * T_PRIME - B - MU;

   return G0inv; 
}

#endif

dcomplex asympt_SGpGS_pp( int W, double Lam )
{
   return 2.0 * asympt_SG_pp( W, Lam ); 
}

dcomplex asympt_SG_ph( int W, double Lam )
{
   return -asympt_SG_pp( W, Lam ); 
}

dcomplex asympt_SGpGS_ph( int W, double Lam )
{
   return 2.0 * asympt_SG_ph( W, Lam ); 
}

dcomplex asympt_GG_ph( int W, double Lam )
{
   return -asympt_GG_pp( W, Lam ); 
}

// ---- Initial values 

/*********************** MULTIORBITAL INTERACTION MATRIX *********************************

dcomplex vert_bare( int s1_in, int s2_in, int s1_out, int s2_out ) // bare vertex values in Nambu basis
{
   if ((s1_in == s2_in) && (s1_in == s1_out) && (s1_in == s2_out))
      return -UINT;

   else if ((s1_in == s2_out) && (s2_in == s1_out))
      return -J;

   else if ((s1_in == s1_out) && (s2_in == s2_out))
      return -UINTP;

   else if ((s1_in == s2_in) && (s1_out == s2_out))
      return -JP;
   
   else
   return dcomplex(0.0,0.0);
   
}

*****************************************************************************************/

dcomplex vert_bare( const idx_2p_t& idx ) // initial vertex values in Nambu basis
{
   return vert_bare( idx( I2P::s1_in ), idx( I2P::s2_in ), idx( I2P::s1_out ), idx( I2P::s2_out ) ); // return bare interaction 
}
// Unitary transformation from orbital to band basis

dcomplex vert_bare( int s1_in, int s2_in, int s1_out, int s2_out ) // bare vertex values in Nambu basis
{
   return -UINT;
}

dcomplex Sig_init( const idx_1p_t& idx ) // initial self-energy values in Nambu basis
{
   return 0.0; // vanishes for weak coupling flows
}

dcomplex phi_init( const idx_phi_t& idx ) // initial self-energy values in Nambu basis
{
   return 0.0; // vanishes for weak coupling flows
}

dcomplex P_init( const idx_P_t& idx ) // initial self-energy values in Nambu basis
{
   return 0.0; // vanishes for weak coupling flows
}

dcomplex chi_init( const idx_chi_t& idx ) // inital Karrasch functions in Nambu basis
{
   return 0.0; // vanishes for weak coupling flows
}
/************************ Initial values for the response functions *****************************/

dcomplex tri_bare( int n_in, int n_out, int s1_in, int s2_in, int s1_out, int s2_out ) // initial self-energy values in Nambu basis
{
   return double(n_in == n_out) * 1.0 * 4.0 * PI * PI; // vanishes for weak coupling flows
}

dcomplex tri_init( const idx_tri_t& idx ) // initial self-energy values in Nambu basis
{
   return tri_bare( idx(ITRI::n_in), idx(ITRI::n_out), idx( ITRI::s1_in ), idx( ITRI::s2_in ), idx( ITRI::s1_out ), idx( ITRI::s2_out ) ); // vanishes for weak coupling flows
}

dcomplex asytri_init( const idx_asytri_t& idx ) // inital Karrasch functions in Nambu basis
{
   return 0.0; // vanishes for weak coupling flows
}

dcomplex susc_init( const idx_susc_t& idx ) // inital Karrasch functions in Nambu basis
{
   return 0.0; // vanishes for weak coupling flows
}

dcomplex susc_postproc_init( const idx_susc_postproc_t& idx ) // inital Karrasch functions in Nambu basis
{
   return 0.0; // vanishes for weak coupling flows
}

/******************* set back values of all gf in state_t to 0 (needed for MPI)  ****************/

dcomplex Sig_zero( const idx_1p_t& idx ) 
{
   return 0.0; 
}
dcomplex phi_zero( const idx_phi_t& idx ) 
{
   return 0.0; 
}
dcomplex P_zero( const idx_P_t& idx ) 
{
   return 0.0; 
}
dcomplex chi_zero( const idx_chi_t& idx ) 
{
   return 0.0; 
}
dcomplex tri_zero( const idx_tri_t& idx ) 
{
   return 0.0; 
}
dcomplex asytri_zero( const idx_asytri_t& idx ) 
{
   return 0.0; 
}
dcomplex susc_zero( const idx_susc_t& idx ) 
{
   return 0.0; 
}
