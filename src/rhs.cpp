
/************************************************************************************************//**
 *  		
 * 	file: 		rhs.cpp
 * 	contents:   	See rhs.h
 * 
 ****************************************************************************************************/


#include <rhs.h>
#include <frg.h>
#include <params.h>
//#include <observables.h>
#include <cmath>
#include <translate.h>
#include <mymath.h>
#include <projection.h>
#include <complex>

using namespace std;
using boost::bind;

// --- Create static objects: symmetry groups, weight vector

// ALL SYMM

symmetry_grp_t<dcomplex, 4> rhs_t::symm_grp_sig(gf_1p_t(),{
    cconj_sig
    , rot_pi2_z_sig
    , mirror_y_sig
});

symmetry_grp_t<dcomplex, 10> rhs_t::symm_grp_phi_pp(gf_phi_t(),{
    hmirror_phi_pp
    , cconj_phi_pp
    , timerev_phi_pp
    , rot_pi2_z_phi
    , mirror_y_phi
});
symmetry_grp_t<dcomplex, 10> rhs_t::symm_grp_phi_ph(gf_phi_t(),{
    hmirror_phi_ph
    , cconj_phi_ph
    , timerev_phi_ph
    , rot_pi2_z_phi
    , mirror_y_phi
});
symmetry_grp_t<dcomplex, 10> rhs_t::symm_grp_phi_xph(gf_phi_t(),{
    hmirror_phi_xph
    , cconj_phi_xph
    , timerev_phi_xph
    , rot_pi2_z_phi
    , mirror_y_phi
});

symmetry_grp_t<dcomplex, 8> rhs_t::symm_grp_P_pp(gf_P_t(),{
    hmirror_P_pp
    , cconj_timerev_P_pp
    , rot_pi2_z_P
    , mirror_y_P
});
symmetry_grp_t<dcomplex, 8> rhs_t::symm_grp_P_ph(gf_P_t(),{
    timerev_P_ph
    , cconj_P_ph
    , rot_pi2_z_P
    , mirror_y_P
});
symmetry_grp_t<dcomplex, 8> rhs_t::symm_grp_P_xph(gf_P_t(),{
    hmirror_cconj_P_xph,
    hmirror_timerev_P_xph
    , rot_pi2_z_P
    , mirror_y_P
});

symmetry_grp_t<dcomplex, 6> rhs_t::symm_grp_chi_pp(gf_chi_t(),{
    hmirror_chi_pp
    , cconj_chi_pp
    , timerev_chi_pp
    , rot_pi2_z_chi
    , mirror_y_chi
});
symmetry_grp_t<dcomplex, 6> rhs_t::symm_grp_chi_ph(gf_chi_t(),{
    hmirror_chi_ph
    , cconj_chi_ph
    , timerev_chi_ph
    , rot_pi2_z_chi
    , mirror_y_chi
});
symmetry_grp_t<dcomplex, 6> rhs_t::symm_grp_chi_xph(gf_chi_t(),{
    hmirror_chi_xph
    , cconj_chi_xph
    , timerev_chi_xph
    , rot_pi2_z_chi
    , mirror_y_chi
});

symmetry_grp_t<dcomplex, 9> rhs_t::symm_grp_tri_sc(gf_tri_t(),{
    hmirror_tri_sc
    , cconj_timerev_tri_sc
    , rot_pi2_z_tri
    , mirror_y_tri
});
symmetry_grp_t<dcomplex, 9> rhs_t::symm_grp_tri_d(gf_tri_t(),{
    timerev_tri_d
    , cconj_tri_d
    , rot_pi2_z_tri
    , mirror_y_tri
});
symmetry_grp_t<dcomplex, 9> rhs_t::symm_grp_tri_m(gf_tri_t(),{
    hmirror_cconj_tri_m
    , hmirror_timerev_tri_m
    , rot_pi2_z_tri
    , mirror_y_tri
});

symmetry_grp_t<dcomplex, 7> rhs_t::symm_grp_asytri_sc(gf_asytri_t(),{
    hmirror_asytri_sc
    , cconj_timerev_asytri_sc
    , rot_pi2_z_asytri
    , mirror_y_asytri
});

symmetry_grp_t<dcomplex, 7> rhs_t::symm_grp_asytri_d(gf_asytri_t(),{
    timerev_asytri_d
    , cconj_asytri_d
    , rot_pi2_z_asytri
    , mirror_y_asytri
});

symmetry_grp_t<dcomplex, 7> rhs_t::symm_grp_asytri_m(gf_asytri_t(),{
    hmirror_cconj_asytri_m
    , hmirror_timerev_asytri_m
    , rot_pi2_z_asytri
    , mirror_y_asytri
});
symmetry_grp_t<dcomplex, 8> rhs_t::symm_grp_susc_sc(gf_susc_t(),{
    hmirror_susc_sc
    , cconj_susc_sc
    , timerev_susc_sc
    , rot_pi2_z_susc
    , mirror_y_susc
});

symmetry_grp_t<dcomplex, 8> rhs_t::symm_grp_susc_d(gf_susc_t(),{
    hmirror_susc_d
    , cconj_susc_d
    , timerev_susc_d
    , rot_pi2_z_susc
    , mirror_y_susc
});

symmetry_grp_t<dcomplex, 8> rhs_t::symm_grp_susc_m(gf_susc_t(),{
    hmirror_susc_m
    , cconj_susc_m
    , timerev_susc_m
    , rot_pi2_z_susc
    , mirror_y_susc
});

symmetry_grp_t<dcomplex, 1> rhs_t::symm_grp_susc_postproc(gf_susc_postproc_t(),{
});

symmetry_grp_t<MatReal, 3> rhs_t::symm_grp_Gvec_real(gf_1p_mat_real_t(),{});

symmetry_grp_t<MatPatch, 8> rhs_t::symm_grp_bubble_pp(gf_bubble_mat_t(),{
    swap_bubble, cconj_bubble, hmirror_bubble_pp
});
symmetry_grp_t<MatPatch, 8> rhs_t::symm_grp_bubble_ph(gf_bubble_mat_t(),{
    swap_bubble, cconj_bubble, hmirror_timerev_ph
});


// TODO: Implement more symmetries in the multiloop "LEFT"-diamgrams 
// (Note: If we include just the lattice symmetries, same as "RIGHT"-diagrams)

symmetry_grp_t<dcomplex, 10> rhs_t::symm_grp_phi_pp_L(gf_phi_t(),{
    hmirror_phi_pp_L
    , cconj_timerev_phi_pp_L
    , rot_pi2_z_phi
    , mirror_y_phi
});
symmetry_grp_t<dcomplex, 10> rhs_t::symm_grp_phi_ph_L(gf_phi_t(),{
    timerev_phi_ph_L
    , cconj_phi_ph_L
    , rot_pi2_z_phi
    , mirror_y_phi
});
symmetry_grp_t<dcomplex, 10> rhs_t::symm_grp_phi_xph_L(gf_phi_t(),{
    hmirror_cconj_phi_xph_L
    , hmirror_timerev_phi_xph_L
    , rot_pi2_z_phi
    , mirror_y_phi
});

gf_weight_vec_t rhs_t::weight_vec(generate_weights(POS_INT_RANGE - TAIL_LENGTH, TAIL_LENGTH, FIT_ORDER));

gf<double, 2> rhs_t::weight_vec_2d(generate_2d_weights(POS_INT_RANGE - TAIL_LENGTH, TAIL_LENGTH, FIT_ORDER));

void rhs_t::operator()(const state_t& state_vec, state_t& dfdl, const double Lam) {
    int nranks = 1;
    int myrank = 0;
#ifdef MPI_PARALLEL
    MPI_Comm_size(MPI_COMM_WORLD, &nranks);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    if ( myrank == 0 ) {
        MPI_Bcast(const_cast<double *> (&Lam), 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        MPI_Bcast(&state_vec.gf_Sig()(0), state_vec.gf_Sig().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(&state_vec.gf_phi_pp()(0), state_vec.gf_phi_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(const_cast<dcomplex *> (&state_vec.gf_phi_ph()(0)), state_vec.gf_phi_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(const_cast<dcomplex *> (&state_vec.gf_phi_xph()(0)), state_vec.gf_phi_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(const_cast<dcomplex *> (&state_vec.gf_P_pp()(0)), state_vec.gf_P_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(const_cast<dcomplex *> (&state_vec.gf_P_ph()(0)), state_vec.gf_P_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(const_cast<dcomplex *> (&state_vec.gf_P_xph()(0)), state_vec.gf_P_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(const_cast<dcomplex *> (&state_vec.gf_chi_pp()(0)), state_vec.gf_chi_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(const_cast<dcomplex *> (&state_vec.gf_chi_ph()(0)), state_vec.gf_chi_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(const_cast<dcomplex *> (&state_vec.gf_chi_xph()(0)), state_vec.gf_chi_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(const_cast<dcomplex *> (&state_vec.gf_tri_sc()(0)), state_vec.gf_tri_sc().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(const_cast<dcomplex *> (&state_vec.gf_tri_d()(0)), state_vec.gf_tri_d().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(const_cast<dcomplex *> (&state_vec.gf_tri_m()(0)), state_vec.gf_tri_m().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(const_cast<dcomplex *> (&state_vec.gf_susc_sc()(0)), state_vec.gf_susc_sc().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(const_cast<dcomplex *> (&state_vec.gf_susc_d()(0)), state_vec.gf_susc_d().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(const_cast<dcomplex *> (&state_vec.gf_susc_m()(0)), state_vec.gf_susc_m().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(const_cast<dcomplex *> (&state_vec.gf_asytri_sc()(0)), state_vec.gf_asytri_sc().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(const_cast<dcomplex *> (&state_vec.gf_asytri_d()(0)), state_vec.gf_asytri_d().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(const_cast<dcomplex *> (&state_vec.gf_asytri_m()(0)), state_vec.gf_asytri_m().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    }
#endif
    if ( myrank == 0 ) cout << " Preprocessing at scale " << Lam << endl;

    // Precalculate Green function and single-scale propagator on frequency grid

    gf_1p_mat_t Gvec(POS_1P_RANGE, FFT_DIM * FFT_DIM);
    gf_1p_mat_t Svec(POS_1P_RANGE, FFT_DIM * FFT_DIM);

#ifdef MPI_PARALLEL
    gf_1p_mat_t Gvec_loc(POS_1P_RANGE, FFT_DIM * FFT_DIM);
    gf_1p_mat_t Svec_loc(POS_1P_RANGE, FFT_DIM * FFT_DIM);
    Gvec_loc.init(bind(&state_t::GMat, boost::cref(state_vec), _1, Lam), nranks, myrank); // Initialize big Green function vector 
    Svec_loc.init(bind(&state_t::SMat, boost::cref(state_vec), _1, Lam), nranks, myrank); // Initialize big Single scale vector 
    MPI_Reduce(&Gvec_loc(0), &Gvec(0), Gvec.num_elements() * Gvec_loc[0][0].size(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&Svec_loc(0), &Svec(0), Svec.num_elements() * Svec_loc[0][0].size(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Bcast(&Gvec(0), Gvec.num_elements() * Gvec_loc[0][0].size(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&Svec(0), Svec.num_elements() * Svec_loc[0][0].size(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
#else
    Gvec.init(bind(&state_t::GMat, boost::cref(state_vec), _1, Lam)); // Initialize big Green function vector 
    Svec.init(bind(&state_t::SMat, boost::cref(state_vec), _1, Lam)); // Initialize big Single scale vector 
#endif

    if ( myrank == 0 ) cout << " Rhs calculation " << endl;

    /*********************  Open MP parallelized RHS calculation  ********************/

#ifdef MPI_PARALLEL
    gf_1p_t gf_Sig_loc;
    symm_grp_sig.init(gf_Sig_loc, [&state_vec, &Svec, &Lam](const idx_1p_t & idx) {
        return eval_rhs_Sig(idx, state_vec, Svec, Lam); }, nranks, myrank);
    MPI_Reduce(&gf_Sig_loc(0), &dfdl.gf_Sig()(0), gf_Sig_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
#else
    symm_grp_sig.initFFT(dfdl.gf_Sig(), [&state_vec, &Svec, &Lam](const idx_1p_t & idx) {
        return eval_rhs_Sig(idx, state_vec, Svec, Lam); });
#endif

#ifdef KATANIN
    {
        gf_1p_mat_t SigDotvec(POS_1P_RANGE, FFT_DIM * FFT_DIM);
        SigDotvec.init(bind(&state_t::SigMat_big, boost::cref(dfdl), _1));
        Svec += Gvec * SigDotvec * Gvec; // Add Katanin correction
    }
#endif


    gf_1p_mat_real_t Gvec_real;
    gf_1p_mat_real_t Svec_real;
    gf_bubble_mat_t bubble_pp;
    gf_bubble_mat_t bubble_ph;
    gf_bubble_mat_t bubble_GG_pp;
    gf_bubble_mat_t bubble_GG_ph;

    if ( myrank == 0 ) cout << " ... bubble PT1 " << endl;
    fftw_complex *input, *output; // input output functions for the FFTW
    fftw_plan p, p_b; // plan for FFTW

    input = (fftw_complex*) fftw_malloc(sizeof (fftw_complex) * FFT_DIM * FFT_DIM);
    output = (fftw_complex*) fftw_malloc(sizeof (fftw_complex) * FFT_DIM * FFT_DIM);

    p = fftw_plan_dft_2d(FFT_DIM, FFT_DIM, input, output, FFTW_BACKWARD, FFTW_ESTIMATE); //plan for FFTW, same for Gvec and Svec (BACKWARD), different for bubble (FORWARD)

#ifdef MPI_PARALLEL
    gf_1p_mat_real_t Gvec_real_loc;
    gf_1p_mat_real_t Svec_real_loc;
    symm_grp_Gvec_real.init(Gvec_real_loc, [&Gvec, p, Lam](const idx_1p_mat_real_t & idx) {
        return eval_Gvec_real(idx, Gvec, p, Lam); }, nranks, myrank);
    symm_grp_Gvec_real.init(Svec_real_loc, [&Svec, p, Lam](const idx_1p_mat_real_t & idx) {
        return eval_Svec_real(idx, Svec, p, Lam); }, nranks, myrank); // Same symm as Gvec_real
    MPI_Reduce(&Gvec_real_loc(0), &Gvec_real(0), Gvec_real.num_elements() * Gvec_real_loc[0][0][0].size(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&Svec_real_loc(0), &Svec_real(0), Svec_real.num_elements() * Svec_real_loc[0][0][0].size(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Bcast(&Gvec_real(0), Gvec_real.num_elements() * Gvec_real_loc[0][0][0].size(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&Svec_real(0), Svec_real.num_elements() * Svec_real_loc[0][0][0].size(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
#else
    symm_grp_Gvec_real.initFFT(Gvec_real, [&Gvec, p, Lam](const idx_1p_mat_real_t & idx) {
        return eval_Gvec_real(idx, Gvec, p, Lam); });
    symm_grp_Gvec_real.initFFT(Svec_real, [&Svec, p, Lam](const idx_1p_mat_real_t & idx) {
        return eval_Svec_real(idx, Svec, p, Lam); }); // Same symm as Gvec_real
#endif

    if ( myrank == 0 ) cout << " ... bubble PT2 " << endl;

    p_b = fftw_plan_dft_2d(FFT_DIM, FFT_DIM, input, output, FFTW_FORWARD, FFTW_ESTIMATE); //plan for FFTW, same for Gvec and Svec (BACKWARD), different for bubble (FORWARD)

#ifdef MPI_PARALLEL
    gf_bubble_mat_t bubble_pp_loc;
    gf_bubble_mat_t bubble_ph_loc;
    gf_bubble_mat_t bubble_GG_pp_loc;
    gf_bubble_mat_t bubble_GG_ph_loc;
    symm_grp_bubble_pp.init(bubble_pp_loc, [&Svec_real, &Gvec_real, p_b, Lam](const idx_bubble_mat_t & idx) {
        return eval_diag_bubble_pp(idx, Svec_real, Gvec_real, p_b, Lam); }, nranks, myrank);
    symm_grp_bubble_ph.init(bubble_ph_loc, [&Svec_real, &Gvec_real, p_b, Lam](const idx_bubble_mat_t & idx) {
        return eval_diag_bubble_ph(idx, Svec_real, Gvec_real, p_b, Lam); }, nranks, myrank);
    symm_grp_bubble_pp.init(bubble_GG_pp_loc, [&Gvec_real, p_b, Lam](const idx_bubble_mat_t & idx) {
        return eval_diag_bubble_GG_pp(idx, Gvec_real, p_b, Lam); }, nranks, myrank);
    symm_grp_bubble_ph.init(bubble_GG_ph_loc, [&Gvec_real, p_b, Lam](const idx_bubble_mat_t & idx) {
        return eval_diag_bubble_GG_ph(idx, Gvec_real, p_b, Lam); }, nranks, myrank);
    MPI_Reduce(&bubble_pp_loc(0), &bubble_pp(0), bubble_pp.num_elements() * bubble_pp_loc[0][0][0][0][0][0][0][0].size(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&bubble_ph_loc(0), &bubble_ph(0), bubble_ph.num_elements() * bubble_ph_loc[0][0][0][0][0][0][0][0].size(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&bubble_GG_pp_loc(0), &bubble_GG_pp(0), bubble_GG_pp.num_elements() * bubble_GG_pp_loc[0][0][0][0][0][0][0][0].size(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&bubble_GG_ph_loc(0), &bubble_GG_ph(0), bubble_GG_ph.num_elements() * bubble_GG_ph_loc[0][0][0][0][0][0][0][0].size(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Bcast(&bubble_pp(0), bubble_pp.num_elements() * bubble_pp_loc[0][0][0][0][0][0][0][0].size(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&bubble_ph(0), bubble_ph.num_elements() * bubble_ph_loc[0][0][0][0][0][0][0][0].size(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&bubble_GG_pp(0), bubble_GG_pp.num_elements() * bubble_GG_pp_loc[0][0][0][0][0][0][0][0].size(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&bubble_GG_ph(0), bubble_GG_ph.num_elements() * bubble_GG_ph_loc[0][0][0][0][0][0][0][0].size(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
#else 
    symm_grp_bubble_pp.initFFT(bubble_pp, [&Svec_real, &Gvec_real, p_b, Lam](const idx_bubble_mat_t & idx) {
        return eval_diag_bubble_pp(idx, Svec_real, Gvec_real, p_b, Lam); });
    symm_grp_bubble_ph.initFFT(bubble_ph, [&Svec_real, &Gvec_real, p_b, Lam](const idx_bubble_mat_t & idx) {
        return eval_diag_bubble_ph(idx, Svec_real, Gvec_real, p_b, Lam); });
    symm_grp_bubble_pp.initFFT(bubble_GG_pp, [&Gvec_real, p_b, Lam](const idx_bubble_mat_t & idx) {
        return eval_diag_bubble_GG_pp(idx, Gvec_real, p_b, Lam); });
    symm_grp_bubble_ph.initFFT(bubble_GG_ph, [&Gvec_real, p_b, Lam](const idx_bubble_mat_t & idx) {
        return eval_diag_bubble_GG_ph(idx, Gvec_real, p_b, Lam); });
#endif

    fftw_destroy_plan(p);
    fftw_destroy_plan(p_b);
    fftw_free(input);
    fftw_free(output);

    // --- CONVENTIONAL FLOW
#pragma omp parallel shared(dfdl,bubble_pp,bubble_ph,state_vec)
    {//global parallelization
#ifdef MPI_PARALLEL
        if ( myrank == 0 ) cout << " ... phi " << endl;
        gf_phi_t gf_phi_pp_loc;
        gf_phi_t gf_phi_ph_loc;
        gf_phi_t gf_phi_xph_loc;
        symm_grp_phi_pp.init(gf_phi_pp_loc, [&state_vec, &bubble_pp, Lam](const idx_phi_t & idx) {
            return eval_diag_pp(idx, state_vec, bubble_pp, Lam); }, nranks, myrank);
        symm_grp_phi_ph.init(gf_phi_ph_loc, [&state_vec, &bubble_ph, Lam](const idx_phi_t & idx) {
            return eval_diag_ph(idx, state_vec, bubble_ph, Lam); }, nranks, myrank);
        symm_grp_phi_xph.init(gf_phi_xph_loc, [&state_vec, &bubble_ph, Lam](const idx_phi_t & idx) {
            return eval_diag_xph(idx, state_vec, bubble_ph, Lam); }, nranks, myrank);
        MPI_Reduce(&gf_phi_pp_loc(0), &dfdl.gf_phi_pp()(0), gf_phi_pp_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_phi_ph_loc(0), &dfdl.gf_phi_ph()(0), gf_phi_ph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_phi_xph_loc(0), &dfdl.gf_phi_xph()(0), gf_phi_xph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);

        if ( myrank == 0 ) cout << " ... chi " << endl;
        gf_chi_t gf_chi_pp_loc;
        gf_chi_t gf_chi_ph_loc;
        gf_chi_t gf_chi_xph_loc;
        symm_grp_chi_pp.init(gf_chi_pp_loc, [&state_vec, &bubble_pp, Lam](const idx_chi_t & idx) {
            return eval_diag_pp(ichi_to_iphi(idx), state_vec, bubble_pp, Lam); }, nranks, myrank);
        symm_grp_chi_ph.init(gf_chi_ph_loc, [&state_vec, &bubble_ph, Lam](const idx_chi_t & idx) {
            return eval_diag_ph(ichi_to_iphi(idx), state_vec, bubble_ph, Lam); }, nranks, myrank);
        symm_grp_chi_xph.init(gf_chi_xph_loc, [&state_vec, &bubble_ph, Lam](const idx_chi_t & idx) {
            return eval_diag_xph(ichi_to_iphi(idx), state_vec, bubble_ph, Lam); }, nranks, myrank);
        MPI_Reduce(&gf_chi_pp_loc(0), &dfdl.gf_chi_pp()(0), gf_chi_pp_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_chi_ph_loc(0), &dfdl.gf_chi_ph()(0), gf_chi_ph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_chi_xph_loc(0), &dfdl.gf_chi_xph()(0), gf_chi_xph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);

        if ( myrank == 0 ) cout << " ... P " << endl << endl;
        gf_P_t gf_P_pp_loc;
        gf_P_t gf_P_ph_loc;
        gf_P_t gf_P_xph_loc;
        symm_grp_P_pp.init(gf_P_pp_loc, [&state_vec, &bubble_pp, Lam](const idx_P_t & idx) {
            return eval_diag_pp(iP_to_iphi(idx), state_vec, bubble_pp, Lam); }, nranks, myrank);
        symm_grp_P_ph.init(gf_P_ph_loc, [&state_vec, &bubble_ph, Lam](const idx_P_t & idx) {
            return eval_diag_ph(iP_to_iphi(idx), state_vec, bubble_ph, Lam); }, nranks, myrank);
        symm_grp_P_xph.init(gf_P_xph_loc, [&state_vec, &bubble_ph, Lam](const idx_P_t & idx) {
            return eval_diag_xph(iP_to_iphi(idx), state_vec, bubble_ph, Lam); }, nranks, myrank);
        MPI_Reduce(&gf_P_pp_loc(0), &dfdl.gf_P_pp()(0), gf_P_pp_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_P_ph_loc(0), &dfdl.gf_P_ph()(0), gf_P_ph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_P_xph_loc(0), &dfdl.gf_P_xph()(0), gf_P_xph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);


        if ( myrank == 0 ) cout << " ... trileg asymptotics" << endl;
        gf_asytri_t gf_asytri_sc_loc;
        gf_asytri_t gf_asytri_d_loc;
        gf_asytri_t gf_asytri_m_loc;
        symm_grp_asytri_sc.init(gf_asytri_sc_loc, [&state_vec, &bubble_pp, Lam](const idx_asytri_t & idx) {
            return eval_diag_tri_sc(iasy_to_itri(idx), state_vec, bubble_pp, Lam); }, nranks, myrank);
        symm_grp_asytri_d.init(gf_asytri_d_loc, [&state_vec, &bubble_ph, Lam](const idx_asytri_t & idx) {
            return eval_diag_tri_d(iasy_to_itri(idx), state_vec, bubble_ph, Lam); }, nranks, myrank);
        symm_grp_asytri_m.init(gf_asytri_m_loc, [&state_vec, &bubble_ph, Lam](const idx_asytri_t & idx) {
            return eval_diag_tri_m(iasy_to_itri(idx), state_vec, bubble_ph, Lam); }, nranks, myrank);
        MPI_Reduce(&gf_asytri_sc_loc(0), &dfdl.gf_asytri_sc()(0), gf_asytri_sc_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_asytri_d_loc(0), &dfdl.gf_asytri_d()(0), gf_asytri_d_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_asytri_m_loc(0), &dfdl.gf_asytri_m()(0), gf_asytri_m_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);

        if ( myrank == 0 ) cout << " ... trileg " << endl;
        gf_tri_t gf_tri_sc_loc;
        gf_tri_t gf_tri_d_loc;
        gf_tri_t gf_tri_m_loc;
        symm_grp_tri_sc.init(gf_tri_sc_loc, [&state_vec, &bubble_pp, Lam](const idx_tri_t & idx) {
            return eval_diag_tri_sc(idx, state_vec, bubble_pp, Lam); }, nranks, myrank);
        symm_grp_tri_d.init(gf_tri_d_loc, [&state_vec, &bubble_ph, Lam](const idx_tri_t & idx) {
            return eval_diag_tri_d(idx, state_vec, bubble_ph, Lam); }, nranks, myrank);
        symm_grp_tri_m.init(gf_tri_m_loc, [&state_vec, &bubble_ph, Lam](const idx_tri_t & idx) {
            return eval_diag_tri_m(idx, state_vec, bubble_ph, Lam); }, nranks, myrank);
        MPI_Reduce(&gf_tri_sc_loc(0), &dfdl.gf_tri_sc()(0), gf_tri_sc_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_tri_d_loc(0), &dfdl.gf_tri_d()(0), gf_tri_d_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_tri_m_loc(0), &dfdl.gf_tri_m()(0), gf_tri_m_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);

        if ( myrank == 0 ) cout << " ... susc " << endl << endl;
        gf_susc_t gf_susc_sc_loc;
        gf_susc_t gf_susc_d_loc;
        gf_susc_t gf_susc_m_loc;
        symm_grp_susc_sc.init(gf_susc_sc_loc, [&state_vec, &bubble_pp, Lam](const idx_susc_t & idx) {
            return eval_diag_susc_sc(idx, state_vec, bubble_pp, Lam); }, nranks, myrank);
        symm_grp_susc_d.init(gf_susc_d_loc, [&state_vec, &bubble_ph, Lam](const idx_susc_t & idx) {
            return eval_diag_susc_d(idx, state_vec, bubble_ph, Lam); }, nranks, myrank);
        symm_grp_susc_m.init(gf_susc_m_loc, [&state_vec, &bubble_ph, Lam](const idx_susc_t & idx) {
            return eval_diag_susc_m(idx, state_vec, bubble_ph, Lam); }, nranks, myrank);
        MPI_Reduce(&gf_susc_sc_loc(0), &dfdl.gf_susc_sc()(0), gf_susc_sc_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_susc_d_loc(0), &dfdl.gf_susc_d()(0), gf_susc_d_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_susc_m_loc(0), &dfdl.gf_susc_m()(0), gf_susc_m_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
#else
        if ( myrank == 0 ) cout << " ... phi " << endl;
        symm_grp_phi_pp.init(dfdl.gf_phi_pp(), [&state_vec, &bubble_pp, Lam](const idx_phi_t & idx) {
            return eval_diag_pp(idx, state_vec, bubble_pp, Lam); });
        symm_grp_phi_ph.init(dfdl.gf_phi_ph(), [&state_vec, &bubble_ph, Lam](const idx_phi_t & idx) {
            return eval_diag_ph(idx, state_vec, bubble_ph, Lam); });
        symm_grp_phi_xph.init(dfdl.gf_phi_xph(), [&state_vec, &bubble_ph, Lam](const idx_phi_t & idx) {
            return eval_diag_xph(idx, state_vec, bubble_ph, Lam); });

        if ( myrank == 0 ) cout << " ... chi " << endl;
        symm_grp_chi_pp.init(dfdl.gf_chi_pp(), [&state_vec, &bubble_pp, Lam](const idx_chi_t & idx) {
            return eval_diag_pp(ichi_to_iphi(idx), state_vec, bubble_pp, Lam); });
        symm_grp_chi_ph.init(dfdl.gf_chi_ph(), [&state_vec, &bubble_ph, Lam](const idx_chi_t & idx) {
            return eval_diag_ph(ichi_to_iphi(idx), state_vec, bubble_ph, Lam); });
        symm_grp_chi_xph.init(dfdl.gf_chi_xph(), [&state_vec, &bubble_ph, Lam](const idx_chi_t & idx) {
            return eval_diag_xph(ichi_to_iphi(idx), state_vec, bubble_ph, Lam); });

#pragma omp barrier //necessary since prior results are used to compute P

        if ( myrank == 0 ) cout << " ... P " << endl << endl;
        symm_grp_P_pp.init(dfdl.gf_P_pp(), [&state_vec, &dfdl, &bubble_pp, Lam](const idx_P_t & idx) {
            return eval_diag_pp(iP_to_iphi(idx), state_vec, bubble_pp, Lam) - double(idx(IP::n) == 0) * dfdl.gf_chi_pp()(iP_to_ichi(idx)); });
        symm_grp_P_ph.init(dfdl.gf_P_ph(), [&state_vec, &dfdl, &bubble_ph, Lam](const idx_P_t & idx) {
            return eval_diag_ph(iP_to_iphi(idx), state_vec, bubble_ph, Lam) - double(idx(IP::n) == 0) * dfdl.gf_chi_ph()(iP_to_ichi(idx)); });
        symm_grp_P_xph.init(dfdl.gf_P_xph(), [&state_vec, &dfdl, &bubble_ph, Lam](const idx_P_t & idx) {
            return eval_diag_xph(iP_to_iphi(idx), state_vec, bubble_ph, Lam) - double(idx(IP::n) == 0) * dfdl.gf_chi_xph()(iP_to_ichi(idx)); });

        if ( myrank == 0 ) cout << " ... trileg asymptotics" << endl;
        symm_grp_asytri_sc.init(dfdl.gf_asytri_sc(), [&state_vec, &bubble_pp, Lam](const idx_asytri_t & idx) {
            return eval_diag_tri_sc(iasy_to_itri(idx), state_vec, bubble_pp, Lam); });
        symm_grp_asytri_d.init(dfdl.gf_asytri_d(), [&state_vec, &bubble_ph, Lam](const idx_asytri_t & idx) {
            return eval_diag_tri_d(iasy_to_itri(idx), state_vec, bubble_ph, Lam); });
        symm_grp_asytri_m.init(dfdl.gf_asytri_m(), [&state_vec, &bubble_ph, Lam](const idx_asytri_t & idx) {
            return eval_diag_tri_m(iasy_to_itri(idx), state_vec, bubble_ph, Lam); });

        if ( myrank == 0 ) cout << " ... trileg " << endl;
        symm_grp_tri_sc.init(dfdl.gf_tri_sc(), [&state_vec, &bubble_pp, Lam](const idx_tri_t & idx) {
            return eval_diag_tri_sc(idx, state_vec, bubble_pp, Lam); });
        symm_grp_tri_d.init(dfdl.gf_tri_d(), [&state_vec, &bubble_ph, Lam](const idx_tri_t & idx) {
            return eval_diag_tri_d(idx, state_vec, bubble_ph, Lam); });
        symm_grp_tri_m.init(dfdl.gf_tri_m(), [&state_vec, &bubble_ph, Lam](const idx_tri_t & idx) {
            return eval_diag_tri_m(idx, state_vec, bubble_ph, Lam); });

        if ( myrank == 0 ) cout << " ... susc " << endl << endl;
        symm_grp_susc_sc.init(dfdl.gf_susc_sc(), [&state_vec, &bubble_pp, Lam](const idx_susc_t & idx) {
            return eval_diag_susc_sc(idx, state_vec, bubble_pp, Lam); });
        symm_grp_susc_d.init(dfdl.gf_susc_d(), [&state_vec, &bubble_ph, Lam](const idx_susc_t & idx) {
            return eval_diag_susc_d(idx, state_vec, bubble_ph, Lam); });
        symm_grp_susc_m.init(dfdl.gf_susc_m(), [&state_vec, &bubble_ph, Lam](const idx_susc_t & idx) {
            return eval_diag_susc_m(idx, state_vec, bubble_ph, Lam); });
#endif
    }//end outer parallel loop

#ifdef MULTILOOP
    state_t dfdl_old, dfdl_oldold;
    if ( myrank == 0 ) {
        dfdl_old = dfdl;
    } //TODO: or dfdl_oldold=dfdl???
#endif


#ifdef TWOLOOP

    // --- Add two loop corrections

    cout << " Two-loop corrections ... " << endl;
    state_t dfdl_R, dfdl_L, dfdl_T;

#ifdef MPI_PARALLEL

    // cast dfdl to all nodes   //TODO: check if all of them are needed
    MPI_Bcast(&dfdl.gf_phi_pp()(0), dfdl.gf_phi_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl.gf_phi_ph()(0), dfdl.gf_phi_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl.gf_phi_xph()(0), dfdl.gf_phi_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl.gf_P_pp()(0), dfdl.gf_P_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl.gf_P_ph()(0), dfdl.gf_P_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl.gf_P_xph()(0), dfdl.gf_P_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl.gf_chi_pp()(0), dfdl.gf_chi_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl.gf_chi_ph()(0), dfdl.gf_chi_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl.gf_chi_xph()(0), dfdl.gf_chi_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);

    // start calculation 2l 
    if ( myrank == 0 )cout << " ... phi left " << endl;
    gf_phi_pp_loc.init(phi_zero, 1, 0);
    gf_phi_ph_loc.init(phi_zero, 1, 0);
    gf_phi_xph_loc.init(phi_zero, 1, 0);
    symm_grp_phi_pp_L.init(gf_phi_pp_loc, [&state_vec, &dfdl, &bubble_GG_pp, &Gvec, Lam](const idx_phi_t & idx) {
        return eval_diag_pp_L(idx, state_vec, dfdl, bubble_GG_pp, Lam); }, nranks, myrank);
    symm_grp_phi_ph_L.init(gf_phi_ph_loc, [&state_vec, &dfdl, &bubble_GG_ph, &Gvec, Lam](const idx_phi_t & idx) {
        return eval_diag_ph_L(idx, state_vec, dfdl, bubble_GG_ph, Lam); }, nranks, myrank);
    symm_grp_phi_ph_L.init(gf_phi_xph_loc, [&state_vec, &dfdl, &bubble_GG_ph, &Gvec, Lam](const idx_phi_t & idx) {
        return eval_diag_xph_L(idx, state_vec, dfdl, bubble_GG_ph, Lam); }, nranks, myrank);
    MPI_Reduce(&gf_phi_pp_loc(0), &dfdl_L.gf_phi_pp()(0), gf_phi_pp_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&gf_phi_ph_loc(0), &dfdl_L.gf_phi_ph()(0), gf_phi_ph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&gf_phi_xph_loc(0), &dfdl_L.gf_phi_xph()(0), gf_phi_xph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);

    if ( myrank == 0 )cout << " ... P left " << endl << endl;
    gf_P_pp_loc.init(P_zero, 1, 0);
    gf_P_ph_loc.init(P_zero, 1, 0);
    gf_P_xph_loc.init(P_zero, 1, 0);
    symm_grp_P_pp.init(gf_P_pp_loc, [&state_vec, &dfdl, &dfdl_L, &bubble_GG_pp, Lam](const idx_P_t & idx) {
        return eval_diag_pp_L(iP_to_iphi(idx), state_vec, dfdl, bubble_GG_pp, Lam); }, nranks, myrank);
    symm_grp_P_ph.init(gf_P_ph_loc, [&state_vec, &dfdl, &dfdl_L, &bubble_GG_ph, Lam](const idx_P_t & idx) {
        return eval_diag_ph_L(iP_to_iphi(idx), state_vec, dfdl, bubble_GG_ph, Lam); }, nranks, myrank);
    symm_grp_P_xph.init(gf_P_xph_loc, [&state_vec, &dfdl, &dfdl_L, &bubble_GG_ph, Lam](const idx_P_t & idx) {
        return eval_diag_xph_L(iP_to_iphi(idx), state_vec, dfdl, bubble_GG_ph, Lam); }, nranks, myrank);
    MPI_Reduce(&gf_P_pp_loc(0), &dfdl_L.gf_P_pp()(0), gf_P_pp_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&gf_P_ph_loc(0), &dfdl_L.gf_P_ph()(0), gf_P_ph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&gf_P_xph_loc(0), &dfdl_L.gf_P_xph()(0), gf_P_xph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);

    if ( myrank == 0 )cout << " ... phi right " << endl;
    gf_phi_pp_loc.init(phi_zero, 1, 0);
    gf_phi_ph_loc.init(phi_zero, 1, 0);
    gf_phi_xph_loc.init(phi_zero, 1, 0);
    symm_grp_phi_pp_L.init(gf_phi_pp_loc, [&state_vec, &dfdl, &bubble_GG_pp, &Gvec, Lam](const idx_phi_t & idx) {
        return eval_diag_pp_R(idx, state_vec, dfdl, bubble_GG_pp, Lam); }, nranks, myrank);
    symm_grp_phi_ph_L.init(gf_phi_ph_loc, [&state_vec, &dfdl, &bubble_GG_ph, &Gvec, Lam](const idx_phi_t & idx) {
        return eval_diag_ph_R(idx, state_vec, dfdl, bubble_GG_ph, Lam); }, nranks, myrank);
    symm_grp_phi_xph_L.init(gf_phi_xph_loc, [&state_vec, &dfdl, &bubble_GG_ph, &Gvec, Lam](const idx_phi_t & idx) {
        return eval_diag_xph_R(idx, state_vec, dfdl, bubble_GG_ph, Lam); }, nranks, myrank);
    MPI_Reduce(&gf_phi_pp_loc(0), &dfdl_R.gf_phi_pp()(0), gf_phi_pp_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&gf_phi_ph_loc(0), &dfdl_R.gf_phi_ph()(0), gf_phi_ph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&gf_phi_xph_loc(0), &dfdl_R.gf_phi_xph()(0), gf_phi_xph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);

#ifndef MULTILOOP  
    if ( myrank == 0 )cout << " ... trileg " << endl << endl;
    gf_tri_sc_loc.init(tri_zero, 1, 0);
    gf_tri_d_loc.init(tri_zero, 1, 0);
    gf_tri_m_loc.init(tri_zero, 1, 0);
    symm_grp_tri_sc.init(gf_tri_sc_loc, [&state_vec, &dfdl, &bubble_GG_pp, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_sc_R(idx, state_vec, dfdl, bubble_GG_pp, Lam); }, nranks, myrank);
    symm_grp_tri_d.init(gf_tri_d_loc, [&state_vec, &dfdl, &bubble_GG_ph, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_d_R(idx, state_vec, dfdl, bubble_GG_ph, Lam); }, nranks, myrank);
    symm_grp_tri_m.init(gf_tri_m_loc, [&state_vec, &dfdl, &bubble_GG_ph, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_m_R(idx, state_vec, dfdl, bubble_GG_ph, Lam); }, nranks, myrank);
    MPI_Reduce(&gf_tri_sc_loc(0), &dfdl_R.gf_tri_sc()(0), gf_tri_sc_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&gf_tri_d_loc(0), &dfdl_R.gf_tri_d()(0), gf_tri_d_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&gf_tri_m_loc(0), &dfdl_R.gf_tri_m()(0), gf_tri_m_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);

#endif

#else
    cout << " ... phi left " << endl;
    symm_grp_phi_pp_L.init(dfdl_L.gf_phi_pp(), [&state_vec, &dfdl, &bubble_GG_pp, &Gvec, Lam](const idx_phi_t & idx) {
        return eval_diag_pp_L(idx, state_vec, dfdl, bubble_GG_pp, Lam); });
    symm_grp_phi_ph_L.init(dfdl_L.gf_phi_ph(), [&state_vec, &dfdl, &bubble_GG_ph, &Gvec, Lam](const idx_phi_t & idx) {
        return eval_diag_ph_L(idx, state_vec, dfdl, bubble_GG_ph, Lam); });
    symm_grp_phi_xph_L.init(dfdl_L.gf_phi_xph(), [&state_vec, &dfdl, &bubble_GG_ph, &Gvec, Lam](const idx_phi_t & idx) {
        return eval_diag_xph_L(idx, state_vec, dfdl, bubble_GG_ph, Lam); });

    cout << " ... P left " << endl << endl;
    symm_grp_P_pp.init(dfdl_L.gf_P_pp(), [&state_vec, &dfdl, &bubble_GG_pp, Lam](const idx_P_t & idx) {
        return eval_diag_pp_L(iP_to_iphi(idx), state_vec, dfdl, bubble_GG_pp, Lam); });
    symm_grp_P_ph.init(dfdl_L.gf_P_ph(), [&state_vec, &dfdl, &bubble_GG_ph, Lam](const idx_P_t & idx) {
        return eval_diag_ph_L(iP_to_iphi(idx), state_vec, dfdl, bubble_GG_ph, Lam); });
    symm_grp_P_xph.init(dfdl_L.gf_P_xph(), [&state_vec, &dfdl, &bubble_GG_ph, Lam](const idx_P_t & idx) {
        return eval_diag_xph_L(iP_to_iphi(idx), state_vec, dfdl, bubble_GG_ph, Lam); });

    cout << " ... phi right " << endl;
    symm_grp_phi_pp_L.init(dfdl_R.gf_phi_pp(), [&state_vec, &dfdl, &bubble_GG_pp, &Gvec, Lam](const idx_phi_t & idx) {
        return eval_diag_pp_R(idx, state_vec, dfdl, bubble_GG_pp, Lam); });
    symm_grp_phi_ph_L.init(dfdl_R.gf_phi_ph(), [&state_vec, &dfdl, &bubble_GG_ph, &Gvec, Lam](const idx_phi_t & idx) {
        return eval_diag_ph_R(idx, state_vec, dfdl, bubble_GG_ph, Lam); });
    symm_grp_phi_xph_L.init(dfdl_R.gf_phi_xph(), [&state_vec, &dfdl, &bubble_GG_ph, &Gvec, Lam](const idx_phi_t & idx) {
        return eval_diag_xph_R(idx, state_vec, dfdl, bubble_GG_ph, Lam); });

#ifndef MULTILOOP
    cout << " ... trileg " << endl << endl;
    symm_grp_tri_sc.init(dfdl_R.gf_tri_sc(), [&state_vec, &dfdl, &bubble_GG_pp, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_sc_R(idx, state_vec, dfdl, bubble_GG_pp, Lam); });
    symm_grp_tri_d.init(dfdl_R.gf_tri_d(), [&state_vec, &dfdl, &bubble_GG_ph, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_d_R(idx, state_vec, dfdl, bubble_GG_ph, Lam); });
    symm_grp_tri_m.init(dfdl_R.gf_tri_m(), [&state_vec, &dfdl, &bubble_GG_ph, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_m_R(idx, state_vec, dfdl, bubble_GG_ph, Lam); });

#endif

#endif  // ends MPI_PARALLEL

    if ( myrank == 0 ) {
        dfdl_T = dfdl_L + dfdl_R;
        dfdl += dfdl_T;
    }

#endif  // ends TWOLOOP

#ifdef MULTILOOP
    if ( myrank == 0 )cout << " Starting the Multiloop fRG " << endl;

    state_t dfdl_C_sum, dfdl_L_sum;
    state_t dfdl_oldL, dfdl_oldT;
    state_t dfdl_C;


    int count = 0;

    do {
        count++;
        if ( myrank == 0 )cout << " Loop number: " << count + 2 << endl;

        if ( myrank == 0 ) {
            dfdl_L_sum += dfdl_L; //It doesn't get the last loop contribution from the left diagrams
            dfdl_oldold = dfdl_old; // dfdl at two previous loop-steps
            dfdl_oldL = dfdl_L;
            dfdl_oldT = dfdl_T;
        }

#ifdef MPI_PARALLEL
        if ( myrank == 0 )cout << " ... CENTRAL MULTILOOP DIAGRAMS ... " << endl;

        MPI_Bcast(&dfdl_oldL.gf_phi_pp()(0), dfdl_oldL.gf_phi_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(&dfdl_oldL.gf_phi_ph()(0), dfdl_oldL.gf_phi_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(&dfdl_oldL.gf_phi_xph()(0), dfdl_oldL.gf_phi_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(&dfdl_oldL.gf_P_pp()(0), dfdl_oldL.gf_P_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(&dfdl_oldL.gf_P_ph()(0), dfdl_oldL.gf_P_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(&dfdl_oldL.gf_P_xph()(0), dfdl_oldL.gf_P_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(&dfdl_oldL.gf_chi_pp()(0), dfdl_oldL.gf_chi_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(&dfdl_oldL.gf_chi_ph()(0), dfdl_oldL.gf_chi_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(&dfdl_oldL.gf_chi_xph()(0), dfdl_oldL.gf_chi_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);

        if ( myrank == 0 )cout << " ... phi " << endl;
        gf_phi_pp_loc.init(phi_zero, 1, 0);
        gf_phi_ph_loc.init(phi_zero, 1, 0);
        gf_phi_xph_loc.init(phi_zero, 1, 0);
        symm_grp_phi_pp.init(gf_phi_pp_loc, [&state_vec, &dfdl_oldL, &bubble_GG_pp, Lam](const idx_phi_t & idx) {
            return eval_diag_pp_C(idx, state_vec, dfdl_oldL, bubble_GG_pp, Lam); }, nranks, myrank);
        symm_grp_phi_ph.init(gf_phi_ph_loc, [&state_vec, &dfdl_oldL, &bubble_GG_ph, Lam](const idx_phi_t & idx) {
            return eval_diag_ph_C(idx, state_vec, dfdl_oldL, bubble_GG_ph, Lam); }, nranks, myrank);
        symm_grp_phi_xph.init(gf_phi_xph_loc, [&state_vec, &dfdl_oldL, &bubble_GG_ph, Lam](const idx_phi_t & idx) {
            return eval_diag_xph_C(idx, state_vec, dfdl_oldL, bubble_GG_ph, Lam); }, nranks, myrank);
        MPI_Reduce(&gf_phi_pp_loc(0), &dfdl_C.gf_phi_pp()(0), gf_phi_pp_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_phi_ph_loc(0), &dfdl_C.gf_phi_ph()(0), gf_phi_ph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_phi_xph_loc(0), &dfdl_C.gf_phi_xph()(0), gf_phi_xph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);

        if ( myrank == 0 )cout << " ... chi " << endl;
        gf_chi_pp_loc.init(chi_zero, 1, 0);
        gf_chi_ph_loc.init(chi_zero, 1, 0);
        gf_chi_xph_loc.init(chi_zero, 1, 0);
        symm_grp_chi_pp.init(gf_chi_pp_loc, [&state_vec, &dfdl_oldL, &bubble_GG_pp, Lam](const idx_chi_t & idx) {
            return eval_diag_pp_C(ichi_to_iphi(idx), state_vec, dfdl_oldL, bubble_GG_pp, Lam); }, nranks, myrank);
        symm_grp_chi_ph.init(gf_chi_ph_loc, [&state_vec, &dfdl_oldL, &bubble_GG_ph, Lam](const idx_chi_t & idx) {
            return eval_diag_ph_C(ichi_to_iphi(idx), state_vec, dfdl_oldL, bubble_GG_ph, Lam); }, nranks, myrank);
        symm_grp_chi_xph.init(gf_chi_xph_loc, [&state_vec, &dfdl_oldL, &bubble_GG_ph, Lam](const idx_chi_t & idx) {
            return eval_diag_xph_C(ichi_to_iphi(idx), state_vec, dfdl_oldL, bubble_GG_ph, Lam); }, nranks, myrank);
        MPI_Reduce(&gf_chi_pp_loc(0), &dfdl_C.gf_chi_pp()(0), gf_chi_pp_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_chi_ph_loc(0), &dfdl_C.gf_chi_ph()(0), gf_chi_ph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_chi_xph_loc(0), &dfdl_C.gf_chi_xph()(0), gf_chi_xph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);

        if ( myrank == 0 )cout << " ... P " << endl << endl;
        gf_P_pp_loc.init(P_zero, 1, 0);
        gf_P_ph_loc.init(P_zero, 1, 0);
        gf_P_xph_loc.init(P_zero, 1, 0);
        symm_grp_P_pp.init(gf_P_pp_loc, [&state_vec, &dfdl_oldL, &bubble_GG_pp, Lam](const idx_P_t & idx) {
            return eval_diag_pp_C(iP_to_iphi(idx), state_vec, dfdl_oldL, bubble_GG_pp, Lam); }, nranks, myrank);
        symm_grp_P_ph.init(gf_P_ph_loc, [&state_vec, &dfdl_oldL, &bubble_GG_ph, Lam](const idx_P_t & idx) {
            return eval_diag_ph_C(iP_to_iphi(idx), state_vec, dfdl_oldL, bubble_GG_ph, Lam); }, nranks, myrank);
        symm_grp_P_xph.init(gf_P_xph_loc, [&state_vec, &dfdl_oldL, &bubble_GG_ph, Lam](const idx_P_t & idx) {
            return eval_diag_xph_C(iP_to_iphi(idx), state_vec, dfdl_oldL, bubble_GG_ph, Lam); }, nranks, myrank);
        MPI_Reduce(&gf_P_pp_loc(0), &dfdl_C.gf_P_pp()(0), gf_P_pp_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_P_ph_loc(0), &dfdl_C.gf_P_ph()(0), gf_P_ph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_P_xph_loc(0), &dfdl_C.gf_P_xph()(0), gf_P_xph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);


        if ( myrank == 0 )cout << " ... LEFT MULTILOOP DIAGRAMS ... " << endl;
        MPI_Bcast(&dfdl_oldT.gf_phi_pp()(0), dfdl_oldT.gf_phi_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(&dfdl_oldT.gf_phi_ph()(0), dfdl_oldT.gf_phi_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(&dfdl_oldT.gf_phi_xph()(0), dfdl_oldT.gf_phi_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(&dfdl_oldT.gf_P_pp()(0), dfdl_oldT.gf_P_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(&dfdl_oldT.gf_P_ph()(0), dfdl_oldT.gf_P_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(&dfdl_oldT.gf_P_xph()(0), dfdl_oldT.gf_P_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(&dfdl_oldT.gf_chi_pp()(0), dfdl_oldT.gf_chi_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(&dfdl_oldT.gf_chi_ph()(0), dfdl_oldT.gf_chi_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
        MPI_Bcast(&dfdl_oldT.gf_chi_xph()(0), dfdl_oldT.gf_chi_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);

        if ( myrank == 0 )cout << " ... phi " << endl;
        gf_phi_pp_loc.init(phi_zero, 1, 0);
        gf_phi_ph_loc.init(phi_zero, 1, 0);
        gf_phi_xph_loc.init(phi_zero, 1, 0);
        symm_grp_phi_pp_L.init(gf_phi_pp_loc, [&state_vec, &dfdl_oldT, &bubble_GG_pp, Lam](const idx_phi_t & idx) {
            return eval_diag_pp_L(idx, state_vec, dfdl_oldT, bubble_GG_pp, Lam); }, nranks, myrank);
        symm_grp_phi_ph_L.init(gf_phi_ph_loc, [&state_vec, &dfdl_oldT, &bubble_GG_ph, Lam](const idx_phi_t & idx) {
            return eval_diag_ph_L(idx, state_vec, dfdl_oldT, bubble_GG_ph, Lam); }, nranks, myrank);
        symm_grp_phi_xph_L.init(gf_phi_xph_loc, [&state_vec, &dfdl_oldT, &bubble_GG_ph, Lam](const idx_phi_t & idx) {
            return eval_diag_xph_L(idx, state_vec, dfdl_oldT, bubble_GG_ph, Lam); }, nranks, myrank);
        MPI_Reduce(&gf_phi_pp_loc(0), &dfdl_L.gf_phi_pp()(0), gf_phi_pp_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_phi_ph_loc(0), &dfdl_L.gf_phi_ph()(0), gf_phi_ph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_phi_xph_loc(0), &dfdl_L.gf_phi_xph()(0), gf_phi_xph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);

        if ( myrank == 0 )cout << " ... P " << endl << endl;
        gf_P_pp_loc.init(P_zero, 1, 0);
        gf_P_ph_loc.init(P_zero, 1, 0);
        gf_P_xph_loc.init(P_zero, 1, 0);
        symm_grp_P_pp.init(gf_P_pp_loc, [&state_vec, &dfdl_oldT, &bubble_GG_pp, Lam](const idx_P_t & idx) {
            return eval_diag_pp_L(iP_to_iphi(idx), state_vec, dfdl_oldT, bubble_GG_pp, Lam); }, nranks, myrank);
        symm_grp_P_ph.init(gf_P_ph_loc, [&state_vec, &dfdl_oldT, &bubble_GG_ph, Lam](const idx_P_t & idx) {
            return eval_diag_ph_L(iP_to_iphi(idx), state_vec, dfdl_oldT, bubble_GG_ph, Lam); }, nranks, myrank);
        symm_grp_P_xph.init(gf_P_xph_loc, [&state_vec, &dfdl_oldT, &bubble_GG_ph, Lam](const idx_P_t & idx) {
            return eval_diag_xph_L(iP_to_iphi(idx), state_vec, dfdl_oldT, bubble_GG_ph, Lam); }, nranks, myrank);
        MPI_Reduce(&gf_P_pp_loc(0), &dfdl_L.gf_P_pp()(0), gf_P_pp_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_P_ph_loc(0), &dfdl_L.gf_P_ph()(0), gf_P_ph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_P_xph_loc(0), &dfdl_L.gf_P_xph()(0), gf_P_xph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);

        if ( myrank == 0 )cout << " ... RIGHT MULTILOOP DIAGRAMS ... " << endl;

        if ( myrank == 0 )cout << " ... phi " << endl;
        gf_phi_pp_loc.init(phi_zero, 1, 0);
        gf_phi_ph_loc.init(phi_zero, 1, 0);
        gf_phi_xph_loc.init(phi_zero, 1, 0);
        symm_grp_phi_pp_L.init(gf_phi_pp_loc, [&state_vec, &dfdl_oldT, &bubble_GG_pp, Lam](const idx_phi_t & idx) {
            return eval_diag_pp_R(idx, state_vec, dfdl_oldT, bubble_GG_pp, Lam); }, nranks, myrank);
        symm_grp_phi_ph_L.init(gf_phi_ph_loc, [&state_vec, &dfdl_oldT, &bubble_GG_ph, Lam](const idx_phi_t & idx) {
            return eval_diag_ph_R(idx, state_vec, dfdl_oldT, bubble_GG_ph, Lam); }, nranks, myrank);
        symm_grp_phi_xph_L.init(gf_phi_xph_loc, [&state_vec, &dfdl_oldT, &bubble_GG_ph, Lam](const idx_phi_t & idx) {
            return eval_diag_xph_R(idx, state_vec, dfdl_oldT, bubble_GG_ph, Lam); }, nranks, myrank);
        MPI_Reduce(&gf_phi_pp_loc(0), &dfdl_R.gf_phi_pp()(0), gf_phi_pp_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_phi_ph_loc(0), &dfdl_R.gf_phi_ph()(0), gf_phi_ph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&gf_phi_xph_loc(0), &dfdl_R.gf_phi_xph()(0), gf_phi_xph_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);

#else   

        cout << " ... phi central " << endl;
        symm_grp_phi_pp.init(dfdl_C.gf_phi_pp(), [&state_vec, &dfdl_oldL, &bubble_GG_pp, Lam](const idx_phi_t & idx) {
            return eval_diag_pp_C(idx, state_vec, dfdl_oldL, bubble_GG_pp, Lam); });
        symm_grp_phi_ph.init(dfdl_C.gf_phi_ph(), [&state_vec, &dfdl_oldL, &bubble_GG_ph, Lam](const idx_phi_t & idx) {
            return eval_diag_ph_C(idx, state_vec, dfdl_oldL, bubble_GG_ph, Lam); });
        symm_grp_phi_xph.init(dfdl_C.gf_phi_xph(), [&state_vec, &dfdl_oldL, &bubble_GG_ph, Lam](const idx_phi_t & idx) {
            return eval_diag_xph_C(idx, state_vec, dfdl_oldL, bubble_GG_ph, Lam); });

        cout << " ... chi central " << endl;
        symm_grp_chi_pp.init(dfdl_C.gf_chi_pp(), [&state_vec, &dfdl_oldL, &bubble_GG_pp, Lam](const idx_chi_t & idx) {
            return eval_diag_pp_C(ichi_to_iphi(idx), state_vec, dfdl_oldL, bubble_GG_pp, Lam); });
        symm_grp_chi_ph.init(dfdl_C.gf_chi_ph(), [&state_vec, &dfdl_oldL, &bubble_GG_ph, Lam](const idx_chi_t & idx) {
            return eval_diag_ph_C(ichi_to_iphi(idx), state_vec, dfdl_oldL, bubble_GG_ph, Lam); });
        symm_grp_chi_xph.init(dfdl_C.gf_chi_xph(), [&state_vec, &dfdl_oldL, &bubble_GG_ph, Lam](const idx_chi_t & idx) {
            return eval_diag_xph_C(ichi_to_iphi(idx), state_vec, dfdl_oldL, bubble_GG_ph, Lam); });

        cout << " ... P central " << endl << endl;
        symm_grp_P_pp.init(dfdl_C.gf_P_pp(), [&state_vec, &dfdl_oldL, &dfdl_C, &bubble_GG_pp, Lam](const idx_P_t & idx) {
            return eval_diag_pp_C(iP_to_iphi(idx), state_vec, dfdl_oldL, bubble_GG_pp, Lam) - double(idx(IP::n) == 0) * dfdl_C.gf_chi_pp()(iP_to_ichi(idx)); });
        symm_grp_P_ph.init(dfdl_C.gf_P_ph(), [&state_vec, &dfdl_oldL, &dfdl_C, &bubble_GG_ph, Lam](const idx_P_t & idx) {
            return eval_diag_ph_C(iP_to_iphi(idx), state_vec, dfdl_oldL, bubble_GG_ph, Lam) - double(idx(IP::n) == 0) * dfdl_C.gf_chi_ph()(iP_to_ichi(idx)); });
        symm_grp_P_xph.init(dfdl_C.gf_P_xph(), [&state_vec, &dfdl_oldL, &dfdl_C, &bubble_GG_ph, Lam](const idx_P_t & idx) {
            return eval_diag_xph_C(iP_to_iphi(idx), state_vec, dfdl_oldL, bubble_GG_ph, Lam) - double(idx(IP::n) == 0) * dfdl_C.gf_chi_xph()(iP_to_ichi(idx)); });


        cout << " ... LEFT MULTILOOP DIAGRAMS ... " << endl;

        cout << " ... phi left " << endl;
        symm_grp_phi_pp_L.init(dfdl_L.gf_phi_pp(), [&state_vec, &dfdl_oldT, &bubble_GG_pp, Lam](const idx_phi_t & idx) {
            return eval_diag_pp_L(idx, state_vec, dfdl_oldT, bubble_GG_pp, Lam); });
        symm_grp_phi_ph_L.init(dfdl_L.gf_phi_ph(), [&state_vec, &dfdl_oldT, &bubble_GG_ph, Lam](const idx_phi_t & idx) {
            return eval_diag_ph_L(idx, state_vec, dfdl_oldT, bubble_GG_ph, Lam); });
        symm_grp_phi_xph_L.init(dfdl_L.gf_phi_xph(), [&state_vec, &dfdl_oldT, &bubble_GG_ph, Lam](const idx_phi_t & idx) {
            return eval_diag_xph_L(idx, state_vec, dfdl_oldT, bubble_GG_ph, Lam); });

        cout << " ... P left " << endl << endl;
        symm_grp_P_pp.init(dfdl_L.gf_P_pp(), [&state_vec, &dfdl_oldT, &bubble_GG_pp, Lam](const idx_P_t & idx) {
            return eval_diag_pp_L(iP_to_iphi(idx), state_vec, dfdl_oldT, bubble_GG_pp, Lam); });
        symm_grp_P_ph.init(dfdl_L.gf_P_ph(), [&state_vec, &dfdl_oldT, &bubble_GG_ph, Lam](const idx_P_t & idx) {
            return eval_diag_ph_L(iP_to_iphi(idx), state_vec, dfdl_oldT, bubble_GG_ph, Lam); });
        symm_grp_P_xph.init(dfdl_L.gf_P_xph(), [&state_vec, &dfdl_oldT, &bubble_GG_ph, Lam](const idx_P_t & idx) {
            return eval_diag_xph_L(iP_to_iphi(idx), state_vec, dfdl_oldT, bubble_GG_ph, Lam); });

        cout << " ... RIGHT MULTILOOP DIAGRAMS ... " << endl;

        cout << " ... phi right " << endl << endl;
        symm_grp_phi_pp_L.init(dfdl_R.gf_phi_pp(), [&state_vec, &dfdl_oldT, &bubble_GG_pp, Lam](const idx_phi_t & idx) {
            return eval_diag_pp_R(idx, state_vec, dfdl_oldT, bubble_GG_pp, Lam); });
        symm_grp_phi_ph_L.init(dfdl_R.gf_phi_ph(), [&state_vec, &dfdl_oldT, &bubble_GG_ph, Lam](const idx_phi_t & idx) {
            return eval_diag_ph_R(idx, state_vec, dfdl_oldT, bubble_GG_ph, Lam); });
        symm_grp_phi_xph_L.init(dfdl_R.gf_phi_xph(), [&state_vec, &dfdl_oldT, &bubble_GG_ph, Lam](const idx_phi_t & idx) {
            return eval_diag_xph_R(idx, state_vec, dfdl_oldT, bubble_GG_ph, Lam); });

#endif // ends MPI_PARALLEL

        if ( myrank == 0 )cout << " Update dfdl.." << endl;

        if ( myrank == 0 ) {
            dfdl_T = dfdl_L + dfdl_R + dfdl_C;
            dfdl_old = dfdl; // dfdl at the previous loop-step 
            dfdl += dfdl_T;
            dfdl_C_sum += dfdl_C;
        }

    } while (count < MULOOP_NUM);

    // START EVALUATING THE MULTILOOP CORRECTIVE DIAGRAMS FOR TRILEG AND SUSCEPTIBILITY

    state_t dfdl_TRI1, dfdl_TRI2;
    state_t dfdl_SUSC1, dfdl_SUSC2;

#ifdef MPI_PARALLEL

    if ( myrank == 0 )cout << " ... FIRST MULTILOOP CORRECTIONS for trileg and trileg's asymptotics " << endl;
    MPI_Bcast(&dfdl_old.gf_phi_pp()(0), dfdl_old.gf_phi_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_old.gf_phi_ph()(0), dfdl_old.gf_phi_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_old.gf_phi_xph()(0), dfdl_old.gf_phi_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_old.gf_P_pp()(0), dfdl_old.gf_P_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_old.gf_P_ph()(0), dfdl_old.gf_P_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_old.gf_P_xph()(0), dfdl_old.gf_P_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_old.gf_chi_pp()(0), dfdl_old.gf_chi_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_old.gf_chi_ph()(0), dfdl_old.gf_chi_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_old.gf_chi_xph()(0), dfdl_old.gf_chi_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);


    if ( myrank == 0 )cout << " ... trileg FIRST MULTILOOP CORRECTION " << endl;
    gf_tri_sc_loc.init(tri_zero, 1, 0);
    gf_tri_d_loc.init(tri_zero, 1, 0);
    gf_tri_m_loc.init(tri_zero, 1, 0);
    symm_grp_tri_sc.init(gf_tri_sc_loc, [&state_vec, &dfdl_old, &bubble_GG_pp, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_sc_R(idx, state_vec, dfdl_old, bubble_GG_pp, Lam); }, nranks, myrank);
    symm_grp_tri_d.init(gf_tri_d_loc, [&state_vec, &dfdl_old, &bubble_GG_ph, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_d_R(idx, state_vec, dfdl_old, bubble_GG_ph, Lam); }, nranks, myrank);
    symm_grp_tri_m.init(gf_tri_m_loc, [&state_vec, &dfdl_old, &bubble_GG_ph, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_m_R(idx, state_vec, dfdl_old, bubble_GG_ph, Lam); }, nranks, myrank);

    MPI_Reduce(&gf_tri_sc_loc(0), &dfdl_TRI1.gf_tri_sc()(0), gf_tri_sc_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&gf_tri_d_loc(0), &dfdl_TRI1.gf_tri_d()(0), gf_tri_d_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&gf_tri_m_loc(0), &dfdl_TRI1.gf_tri_m()(0), gf_tri_m_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);

    if ( myrank == 0 ) {
        dfdl += dfdl_TRI1;
    }

    if ( myrank == 0 )cout << " ... SECOND MULTILOOP CORRECTIONS for trileg and trileg's asymptotics " << endl;
    MPI_Bcast(&dfdl_L_sum.gf_phi_pp()(0), dfdl_L_sum.gf_phi_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_L_sum.gf_phi_ph()(0), dfdl_L_sum.gf_phi_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_L_sum.gf_phi_xph()(0), dfdl_L_sum.gf_phi_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_L_sum.gf_P_pp()(0), dfdl_L_sum.gf_P_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_L_sum.gf_P_ph()(0), dfdl_L_sum.gf_P_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_L_sum.gf_P_xph()(0), dfdl_L_sum.gf_P_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_L_sum.gf_chi_pp()(0), dfdl_L_sum.gf_chi_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_L_sum.gf_chi_ph()(0), dfdl_L_sum.gf_chi_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_L_sum.gf_chi_xph()(0), dfdl_L_sum.gf_chi_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);

    if ( myrank == 0 )cout << " ... asymptotics trileg SECOND MULTILOOP CORRECTION " << endl;
    gf_asytri_sc_loc.init(asytri_zero, 1, 0);
    gf_asytri_d_loc.init(asytri_zero, 1, 0);
    gf_asytri_m_loc.init(asytri_zero, 1, 0);
    symm_grp_asytri_sc.init(gf_asytri_sc_loc, [&state_vec, &dfdl_L_sum, &bubble_GG_pp, Lam](const idx_asytri_t & idx) {
        return eval_diag_tri_sc_C(iasy_to_itri(idx), state_vec, dfdl_L_sum, bubble_GG_pp, Lam); }, nranks, myrank);
    symm_grp_asytri_d.init(gf_asytri_d_loc, [&state_vec, &dfdl_L_sum, &bubble_GG_ph, Lam](const idx_asytri_t & idx) {
        return eval_diag_tri_d_C(iasy_to_itri(idx), state_vec, dfdl_L_sum, bubble_GG_ph, Lam); }, nranks, myrank);
    symm_grp_asytri_m.init(gf_asytri_m_loc, [&state_vec, &dfdl_L_sum, &bubble_GG_ph, Lam](const idx_asytri_t & idx) {
        return eval_diag_tri_m_C(iasy_to_itri(idx), state_vec, dfdl_L_sum, bubble_GG_ph, Lam); }, nranks, myrank);

    MPI_Reduce(&gf_asytri_sc_loc(0), &dfdl_TRI2.gf_asytri_sc()(0), gf_asytri_sc_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&gf_asytri_d_loc(0), &dfdl_TRI2.gf_asytri_d()(0), gf_asytri_d_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&gf_asytri_m_loc(0), &dfdl_TRI2.gf_asytri_m()(0), gf_asytri_m_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);

    if ( myrank == 0 )cout << " ... trileg SECOND MULTILOOP CORRECTION " << endl;
    gf_tri_sc_loc.init(tri_zero, 1, 0);
    gf_tri_d_loc.init(tri_zero, 1, 0);
    gf_tri_m_loc.init(tri_zero, 1, 0);
    symm_grp_tri_sc.init(gf_tri_sc_loc, [&state_vec, &dfdl_L_sum, &bubble_GG_pp, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_sc_C(idx, state_vec, dfdl_L_sum, bubble_GG_pp, Lam); }, nranks, myrank);
    symm_grp_tri_d.init(gf_tri_d_loc, [&state_vec, &dfdl_L_sum, &bubble_GG_ph, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_d_C(idx, state_vec, dfdl_L_sum, bubble_GG_ph, Lam); }, nranks, myrank);
    symm_grp_tri_m.init(gf_tri_m_loc, [&state_vec, &dfdl_L_sum, &bubble_GG_ph, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_m_C(idx, state_vec, dfdl_L_sum, bubble_GG_ph, Lam); }, nranks, myrank);

    MPI_Reduce(&gf_tri_sc_loc(0), &dfdl_TRI2.gf_tri_sc()(0), gf_tri_sc_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&gf_tri_d_loc(0), &dfdl_TRI2.gf_tri_d()(0), gf_tri_d_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&gf_tri_m_loc(0), &dfdl_TRI2.gf_tri_m()(0), gf_tri_m_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);

    if ( myrank == 0 ) {
        dfdl += dfdl_TRI2;
    }

    if ( myrank == 0 )cout << " ... FIRST MULTILOOP CORRECTIONS for trileg and trileg's asymptotics required for the susceptibility" << endl;
    MPI_Bcast(&dfdl_oldold.gf_phi_pp()(0), dfdl_oldold.gf_phi_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_oldold.gf_phi_ph()(0), dfdl_oldold.gf_phi_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_oldold.gf_phi_xph()(0), dfdl_oldold.gf_phi_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_oldold.gf_P_pp()(0), dfdl_oldold.gf_P_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_oldold.gf_P_ph()(0), dfdl_oldold.gf_P_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_oldold.gf_P_xph()(0), dfdl_oldold.gf_P_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_oldold.gf_chi_pp()(0), dfdl_oldold.gf_chi_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_oldold.gf_chi_ph()(0), dfdl_oldold.gf_chi_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_oldold.gf_chi_xph()(0), dfdl_oldold.gf_chi_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);

    if ( myrank == 0 )cout << " ... trileg FIRST MULTILOOP CORRECTION FOR THE SUSCEPT " << endl;
    gf_tri_sc_loc.init(tri_zero, 1, 0);
    gf_tri_d_loc.init(tri_zero, 1, 0);
    gf_tri_m_loc.init(tri_zero, 1, 0);

    symm_grp_tri_sc.init(gf_tri_sc_loc, [&state_vec, &dfdl_oldold, &bubble_GG_pp, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_sc_R(idx, state_vec, dfdl_oldold, bubble_GG_pp, Lam); }, nranks, myrank);
    symm_grp_tri_d.init(gf_tri_d_loc, [&state_vec, &dfdl_oldold, &bubble_GG_ph, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_d_R(idx, state_vec, dfdl_oldold, bubble_GG_ph, Lam); }, nranks, myrank);
    symm_grp_tri_m.init(gf_tri_m_loc, [&state_vec, &dfdl_oldold, &bubble_GG_ph, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_m_R(idx, state_vec, dfdl_oldold, bubble_GG_ph, Lam); }, nranks, myrank);

    MPI_Reduce(&gf_tri_sc_loc(0), &dfdl_SUSC1.gf_tri_sc()(0), gf_tri_sc_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&gf_tri_d_loc(0), &dfdl_SUSC1.gf_tri_d()(0), gf_tri_d_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&gf_tri_m_loc(0), &dfdl_SUSC1.gf_tri_m()(0), gf_tri_m_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);

    if ( myrank == 0 )cout << " ... MULTILOOP CORRECTION TO suscept " << endl;
    gf_susc_sc_loc.init(susc_zero, 1, 0);
    gf_susc_d_loc.init(susc_zero, 1, 0);
    gf_susc_m_loc.init(susc_zero, 1, 0);

    MPI_Bcast(&dfdl_SUSC1.gf_asytri_sc()(0), dfdl_SUSC1.gf_asytri_sc().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_SUSC1.gf_asytri_d()(0), dfdl_SUSC1.gf_asytri_d().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_SUSC1.gf_asytri_m()(0), dfdl_SUSC1.gf_asytri_m().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_SUSC1.gf_tri_sc()(0), dfdl_SUSC1.gf_tri_sc().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_SUSC1.gf_tri_d()(0), dfdl_SUSC1.gf_tri_d().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_SUSC1.gf_tri_m()(0), dfdl_SUSC1.gf_tri_m().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);

    symm_grp_susc_sc.init(gf_susc_sc_loc, [&state_vec, &dfdl_SUSC1, &bubble_GG_pp, Lam](const idx_susc_t & idx) {
        return eval_diag_susc_sc_C(idx, state_vec, dfdl_SUSC1, bubble_GG_pp, Lam); });
    symm_grp_susc_d.init(gf_susc_d_loc, [&state_vec, &dfdl_SUSC1, &bubble_GG_ph, Lam](const idx_susc_t & idx) {
        return eval_diag_susc_d_C(idx, state_vec, dfdl_SUSC1, bubble_GG_ph, Lam); });
    symm_grp_susc_m.init(gf_susc_m_loc, [&state_vec, &dfdl_SUSC1, &bubble_GG_ph, Lam](const idx_susc_t & idx) {
        return eval_diag_susc_m_C(idx, state_vec, dfdl_SUSC1, bubble_GG_ph, Lam); });

    MPI_Reduce(&gf_susc_sc_loc(0), &dfdl_SUSC2.gf_susc_sc()(0), gf_susc_sc_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&gf_susc_d_loc(0), &dfdl_SUSC2.gf_susc_d()(0), gf_susc_d_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&gf_susc_m_loc(0), &dfdl_SUSC2.gf_susc_m()(0), gf_susc_m_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);

    if ( myrank == 0 ) {
        dfdl += dfdl_SUSC2;
    }

#else

    cout << " ... MULTILOOP  FIRST CORRECTION TO trileg " << endl << endl;
    symm_grp_tri_sc.init(dfdl_TRI1.gf_tri_sc(), [&state_vec, &dfdl_old, &bubble_GG_pp, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_sc_R(idx, state_vec, dfdl_old, bubble_GG_pp, Lam); });
    symm_grp_tri_d.init(dfdl_TRI1.gf_tri_d(), [&state_vec, &dfdl_old, &bubble_GG_ph, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_d_R(idx, state_vec, dfdl_old, bubble_GG_ph, Lam); });
    symm_grp_tri_m.init(dfdl_TRI1.gf_tri_m(), [&state_vec, &dfdl_old, &bubble_GG_ph, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_m_R(idx, state_vec, dfdl_old, bubble_GG_ph, Lam); });

    if ( myrank == 0 ) {
        dfdl += dfdl_TRI1;
    }

    cout << " ... MULTILOOP SECOND CORRECTION TO trileg asymptotics" << endl << endl;
    symm_grp_asytri_sc.init(dfdl_TRI2.gf_asytri_sc(), [&state_vec, &dfdl_L_sum, &bubble_GG_pp, Lam](const idx_asytri_t & idx) {
        return eval_diag_tri_sc_C(iasy_to_itri(idx), state_vec, dfdl_L_sum, bubble_GG_pp, Lam); });
    symm_grp_asytri_d.init(dfdl_TRI2.gf_asytri_d(), [&state_vec, &dfdl_L_sum, &bubble_GG_ph, Lam](const idx_asytri_t & idx) {
        return eval_diag_tri_d_C(iasy_to_itri(idx), state_vec, dfdl_L_sum, bubble_GG_ph, Lam); });
    symm_grp_asytri_m.init(dfdl_TRI2.gf_asytri_m(), [&state_vec, &dfdl_L_sum, &bubble_GG_ph, Lam](const idx_asytri_t & idx) {
        return eval_diag_tri_m_C(iasy_to_itri(idx), state_vec, dfdl_L_sum, bubble_GG_ph, Lam); });

    cout << " ... MULTILOOP SECOND CORRECTION TO trileg " << endl << endl;
    symm_grp_tri_sc.init(dfdl_TRI2.gf_tri_sc(), [&state_vec, &dfdl_L_sum, &bubble_GG_pp, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_sc_C(idx, state_vec, dfdl_L_sum, bubble_GG_pp, Lam); });
    symm_grp_tri_d.init(dfdl_TRI2.gf_tri_d(), [&state_vec, &dfdl_L_sum, &bubble_GG_ph, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_d_C(idx, state_vec, dfdl_L_sum, bubble_GG_ph, Lam); });
    symm_grp_tri_m.init(dfdl_TRI2.gf_tri_m(), [&state_vec, &dfdl_L_sum, &bubble_GG_ph, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_m_C(idx, state_vec, dfdl_L_sum, bubble_GG_ph, Lam); });

    if ( myrank == 0 ) {
        dfdl += dfdl_TRI2;
    }

    cout << " ... MULTILOOP FIRST CORRECTION TO trileg REQUIRED FOR THE SUSCEPT " << endl << endl;
    symm_grp_tri_sc.init(dfdl_SUSC1.gf_tri_sc(), [&state_vec, &dfdl_oldold, &bubble_GG_pp, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_sc_R(idx, state_vec, dfdl_oldold, bubble_GG_pp, Lam); });
    symm_grp_tri_d.init(dfdl_SUSC1.gf_tri_d(), [&state_vec, &dfdl_oldold, &bubble_GG_ph, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_d_R(idx, state_vec, dfdl_oldold, bubble_GG_ph, Lam); });
    symm_grp_tri_m.init(dfdl_SUSC1.gf_tri_m(), [&state_vec, &dfdl_oldold, &bubble_GG_ph, Lam](const idx_tri_t & idx) {
        return eval_diag_tri_m_R(idx, state_vec, dfdl_oldold, bubble_GG_ph, Lam); });

    cout << " ... MULTILOOP CORRECTION TO suscept " << endl << endl;
    symm_grp_susc_sc.init(dfdl_SUSC2.gf_susc_sc(), [&state_vec, &dfdl_SUSC1, &bubble_GG_pp, Lam](const idx_susc_t & idx) {
        return eval_diag_susc_sc_C(idx, state_vec, dfdl_SUSC1, bubble_GG_pp, Lam); });
    symm_grp_susc_d.init(dfdl_SUSC2.gf_susc_d(), [&state_vec, &dfdl_SUSC1, &bubble_GG_ph, Lam](const idx_susc_t & idx) {
        return eval_diag_susc_d_C(idx, state_vec, dfdl_SUSC1, bubble_GG_ph, Lam); });
    symm_grp_susc_m.init(dfdl_SUSC2.gf_susc_m(), [&state_vec, &dfdl_SUSC1, &bubble_GG_ph, Lam](const idx_susc_t & idx) {
        return eval_diag_susc_m_C(idx, state_vec, dfdl_SUSC1, bubble_GG_ph, Lam); });


    if ( myrank == 0 ) {
        dfdl += dfdl_SUSC2;
    }

#endif //END MULTILOOP IF CONDITION

#ifdef SE_CORR

    if ( myrank == 0 )cout << "... SE multiloop corrections" << endl;

    state_t dfdl_SE1, dfdl_SE2;

    gf_1p_mat_t GdSigG(POS_1P_RANGE, FFT_DIM * FFT_DIM);

#ifdef MPI_PARALLEL
    //gf_1p_mat_t gf_Sig_loc( POS_1P_RANGE, FFT_DIM*FFT_DIM ); //should be already defined in KATANIN and KATANIN always needed for SE_CORR
    MPI_Bcast(&dfdl_C_sum.gf_phi_pp()(0), dfdl_C_sum.gf_phi_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_C_sum.gf_phi_ph()(0), dfdl_C_sum.gf_phi_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_C_sum.gf_phi_xph()(0), dfdl_C_sum.gf_phi_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_C_sum.gf_P_pp()(0), dfdl_C_sum.gf_P_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_C_sum.gf_P_ph()(0), dfdl_C_sum.gf_P_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_C_sum.gf_P_xph()(0), dfdl_C_sum.gf_P_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_C_sum.gf_chi_pp()(0), dfdl_C_sum.gf_chi_pp().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_C_sum.gf_chi_ph()(0), dfdl_C_sum.gf_chi_ph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dfdl_C_sum.gf_chi_xph()(0), dfdl_C_sum.gf_chi_xph().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);

    cout << " ... First correction" << endl;
    symm_grp_sig.init(gf_Sig_loc, [&dfdl_C_sum, &Gvec, &Lam](const idx_1p_t & idx) {
        return eval_rhs_Sig_corr1(idx, dfdl_C_sum, Gvec, Lam); }, nranks, myrank);
    MPI_Reduce(&gf_Sig_loc(0), &dfdl_SE1.gf_Sig()(0), gf_Sig_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);

    if ( myrank == 0 ) dfdl += dfdl_SE1;

    cout << " ... Second correction" << endl;
    MPI_Bcast(&dfdl_SE1.gf_Sig()(0), dfdl_SE1.gf_Sig().num_elements(), MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
    gf_1p_mat_t dSigvec(POS_1P_RANGE, FFT_DIM * FFT_DIM);
    dSigvec.init(bind(&state_t::SigMat_big, boost::cref(dfdl_SE1), _1), 1, 0); //nranks=1 and myrank=0 leads to calculation of all elements by all nodes
    GdSigG = Gvec * dSigvec * Gvec; // Add Katanin correction

    symm_grp_sig.init(gf_Sig_loc, [&state_vec, &GdSigG, &Lam](const idx_1p_t & idx) {
        return eval_rhs_Sig_corr2(idx, state_vec, GdSigG, Lam); }, nranks, myrank);
    MPI_Reduce(&gf_Sig_loc(0), &dfdl_SE2.gf_Sig()(0), gf_Sig_loc.num_elements(), MPI_DOUBLE_COMPLEX, MPI_SUM, 0, MPI_COMM_WORLD);
    if ( myrank == 0 ) dfdl += dfdl_SE2;
#else   
    cout << " ... First correction" << endl;
    symm_grp_sig.init(dfdl_SE1.gf_Sig(), [&dfdl_C_sum, &Gvec, &Lam](const idx_1p_t & idx) {
        return eval_rhs_Sig_corr1(idx, dfdl_C_sum, Gvec, Lam); });
    dfdl += dfdl_SE1;

    cout << " ... Second correction" << endl;

    gf_1p_mat_t dSigvec(POS_1P_RANGE, FFT_DIM * FFT_DIM);

    dSigvec.init(bind(&state_t::SigMat_big, boost::cref(dfdl_SE1), _1));
    GdSigG = Gvec * dSigvec * Gvec; // Add Katanin correction


    symm_grp_sig.init(dfdl_SE2.gf_Sig(), [&state_vec, &GdSigG, &Lam](const idx_1p_t & idx) {
        return eval_rhs_Sig_corr2(idx, state_vec, GdSigG, Lam); });

    dfdl += dfdl_SE2;

#endif //ends MPI_PARALLEL
#endif // ends SE_CORR
#endif // ends MULTILOOP

}

dcomplex rhs_t::eval_rhs_Sig(const idx_1p_t& idx, const state_t& state_vec, const gf_1p_mat_t& Svec, double Lam) {
    dcomplex val(0.0, 0.0);

    for ( int s2 = 0; s2 < QN_COUNT; ++s2 )
        for ( int s2p = 0; s2p < QN_COUNT; ++s2p )
            for ( int k = 0; k < PATCH_COUNT; ++k )
                for ( int w = -POS_INT_RANGE; w < POS_INT_RANGE; ++w ) {
                    int p = k_to_p_patch(k);
                    val += -Svec[w][p](s2, s2p) * weight_vec[w] *
                            (2. * state_vec.vertx(idx(I1P::w), w, idx(I1P::w), idx(I1P::k), k, idx(I1P::k), idx(I1P::s_in), s2, idx(I1P::s_out), s2p) -
                            state_vec.vertx(w, idx(I1P::w), idx(I1P::w), k, idx(I1P::k), idx(I1P::k), s2, idx(I1P::s_in), idx(I1P::s_out), s2p)); // Minus sign from loop //SU2 symmetry used
                }
    return val *= 1.0 / BETA / PATCH_COUNT;
}

dcomplex rhs_t::eval_rhs_Sig_corr1(const idx_1p_t& idx, const state_t& dfdl, const gf_1p_mat_t& Gvec, double Lam) {
    dcomplex val(0.0, 0.0);

    for ( int s2 = 0; s2 < QN_COUNT; ++s2 )
        for ( int s2p = 0; s2p < QN_COUNT; ++s2p )
            for ( int k = 0; k < PATCH_COUNT; ++k )
                for ( int w = -POS_INT_RANGE; w < POS_INT_RANGE; ++w ) {
                    int p = k_to_p_patch(k);
                    val += -Gvec[w][p](s2, s2p) * weight_vec[w] *
                            (2. * dfdl.phi_pp_plus_xph_pf(idx(I1P::w), w, idx(I1P::w), idx(I1P::k), k, idx(I1P::k), idx(I1P::s_in), s2, idx(I1P::s_out), s2p) -
                            dfdl.phi_pp_plus_xph_pf(w, idx(I1P::w), idx(I1P::w), k, idx(I1P::k), idx(I1P::k), s2, idx(I1P::s_in), idx(I1P::s_out), s2p));
                }
    return val *= 1.0 / BETA / PATCH_COUNT;
}

dcomplex rhs_t::eval_rhs_Sig_corr2(const idx_1p_t& idx, const state_t& state_vec, const gf_1p_mat_t& GdSigG, double Lam) {
    dcomplex val(0.0, 0.0);

    for ( int s2 = 0; s2 < QN_COUNT; ++s2 )
        for ( int s2p = 0; s2p < QN_COUNT; ++s2p )
            for ( int k = 0; k < PATCH_COUNT; ++k )
                for ( int w = -POS_INT_RANGE; w < POS_INT_RANGE; ++w ) {
                    int p = k_to_p_patch(k);
                    val += -GdSigG[w][p](s2, s2p) * weight_vec[w] *
                            (2. * state_vec.vertx(idx(I1P::w), w, idx(I1P::w), idx(I1P::k), k, idx(I1P::k), idx(I1P::s_in), s2, idx(I1P::s_out), s2p) -
                            state_vec.vertx(w, idx(I1P::w), idx(I1P::w), k, idx(I1P::k), idx(I1P::k), s2, idx(I1P::s_in), idx(I1P::s_out), s2p)); // Minus sign from loop //SU2 symmetry used
                }

    return val *= 1.0 / BETA / PATCH_COUNT;
}

MatReal rhs_t::eval_Gvec_real(const idx_1p_mat_real_t& idx, const gf_1p_mat_t& Gvec, fftw_plan p_g, double Lam) {
    int w = idx(0);
    int s1 = idx(1);
    int s2 = idx(2);

    MatReal val_g = MatReal::Zero();
    fftw_complex *input_g, *output_g;
    input_g = (fftw_complex*) fftw_malloc(sizeof (fftw_complex) * FFT_DIM * FFT_DIM);
    output_g = (fftw_complex*) fftw_malloc(sizeof (fftw_complex) * FFT_DIM * FFT_DIM);

    for ( int p = 0; p < FFT_DIM * FFT_DIM; ++p ) {
        input_g[p][0] = Gvec[w][p](s1, s2).real();
        input_g[p][1] = Gvec[w][p](s1, s2).imag();
    }

    // plan is created outside and passed to function because only fftw_execute_dft is thread save
    fftw_execute_dft(p_g, input_g, output_g);

    for ( int R = 0; R < FFT_DIM * FFT_DIM; ++R ) {
        val_g(R) = 1.0 / FFT_DIM / FFT_DIM * (output_g[Matidxshift(get_px_idx(R), get_py_idx(R))][0] + I * output_g[Matidxshift(get_px_idx(R), get_py_idx(R))][1]);
    }

    fftw_free(input_g);
    fftw_free(output_g);
    return val_g;
}

MatReal rhs_t::eval_Svec_real(const idx_1p_mat_real_t& idx, const gf_1p_mat_t& Svec, fftw_plan p_s, double Lam) {
    int w = idx(0);
    int s1 = idx(1);
    int s2 = idx(2);

    MatReal val_s = MatReal::Zero();
    fftw_complex *input_s, *output_s;
    input_s = (fftw_complex*) fftw_malloc(sizeof (fftw_complex) * FFT_DIM * FFT_DIM);
    output_s = (fftw_complex*) fftw_malloc(sizeof (fftw_complex) * FFT_DIM * FFT_DIM);

    for ( int p = 0; p < FFT_DIM * FFT_DIM; ++p ) {
        input_s[p][0] = Svec[w][p](s1, s2).real();
        input_s[p][1] = Svec[w][p](s1, s2).imag();
    }

    // plan is created outside and passed to function because only fftw_execute_dft is thread save
    fftw_execute_dft(p_s, input_s, output_s);

    for ( int R = 0; R < FFT_DIM * FFT_DIM; ++R ) {
        val_s(R) = 1.0 / FFT_DIM / FFT_DIM * (output_s[Matidxshift(get_px_idx(R), get_py_idx(R))][0] + I * output_s[Matidxshift(get_px_idx(R), get_py_idx(R))][1]);
    }

    fftw_free(input_s);
    fftw_free(output_s);
    return val_s;
}

MatPatch rhs_t::eval_diag_bubble_pp(const idx_bubble_mat_t& idx, const gf_1p_mat_real_t& Svec_real, const gf_1p_mat_real_t& Gvec_real, fftw_plan p_b, double Lam) {
    int W = idx(0);
    int w = idx(1);
    int m = idx(2);
    int n = idx(3);
    int s4 = idx(4);
    int s4p = idx(5);
    int s3 = idx(6);
    int s3p = idx(7);

    MatReal val = MatReal::Zero();
    MatPatch val_patch = MatPatch::Zero();
    fftw_complex *input_b, *output_b;
    input_b = (fftw_complex*) fftw_malloc(sizeof (fftw_complex) * FFT_DIM * FFT_DIM);
    output_b = (fftw_complex*) fftw_malloc(sizeof (fftw_complex) * FFT_DIM * FFT_DIM);

    for ( int r = 0; r < REAL_GRID; ++r ) {
        if ( abs(F_factors_real::fft_fxf_weight[r][m][n]) > CHOP_ERR ) {// 1E-16 is error on the weights exponents, cosines,...
            int rx = std::get<0>(R_Grid()[r]);
            int ry = std::get<1>(R_Grid()[r]);
            for ( int Rx = 0; Rx < FFT_DIM; ++Rx )
                for ( int Ry = 0; Ry < FFT_DIM; ++Ry ) {
                    dcomplex temp = Gvec_real[ w + div2_ceil(W) ][s4][s4p](Matidx(Rx, Ry)) * Svec_real[ div2_floor(W) - w - 1 ][s3][s3p](Matidx(Rx + rx, Ry + ry)) + Svec_real[ w + div2_ceil(W) ][s4][s4p](Matidx(Rx, Ry)) * Gvec_real[ div2_floor(W) - w - 1 ][s3][s3p](Matidx(Rx + rx, Ry + ry));
                    input_b[Matidxshift(Rx, Ry)][0] = temp.real();
                    input_b[Matidxshift(Rx, Ry)][1] = temp.imag();
                }
            // plan is created outside and passed to function because only fftw_execute_dft is thread save
            fftw_execute_dft(p_b, input_b, output_b);
            // calculate weight and multiply it
            for ( int K = 0; K < PATCH_COUNT; ++K ) {
                int Kx_idx = std::get<0>(K_Grid::get_indices(K));
                int Ky_idx = std::get<1>(K_Grid::get_indices(K));
                int Kx_shift = (Kx_idx * FFT_DIM / 2 / MAX_KPOS + 10 * FFT_DIM) % FFT_DIM;
                int Ky_shift = (Ky_idx * FFT_DIM / 2 / MAX_KPOS + 10 * FFT_DIM) % FFT_DIM;
                dcomplex weight = F_factors_real::fft_fxf_weight[r][m][n];
                weight *= (cos_k(Kx_idx * rx + Ky_idx * ry) - I * sin_k(Kx_idx * rx + Ky_idx * ry));
                //dcomplex weight = exp(-I*0.5*(double)(Kx_idx*rx+Ky_idy*ry)*2.0*PI/(double)FFT_DIM) * F_factors_real::fft_fxf_weight[r][m][n];
                val_patch(K) += (output_b[Matidx(Kx_shift, Ky_shift)][0] + I * output_b[Matidx(Kx_shift, Ky_shift)][1]) * weight;
            }
        }
    }

    fftw_free(input_b);
    fftw_free(output_b);
    return val_patch;

}

MatPatch rhs_t::eval_diag_bubble_ph(const idx_bubble_mat_t& idx, const gf_1p_mat_real_t& Svec_real, const gf_1p_mat_real_t& Gvec_real, fftw_plan p_b, double Lam) {
    int W = idx(0);
    int w = idx(1);
    int m = idx(2);
    int n = idx(3);
    int s4 = idx(4);
    int s4p = idx(5);
    int s3 = idx(6);
    int s3p = idx(7);

    MatReal val = MatReal::Zero();
    MatPatch val_patch = MatPatch::Zero();
    fftw_complex *input_b, *output_b;
    input_b = (fftw_complex*) fftw_malloc(sizeof (fftw_complex) * FFT_DIM * FFT_DIM);
    output_b = (fftw_complex*) fftw_malloc(sizeof (fftw_complex) * FFT_DIM * FFT_DIM);

    for ( int r = 0; r < REAL_GRID; ++r ) {
        if ( abs(F_factors_real::fft_fxf_weight[r][m][n]) > CHOP_ERR ) {// 1E-16 is error on the weights exponents, cosines,...
            int rx = std::get<0>(R_Grid()[r]);
            int ry = std::get<1>(R_Grid()[r]);
            for ( int Rx = 0; Rx < FFT_DIM; ++Rx )
                for ( int Ry = 0; Ry < FFT_DIM; ++Ry ) {
                    dcomplex temp = Gvec_real[ w - div2_floor(W) ][s4][s4p](Matidx(FFT_DIM - Rx, FFT_DIM - Ry)) * Svec_real[ div2_ceil(W) + w ][s3][s3p](Matidx(Rx - rx, Ry - ry)) + Svec_real[ w - div2_floor(W) ][s4][s4p](Matidx(FFT_DIM - Rx, FFT_DIM - Ry)) * Gvec_real[ div2_ceil(W) + w ][s3][s3p](Matidx(Rx - rx, Ry - ry));
                    input_b[Matidxshift(Rx, Ry)][0] = temp.real();
                    input_b[Matidxshift(Rx, Ry)][1] = temp.imag();
                }
            // plan is created outside and passed to function because only fftw_execute_dft is thread save
            fftw_execute_dft(p_b, input_b, output_b);
            // calculate weight and multiply it
            for ( int K = 0; K < PATCH_COUNT; ++K ) {
                int Kx_idx = std::get<0>(K_Grid::get_indices(K));
                int Ky_idx = std::get<1>(K_Grid::get_indices(K));
                int Kx_shift = (Kx_idx * FFT_DIM / 2 / MAX_KPOS + 10 * FFT_DIM) % FFT_DIM;
                int Ky_shift = (Ky_idx * FFT_DIM / 2 / MAX_KPOS + 10 * FFT_DIM) % FFT_DIM;
                dcomplex weight = F_factors_real::fft_fxf_weight[r][m][n];
                weight *= (cos_k(Kx_idx * rx + Ky_idx * ry) + I * sin_k(Kx_idx * rx + Ky_idx * ry));
                //dcomplex weight = exp(I*0.5*(double)(Kx_idx*rx+Ky_idx*ry)*PI/(double)MAX_KPOS) * F_factors_real::fft_fxf_weight[r][m][n];
                val_patch(K) += (output_b[Matidx(Kx_shift, Ky_shift)][0] + I * output_b[Matidx(Kx_shift, Ky_shift)][1]) * weight;
            }
        }
    }
    fftw_free(input_b);
    fftw_free(output_b);
    return val_patch;
}

MatPatch rhs_t::eval_diag_bubble_GG_pp(const idx_bubble_mat_t& idx, const gf_1p_mat_real_t& Gvec_real, fftw_plan p_b, double Lam) {
    int W = idx(0);
    int w = idx(1);
    int m = idx(2);
    int n = idx(3);
    int s4 = idx(4);
    int s4p = idx(5);
    int s3 = idx(6);
    int s3p = idx(7);

    MatReal val = MatReal::Zero();
    MatPatch val_patch = MatPatch::Zero();
    fftw_complex *input_b, *output_b;
    input_b = (fftw_complex*) fftw_malloc(sizeof (fftw_complex) * FFT_DIM * FFT_DIM);
    output_b = (fftw_complex*) fftw_malloc(sizeof (fftw_complex) * FFT_DIM * FFT_DIM);

    for ( int r = 0; r < REAL_GRID; ++r ) {
        if ( abs(F_factors_real::fft_fxf_weight[r][m][n]) > CHOP_ERR ) {// 1E-16 is error on the weights exponents, cosines,...
            int rx = std::get<0>(R_Grid()[r]);
            int ry = std::get<1>(R_Grid()[r]);
            for ( int Rx = 0; Rx < FFT_DIM; ++Rx )
                for ( int Ry = 0; Ry < FFT_DIM; ++Ry ) {
                    dcomplex temp = Gvec_real[ w + div2_ceil(W) ][s4][s4p](Matidx(Rx, Ry)) * Gvec_real[ div2_floor(W) - w - 1 ][s3][s3p](Matidx(Rx + rx, Ry + ry));
                    input_b[Matidxshift(Rx, Ry)][0] = temp.real();
                    input_b[Matidxshift(Rx, Ry)][1] = temp.imag();
                }
            // plan is created outside and passed to function because only fftw_execute_dft is thread save
            fftw_execute_dft(p_b, input_b, output_b);
            // calculate weight and multiply it
            for ( int K = 0; K < PATCH_COUNT; ++K ) {
                int Kx_idx = std::get<0>(K_Grid::get_indices(K));
                int Ky_idx = std::get<1>(K_Grid::get_indices(K));
                int Kx_shift = (Kx_idx * FFT_DIM / 2 / MAX_KPOS + 10 * FFT_DIM) % FFT_DIM;
                int Ky_shift = (Ky_idx * FFT_DIM / 2 / MAX_KPOS + 10 * FFT_DIM) % FFT_DIM;
                dcomplex weight = F_factors_real::fft_fxf_weight[r][m][n];
                weight *= (cos_k(Kx_idx * rx + Ky_idx * ry) - I * sin_k(Kx_idx * rx + Ky_idx * ry));
                //dcomplex weight = exp(-I*0.5*(double)(Kx_idx*rx+Ky_idy*ry)*2.0*PI/(double)FFT_DIM) * F_factors_real::fft_fxf_weight[r][m][n];
                val_patch(K) += (output_b[Matidx(Kx_shift, Ky_shift)][0] + I * output_b[Matidx(Kx_shift, Ky_shift)][1]) * weight;
            }
        }
    }

    fftw_free(input_b);
    fftw_free(output_b);
    return val_patch;

}

MatPatch rhs_t::eval_diag_bubble_GG_ph(const idx_bubble_mat_t& idx, const gf_1p_mat_real_t& Gvec_real, fftw_plan p_b, double Lam) {
    int W = idx(0);
    int w = idx(1);
    int m = idx(2);
    int n = idx(3);
    int s4 = idx(4);
    int s4p = idx(5);
    int s3 = idx(6);
    int s3p = idx(7);

    MatReal val = MatReal::Zero();
    MatPatch val_patch = MatPatch::Zero();
    fftw_complex *input_b, *output_b;
    input_b = (fftw_complex*) fftw_malloc(sizeof (fftw_complex) * FFT_DIM * FFT_DIM);
    output_b = (fftw_complex*) fftw_malloc(sizeof (fftw_complex) * FFT_DIM * FFT_DIM);

    for ( int r = 0; r < REAL_GRID; ++r ) {
        if ( abs(F_factors_real::fft_fxf_weight[r][m][n]) > CHOP_ERR ) {// 1E-16 is error on the weights exponents, cosines,...
            int rx = std::get<0>(R_Grid()[r]);
            int ry = std::get<1>(R_Grid()[r]);
            for ( int Rx = 0; Rx < FFT_DIM; ++Rx )
                for ( int Ry = 0; Ry < FFT_DIM; ++Ry ) {
                    dcomplex temp = Gvec_real[ w - div2_floor(W) ][s4][s4p](Matidx(FFT_DIM - Rx, FFT_DIM - Ry)) * Gvec_real[ div2_ceil(W) + w ][s3][s3p](Matidx(Rx - rx, Ry - ry));
                    input_b[Matidxshift(Rx, Ry)][0] = temp.real();
                    input_b[Matidxshift(Rx, Ry)][1] = temp.imag();
                }
            // plan is created outside and passed to function because only fftw_execute_dft is thread save
            fftw_execute_dft(p_b, input_b, output_b);
            // calculate weight and multiply it
            for ( int K = 0; K < PATCH_COUNT; ++K ) {
                int Kx_idx = std::get<0>(K_Grid::get_indices(K));
                int Ky_idx = std::get<1>(K_Grid::get_indices(K));
                int Kx_shift = (Kx_idx * FFT_DIM / 2 / MAX_KPOS + 10 * FFT_DIM) % FFT_DIM;
                int Ky_shift = (Ky_idx * FFT_DIM / 2 / MAX_KPOS + 10 * FFT_DIM) % FFT_DIM;
                dcomplex weight = F_factors_real::fft_fxf_weight[r][m][n];
                weight *= (cos_k(Kx_idx * rx + Ky_idx * ry) + I * sin_k(Kx_idx * rx + Ky_idx * ry));
                //dcomplex weight = exp(I*0.5*(double)(Kx_idx*rx+Ky_idx*ry)*PI/(double)MAX_KPOS) * F_factors_real::fft_fxf_weight[r][m][n];
                val_patch(K) += (output_b[Matidx(Kx_shift, Ky_shift)][0] + I * output_b[Matidx(Kx_shift, Ky_shift)][1]) * weight;
            }
        }
    }
    fftw_free(input_b);
    fftw_free(output_b);
    return val_patch;
}

dcomplex rhs_t::eval_diag_pp(const idx_phi_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_pp, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(IPHI::W);
    int w_in = idx(IPHI::w_in);
    int w_out = idx(IPHI::w_out);

    int K = idx(IPHI::K);
    int n_in = idx(IPHI::n_in);
    int n_out = idx(IPHI::n_out);
    //switched index from I2P to IPHI, seems more appropriate when dealing with an index of type idx_phi_t
    //you should check it!
    int s1_in = idx(IPHI::s1_in);
    int s2_in = idx(IPHI::s2_in);
    int s1_out = idx(IPHI::s1_out);
    int s2_out = idx(IPHI::s2_out);

    //   cout << "W" << W << "w_in" << w_in << "w_out" << w_out << "K" << K << endl;
    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle particle channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {
                                val +=
                                        state_vec.vertx_pp(W, w_in, w, K, n_in, m, s1_in, s2_in, s3p, s4p) *
                                        bubble_pp[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        state_vec.vertx_pp(W, w, w_out, K, mp, n_out, s3, s4, s1_out, s2_out);
                            }

                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    BETA * (s3 == s3p) * (s4 == s4p) * (m == mp) * asympt_SGpGS_pp(W, Lam) * //( W==0 ) * BETA * Lam/4.0 * -> HIGH-FREQ CONTRIBUTION INTFL 
                                    state_vec.vertx_pp(W, w_in, w, K, n_in, m, s1_in, s2_in, s3p, s4p) *
                                    state_vec.vertx_pp(W, w, w_out, K, mp, n_out, s3, s4, s1_out, s2_out);

                        }
    val *= 1.0 / BETA / 4.0 / PI / PI; //TODO: divide for FFACTOR_COUNT? 
    return val;
}

dcomplex rhs_t::eval_diag_ph(const idx_phi_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_ph, double Lam) {
#ifdef PROFILE
    Extrae_event(1000, 20);
#endif
    dcomplex val(0.0, 0.0), tmp1(0, 0), tmp2(0, 0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(IPHI::W);
    int w_in = idx(IPHI::w_in);
    int w_out = idx(IPHI::w_out);

    int K = idx(IPHI::K);
    int n_in = idx(IPHI::n_in);
    int n_out = idx(IPHI::n_out);
    //switched index from I2P to IPHI, seems more appropriate when dealing with an index of type idx_phi_t
    //you should check it!
    int s1_in = idx(IPHI::s1_in);
    int s2_in = idx(IPHI::s2_in);
    int s1_out = idx(IPHI::s1_out);
    int s2_out = idx(IPHI::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle hole channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {
                                //int w_big = POS_INT_RANGE + abs(W/2);
                                tmp1 = state_vec.vertx_ph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p);
                                tmp2 = state_vec.vertx_ph(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out);
                                val +=

                                        -bubble_ph[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        ((2. * tmp1 - state_vec.vertx_xph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p)) * tmp2
                                        - tmp1 * state_vec.vertx_xph(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out));
                            }
                            if ( (s3 == s3p) && (s4 == s4p) && (m == mp) ) {
                                // explicit large frequency contribution -- alternative to weight_vec
                                int w = POS_INT_RANGE + abs(W / 2);
                                tmp1 = state_vec.vertx_ph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p);
                                tmp2 = state_vec.vertx_ph(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out);
                                val +=
                                        -BETA * asympt_SGpGS_ph(W, Lam) * //( W==0 ) * BETA * Lam/4.0 *
                                        ((2. * tmp1 - state_vec.vertx_xph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p)) * tmp2
                                        - tmp1 * state_vec.vertx_xph(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out));
                            }
                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;

    return val;
}

dcomplex rhs_t::eval_diag_xph(const idx_phi_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_ph, double Lam) {
    dcomplex val(0.0, 0.0);


    //#ifdef FORCED_ZEROS
    //   if( forced_zero_check( idx ) )							// check wether element should be forced to zero
    //      return val;
    //#endif
    //
    // Introduce help variables
    int W = idx(IPHI::W);
    int w_in = idx(IPHI::w_in);
    int w_out = idx(IPHI::w_out);

    int K = idx(IPHI::K);
    int n_in = idx(IPHI::n_in);
    int n_out = idx(IPHI::n_out);
    //switched index from I2P to IPHI, seems more appropriate when dealing with an index of type idx_phi_t
    //you should check it!
    int s1_in = idx(IPHI::s1_in);
    int s2_in = idx(IPHI::s2_in);
    int s1_out = idx(IPHI::s1_out);
    int s2_out = idx(IPHI::s2_out);


    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle hole exchange channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {
                                val +=
                                        state_vec.vertx_xph(W, w_in, w, K, n_in, m, s1_in, s3, s4p, s2_out) *
                                        bubble_ph[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        state_vec.vertx_xph(W, w, w_out, K, mp, n_out, s4, s2_in, s1_out, s3p);
                            }
                            if ( (s3 == s3p) && (s4 == s4p) && (m == mp) ) {
                                // explicit large frequency contribution -- alternative to weight_vec
                                int w = POS_INT_RANGE + abs(W / 2);
                                val +=
                                        BETA * asympt_SGpGS_ph(W, Lam) * //( W==0 ) * BETA * Lam/4.0 *
                                        state_vec.vertx_xph(W, w_in, w, K, n_in, m, s1_in, s3, s4p, s2_out) *
                                        state_vec.vertx_xph(W, w, w_out, K, mp, n_out, s4, s2_in, s1_out, s3p);

                            }
                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;
    return val;
}

dcomplex rhs_t::eval_diag_tri_sc(const idx_tri_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_pp, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(ITRI::W);
    int w_in = idx(ITRI::w);

    int K = idx(ITRI::K);
    int n_in = idx(ITRI::n_in);
    int n_out = idx(ITRI::n_out);

    int s1_in = idx(ITRI::s1_in);
    int s2_in = idx(ITRI::s2_in);
    int s1_out = idx(ITRI::s1_out);
    int s2_out = idx(ITRI::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle particle channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {
                                val +=
                                        state_vec.tri_sc(W, w, K, n_in, m, s1_in, s2_in, s3p, s4p) *
                                        bubble_pp[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        state_vec.vertx_sc(W, w, w_in, K, mp, n_out, s3, s4, s1_out, s2_out);
                            }

                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    BETA * (s3 == s3p) * (s4 == s4p) * (m == mp) * asympt_SGpGS_pp(W, Lam) *
                                    state_vec.tri_sc(W, w, K, n_in, m, s1_in, s2_in, s3p, s4p) *
                                    state_vec.vertx_sc(W, w, w_in, K, mp, n_out, s3, s4, s1_out, s2_out);

                        }
    val *= 1.0 / BETA / 4.0 / PI / PI; //TODO: divide for FFACTOR_COUNT? 
    return val;
}

dcomplex rhs_t::eval_diag_tri_d(const idx_tri_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_ph, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(ITRI::W);
    int w_in = idx(ITRI::w);

    int K = idx(ITRI::K);
    int n_in = idx(ITRI::n_in);
    int n_out = idx(ITRI::n_out);

    int s1_in = idx(ITRI::s1_in);
    int s2_in = idx(ITRI::s2_in);
    int s1_out = idx(ITRI::s1_out);
    int s2_out = idx(ITRI::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle hole channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {

                                val +=

                                        -bubble_ph[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        state_vec.tri_d(W, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                        state_vec.vertx_d(W, w, w_in, K, mp, n_out, s4, s2_in, s3p, s2_out);
                            }
                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    -BETA * (s3 == s3p) * (s4 == s4p) * (m == mp) * asympt_SGpGS_ph(W, Lam) *
                                    state_vec.tri_d(W, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                    state_vec.vertx_d(W, w, w_in, K, mp, n_out, s4, s2_in, s3p, s2_out);
                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;

    return val;
}

dcomplex rhs_t::eval_diag_tri_m(const idx_tri_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_ph, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(ITRI::W);
    int w_in = idx(ITRI::w);

    int K = idx(ITRI::K);
    int n_in = idx(ITRI::n_in);
    int n_out = idx(ITRI::n_out);

    int s1_in = idx(ITRI::s1_in);
    int s2_in = idx(ITRI::s2_in);
    int s1_out = idx(ITRI::s1_out);
    int s2_out = idx(ITRI::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle hole channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {

                                val +=
                                        -bubble_ph[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        state_vec.tri_m(W, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                        state_vec.vertx_m(W, w, w_in, K, mp, n_out, s4, s2_in, s3p, s2_out);
                            }
                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    -BETA * (s3 == s3p) * (s4 == s4p) * (m == mp) * asympt_SGpGS_ph(W, Lam) *
                                    state_vec.tri_m(W, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                    state_vec.vertx_m(W, w, w_in, K, mp, n_out, s4, s2_in, s3p, s2_out);
                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;

    return val;
}

dcomplex rhs_t::eval_diag_susc_sc(const idx_susc_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_pp, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(ISUSC::W);

    int K = idx(ISUSC::K);
    int n_in = idx(ISUSC::n_in);
    int n_out = idx(ISUSC::n_out);

    int s1_in = idx(ISUSC::s1_in);
    int s2_in = idx(ISUSC::s2_in);
    int s1_out = idx(ISUSC::s1_out);
    int s2_out = idx(ISUSC::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle particle channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {
                                val +=
                                        state_vec.tri_sc(W, w, K, n_in, m, s1_in, s2_in, s3p, s4p) *
                                        bubble_pp[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        state_vec.tri_sc(W, w, K, mp, n_out, s3, s4, s1_out, s2_out);
                            }

                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    BETA * (s3 == s3p) * (s4 == s4p) * (m == mp) * asympt_SGpGS_pp(W, Lam) *
                                    state_vec.tri_sc(W, w, K, n_in, m, s1_in, s2_in, s3p, s4p) *
                                    state_vec.tri_sc(W, w, K, mp, n_out, s3, s4, s1_out, s2_out);

                        }
    val *= 1.0 / BETA / 4.0 / PI / PI; //TODO: divide for FFACTOR_COUNT? 
    return val;
}

dcomplex rhs_t::eval_diag_susc_d(const idx_susc_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_ph, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(ISUSC::W);

    int K = idx(ISUSC::K);
    int n_in = idx(ISUSC::n_in);
    int n_out = idx(ISUSC::n_out);

    int s1_in = idx(ISUSC::s1_in);
    int s2_in = idx(ISUSC::s2_in);
    int s1_out = idx(ISUSC::s1_out);
    int s2_out = idx(ISUSC::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle hole channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {

                                val +=
                                        -bubble_ph[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        state_vec.tri_d(W, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                        state_vec.tri_d(W, w, K, mp, n_out, s4, s2_in, s3p, s2_out);
                            }
                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    -BETA * (s3 == s3p) * (s4 == s4p) * (m == mp) * asympt_SGpGS_ph(W, Lam) *
                                    state_vec.tri_d(W, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                    state_vec.tri_d(W, w, K, mp, n_out, s4, s2_in, s3p, s2_out);
                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;

    return val;
}

dcomplex rhs_t::eval_diag_susc_m(const idx_susc_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_ph, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(ISUSC::W);

    int K = idx(ISUSC::K);
    int n_in = idx(ISUSC::n_in);
    int n_out = idx(ISUSC::n_out);

    int s1_in = idx(ISUSC::s1_in);
    int s2_in = idx(ISUSC::s2_in);
    int s1_out = idx(ISUSC::s1_out);
    int s2_out = idx(ISUSC::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle hole channel
                            for ( int w = -POS_INT_RANGE; w < POS_INT_RANGE; ++w ) {
                                val +=

                                        -bubble_ph[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        state_vec.tri_m(W, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                        state_vec.tri_m(W, w, K, mp, n_out, s4, s2_in, s3p, s2_out);
                            }
                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE;
                            val +=
                                    -BETA * (s3 == s3p) * (s4 == s4p) * (m == mp) * asympt_SGpGS_ph(W, Lam) *
                                    state_vec.tri_m(W, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                    state_vec.tri_m(W, w, K, mp, n_out, s4, s2_in, s3p, s2_out);
                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;

    return val;
}

dcomplex rhs_t::eval_diag_susc_postproc_sc(const idx_susc_postproc_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_GG_pp, double Lam) {
    dcomplex val(0.0, 0.0);

    // Introduce help variables
    int W = 0;
    int Kp = idx(ISUSC_POSTPROC::K), K;
    if ( Kp == 0 ) K = get_k_patch(0, 0);
    if ( Kp == 1 ) K = get_k_patch(0, MAX_KPOS);
    if ( Kp == 2 ) K = get_k_patch(MAX_KPOS, MAX_KPOS);
    int n_in = 0;
    int n_out = 0;
    int s1_in = 0;
    int s2_in = 0;
    int s1_out = 0;
    int s2_out = 0;
    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp )
                            for ( int w = -POS_INT_RANGE; w < POS_INT_RANGE; ++w )
                                for ( int wp = -POS_INT_RANGE; wp < POS_INT_RANGE; ++wp ) {
                                    val += 1. / BETA / BETA / 4.0 / PI / PI * // Two pairs of equivalent lines -> Factor 1/4
                                            bubble_GG_pp[W][w][n_in][mp][s4][s2_in][s3][s1_in](K) *
                                            state_vec.vertx_sc(W, w, wp, K, mp, m, s3, s4, s3p, s4p) *
                                            bubble_GG_pp[W][wp][m][n_out][s2_out][s4p][s1_out][s3p](K) *
                                            weight_vec_2d[w][wp];
                                }
    for ( int w = -POS_INT_RANGE; w < POS_INT_RANGE; ++w ) {
        val += 1.0 / BETA * //Two equivalent lines -> factor 1/2
                bubble_GG_pp[W][w][n_in][n_out][s2_out][s2_in][s1_out][s1_in](K) *
                weight_vec[w];
    }

    return val;
}

dcomplex rhs_t::eval_diag_susc_postproc_d(const idx_susc_postproc_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_GG_ph, double Lam) {
    dcomplex val(0.0, 0.0);

    // Introduce help variables
    int W = 0;
    int Kp = idx(ISUSC_POSTPROC::K), K;
    if ( Kp == 0 ) K = get_k_patch(0, 0);
    if ( Kp == 1 ) K = get_k_patch(0, MAX_KPOS);
    if ( Kp == 2 ) K = get_k_patch(MAX_KPOS, MAX_KPOS);
    int n_in = 0;
    int n_out = 0;
    int s1_in = 0;
    int s2_in = 0;
    int s1_out = 0;
    int s2_out = 0;
    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp )
                            for ( int w = -POS_INT_RANGE; w < POS_INT_RANGE; ++w )
                                for ( int wp = -POS_INT_RANGE; wp < POS_INT_RANGE; ++wp ) {
                                    //cout << "sum" << endl;  
                                    val += 1.0 / BETA / BETA / 4.0 / PI / PI * //Two internal loops
                                            bubble_GG_ph[W][w][n_in][mp][s4][s2_in][s3][s1_in](K) *
                                            state_vec.vertx_d(W, w, wp, K, mp, m, s3, s4, s3p, s4p) *
                                            bubble_GG_ph[W][wp][m][n_out][s2_out][s4p][s1_out][s3p](K) *
                                            weight_vec_2d[w][wp];
                                }
    for ( int w = -POS_INT_RANGE; w < POS_INT_RANGE; ++w ) {
        val -= 1 / BETA * // One internal loop
                bubble_GG_ph[W][w][n_in][n_out][s2_out][s2_in][s1_out][s1_in](K) *
                weight_vec[w];
    }

    return val;
}

dcomplex rhs_t::eval_diag_susc_postproc_m(const idx_susc_postproc_t& idx, const state_t& state_vec, const gf_bubble_mat_t& bubble_GG_ph, double Lam) {
    dcomplex val(0.0, 0.0);

    // Introduce help variables
    int W = 0;
    int Kp = idx(ISUSC_POSTPROC::K), K;
    if ( Kp == 0 ) K = get_k_patch(0, 0);
    if ( Kp == 1 ) K = get_k_patch(0, MAX_KPOS);
    if ( Kp == 2 ) K = get_k_patch(MAX_KPOS, MAX_KPOS);
    int n_in = 0;
    int n_out = 0;
    int s1_in = 0;
    int s2_in = 0;
    int s1_out = 0;
    int s2_out = 0;
    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp )
                            for ( int w = -POS_INT_RANGE; w < POS_INT_RANGE; ++w )
                                for ( int wp = -POS_INT_RANGE; wp < POS_INT_RANGE; ++wp ) {
                                    val += 1.0 / BETA / BETA / 4.0 / PI / PI *
                                            bubble_GG_ph[W][w][n_in][mp][s4][s2_in][s3][s1_in](K) *
                                            state_vec.vertx_m(W, w, wp, K, mp, m, s3, s4, s3p, s4p) *
                                            bubble_GG_ph[W][wp][m][n_out][s2_out][s4p][s1_out][s3p](K) *
                                            weight_vec_2d[w][wp];
                                }
    for ( int w = -POS_INT_RANGE; w < POS_INT_RANGE; ++w ) {
        val -= 1 / BETA * // One internal loop
                bubble_GG_ph[W][w][n_in][n_out][s2_out][s2_in][s1_out][s1_in](K) *
                weight_vec[w];
    }
    return val;
}


//*************************************************
//*************************************************
//*************************************************
//-------------------------------------------  Diagrams used in the multiloop implementation -----------------------------------------------------//

// ----- CENTRAL DIAGRAMS ----- //

dcomplex rhs_t::eval_diag_pp_C(const idx_phi_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_pp, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(IPHI::W);
    int w_in = idx(IPHI::w_in);
    int w_out = idx(IPHI::w_out);

    int K = idx(IPHI::K);
    int n_in = idx(IPHI::n_in);
    int n_out = idx(IPHI::n_out);

    int s1_in = idx(IPHI::s1_in);
    int s2_in = idx(IPHI::s2_in);
    int s1_out = idx(IPHI::s1_out);
    int s2_out = idx(IPHI::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle particle channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {
                                val +=
                                        bubble_GG_pp[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        (
                                        state_vec.vertx_pp(W, w_in, w, K, n_in, m, s1_in, s2_in, s3p, s4p) *
                                        dfdl.phi_pp_L(W, w, w_out, K, mp, n_out, s3, s4, s1_out, s2_out));
                            }

                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    BETA * (s3 == s3p) * (s4 == s4p) * asympt_GG_pp(W, Lam) *
                                    (
                                    state_vec.vertx_pp(W, w_in, w, K, n_in, m, s1_in, s2_in, s3p, s4p) *
                                    dfdl.phi_pp_L(W, w, w_out, K, mp, n_out, s3, s4, s1_out, s2_out));

                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;
    return val;
}

dcomplex rhs_t::eval_diag_ph_C(const idx_phi_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(IPHI::W);
    int w_in = idx(IPHI::w_in);
    int w_out = idx(IPHI::w_out);

    int K = idx(IPHI::K);
    int n_in = idx(IPHI::n_in);
    int n_out = idx(IPHI::n_out);

    int s1_in = idx(IPHI::s1_in);
    int s2_in = idx(IPHI::s2_in);
    int s1_out = idx(IPHI::s1_out);
    int s2_out = idx(IPHI::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle particle channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {
                                val +=
                                        -bubble_GG_ph[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        (
                                        2. * state_vec.vertx_ph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                        dfdl.phi_ph_L(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out) -
                                        state_vec.vertx_xph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                        dfdl.phi_ph_L(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out) -
                                        state_vec.vertx_ph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                        dfdl.phi_xph_L(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out));
                            }

                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    -BETA * (s3 == s3p) * (s4 == s4p) * asympt_GG_ph(W, Lam) *
                                    (
                                    2. * state_vec.vertx_ph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                    dfdl.phi_ph_L(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out) -
                                    state_vec.vertx_xph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                    dfdl.phi_ph_L(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out) -
                                    state_vec.vertx_ph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                    dfdl.phi_xph_L(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out));

                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;
    return val;
}

dcomplex rhs_t::eval_diag_xph_C(const idx_phi_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(IPHI::W);
    int w_in = idx(IPHI::w_in);
    int w_out = idx(IPHI::w_out);

    int K = idx(IPHI::K);
    int n_in = idx(IPHI::n_in);
    int n_out = idx(IPHI::n_out);

    int s1_in = idx(IPHI::s1_in);
    int s2_in = idx(IPHI::s2_in);
    int s1_out = idx(IPHI::s1_out);
    int s2_out = idx(IPHI::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle hole exchange channel, this way of connecting has no loop and no swap
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {
                                val +=
                                        bubble_GG_ph[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        (state_vec.vertx_xph(W, w_in, w, K, n_in, m, s1_in, s3, s4p, s2_out) *
                                        dfdl.phi_xph_L(W, w, w_out, K, mp, n_out, s4, s2_in, s1_out, s3p));
                            }

                            // explicit large frequency contribution -- alternative to weight_vec

                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    BETA * (s3 == s3p) * (s4 == s4p) * asympt_GG_ph(W, Lam) *
                                    (state_vec.vertx_xph(W, w_in, w, K, n_in, m, s1_in, s3, s4p, s2_out) *
                                    dfdl.phi_xph_L(W, w, w_out, K, mp, n_out, s4, s2_in, s1_out, s3p));
                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;
    return val;
}

dcomplex rhs_t::eval_diag_tri_sc_C(const idx_tri_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_pp, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(ITRI::W);
    int w_in = idx(ITRI::w);

    int K = idx(ITRI::K);
    int n_in = idx(ITRI::n_in);
    int n_out = idx(ITRI::n_out);

    int s1_in = idx(ITRI::s1_in);
    int s2_in = idx(ITRI::s2_in);
    int s1_out = idx(ITRI::s1_out);
    int s2_out = idx(ITRI::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle particle channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {
                                val +=
                                        state_vec.tri_sc(W, w, K, n_in, m, s1_in, s2_in, s3p, s4p) *
                                        bubble_GG_pp[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        dfdl.phi_pp_L(W, w, w_in, K, mp, n_out, s3, s4, s1_out, s2_out);
                            }

                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    BETA * (s3 == s3p) * (s4 == s4p) * (m == mp) * asympt_GG_pp(W, Lam) *
                                    state_vec.tri_sc(W, w, K, n_in, m, s1_in, s2_in, s3p, s4p) *
                                    dfdl.phi_pp_L(W, w, w_in, K, mp, n_out, s3, s4, s1_out, s2_out);

                        }
    val *= 1.0 / BETA / 4.0 / PI / PI; //TODO: divide for FFACTOR_COUNT? 
    return val;
}

dcomplex rhs_t::eval_diag_tri_d_C(const idx_tri_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(ITRI::W);
    int w_in = idx(ITRI::w);

    int K = idx(ITRI::K);
    int n_in = idx(ITRI::n_in);
    int n_out = idx(ITRI::n_out);

    int s1_in = idx(ITRI::s1_in);
    int s2_in = idx(ITRI::s2_in);
    int s1_out = idx(ITRI::s1_out);
    int s2_out = idx(ITRI::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle hole channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {

                                val +=

                                        -bubble_GG_ph[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        state_vec.tri_d(W, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                        dfdl.phi_d_L(W, w, w_in, K, mp, n_out, s4, s2_in, s3p, s2_out);
                            }
                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    -BETA * (s3 == s3p) * (s4 == s4p) * (m == mp) * asympt_GG_ph(W, Lam) *
                                    state_vec.tri_d(W, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                    dfdl.phi_d_L(W, w, w_in, K, mp, n_out, s4, s2_in, s3p, s2_out);
                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;

    return val;
}

dcomplex rhs_t::eval_diag_tri_m_C(const idx_tri_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(ITRI::W);
    int w_in = idx(ITRI::w);

    int K = idx(ITRI::K);
    int n_in = idx(ITRI::n_in);
    int n_out = idx(ITRI::n_out);

    int s1_in = idx(ITRI::s1_in);
    int s2_in = idx(ITRI::s2_in);
    int s1_out = idx(ITRI::s1_out);
    int s2_out = idx(ITRI::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle hole channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {

                                val +=
                                        -bubble_GG_ph[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        state_vec.tri_m(W, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                        dfdl.phi_m_L(W, w, w_in, K, mp, n_out, s4, s2_in, s3p, s2_out);
                            }
                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    -BETA * (s3 == s3p) * (s4 == s4p) * (m == mp) * asympt_GG_ph(W, Lam) *
                                    state_vec.tri_m(W, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                    dfdl.phi_m_L(W, w, w_in, K, mp, n_out, s4, s2_in, s3p, s2_out);
                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;

    return val;
}

dcomplex rhs_t::eval_diag_susc_sc_C(const idx_susc_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_pp, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(ISUSC::W);

    int K = idx(ISUSC::K);
    int n_in = idx(ISUSC::n_in);
    int n_out = idx(ISUSC::n_out);

    int s1_in = idx(ISUSC::s1_in);
    int s2_in = idx(ISUSC::s2_in);
    int s1_out = idx(ISUSC::s1_out);
    int s2_out = idx(ISUSC::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle particle channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {
                                val +=
                                        dfdl.tri_sc_R(W, w, K, n_in, m, s1_in, s2_in, s3p, s4p) *
                                        bubble_GG_pp[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        state_vec.tri_sc(W, w, K, mp, n_out, s3, s4, s1_out, s2_out);
                            }

                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    BETA * (s3 == s3p) * (s4 == s4p) * (m == mp) * asympt_GG_pp(W, Lam) *
                                    dfdl.tri_sc_R(W, w, K, n_in, m, s1_in, s2_in, s3p, s4p) *
                                    state_vec.tri_sc(W, w, K, mp, n_out, s3, s4, s1_out, s2_out);

                        }
    val *= 1.0 / BETA / 4.0 / PI / PI; //TODO: divide for FFACTOR_COUNT? 
    return val;
}

dcomplex rhs_t::eval_diag_susc_d_C(const idx_susc_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(ISUSC::W);

    int K = idx(ISUSC::K);
    int n_in = idx(ISUSC::n_in);
    int n_out = idx(ISUSC::n_out);

    int s1_in = idx(ISUSC::s1_in);
    int s2_in = idx(ISUSC::s2_in);
    int s1_out = idx(ISUSC::s1_out);
    int s2_out = idx(ISUSC::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle hole channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {

                                val +=

                                        -bubble_GG_ph[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        dfdl.tri_d_R(W, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                        state_vec.tri_d(W, w, K, mp, n_out, s4, s2_in, s3p, s2_out);
                            }
                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    -BETA * (s3 == s3p) * (s4 == s4p) * (m == mp) * asympt_GG_ph(W, Lam) *
                                    dfdl.tri_d_R(W, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                    state_vec.tri_d(W, w, K, mp, n_out, s4, s2_in, s3p, s2_out);
                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;

    return val;
}

dcomplex rhs_t::eval_diag_susc_m_C(const idx_susc_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(ISUSC::W);

    int K = idx(ISUSC::K);
    int n_in = idx(ISUSC::n_in);
    int n_out = idx(ISUSC::n_out);

    int s1_in = idx(ISUSC::s1_in);
    int s2_in = idx(ISUSC::s2_in);
    int s1_out = idx(ISUSC::s1_out);
    int s2_out = idx(ISUSC::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle hole channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {

                                val +=
                                        -bubble_GG_ph[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        dfdl.tri_m_R(W, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                        state_vec.tri_m(W, w, K, mp, n_out, s4, s2_in, s3p, s2_out);
                            }
                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    -BETA * (s3 == s3p) * (s4 == s4p) * (m == mp) * asympt_GG_ph(W, Lam) *
                                    dfdl.tri_m_R(W, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                    state_vec.tri_m(W, w, K, mp, n_out, s4, s2_in, s3p, s2_out);
                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;

    return val;
}


// ---------- RIGHT DIAGRAMS ----------//

dcomplex rhs_t::eval_diag_pp_R(const idx_phi_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_pp, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(IPHI::W);
    int w_in = idx(IPHI::w_in);
    int w_out = idx(IPHI::w_out);

    int K = idx(IPHI::K);
    int n_in = idx(IPHI::n_in);
    int n_out = idx(IPHI::n_out);

    int s1_in = idx(IPHI::s1_in);
    int s2_in = idx(IPHI::s2_in);
    int s1_out = idx(IPHI::s1_out);
    int s2_out = idx(IPHI::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle particle channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {
                                val +=
                                        bubble_GG_pp[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        (
                                        state_vec.vertx_pp(W, w_in, w, K, n_in, m, s1_in, s2_in, s3p, s4p) *
                                        dfdl.phi_ph_plus_xph(W, w, w_out, K, mp, n_out, s3, s4, s1_out, s2_out));
                            }

                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    BETA * (s3 == s3p) * (s4 == s4p) * asympt_GG_pp(W, Lam) *
                                    (
                                    state_vec.vertx_pp(W, w_in, w, K, n_in, m, s1_in, s2_in, s3p, s4p) *
                                    dfdl.phi_ph_plus_xph(W, w, w_out, K, mp, n_out, s3, s4, s1_out, s2_out));

                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;
    return val;
}

dcomplex rhs_t::eval_diag_ph_R(const idx_phi_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(IPHI::W);
    int w_in = idx(IPHI::w_in);
    int w_out = idx(IPHI::w_out);

    int K = idx(IPHI::K);
    int n_in = idx(IPHI::n_in);
    int n_out = idx(IPHI::n_out);

    int s1_in = idx(IPHI::s1_in);
    int s2_in = idx(IPHI::s2_in);
    int s1_out = idx(IPHI::s1_out);
    int s2_out = idx(IPHI::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle particle channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2); w < POS_INT_RANGE + abs(W / 2); ++w ) {
                                val +=
                                        -bubble_GG_ph[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        (
                                        2. * state_vec.vertx_ph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                        dfdl.phi_pp_plus_xph(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out) -
                                        state_vec.vertx_xph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                        dfdl.phi_pp_plus_xph(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out) -
                                        state_vec.vertx_ph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                        dfdl.phi_pp_plus_ph(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out));
                            }

                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    -BETA * (s3 == s3p) * (s4 == s4p) * asympt_GG_ph(W, Lam) *
                                    (
                                    2. * state_vec.vertx_ph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                    dfdl.phi_pp_plus_xph(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out) -
                                    state_vec.vertx_xph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                    dfdl.phi_pp_plus_xph(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out) -
                                    state_vec.vertx_ph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                    dfdl.phi_pp_plus_ph(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out));

                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;
    return val;
}

dcomplex rhs_t::eval_diag_xph_R(const idx_phi_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(IPHI::W);
    int w_in = idx(IPHI::w_in);
    int w_out = idx(IPHI::w_out);

    int K = idx(IPHI::K);
    int n_in = idx(IPHI::n_in);
    int n_out = idx(IPHI::n_out);

    int s1_in = idx(IPHI::s1_in);
    int s2_in = idx(IPHI::s2_in);
    int s1_out = idx(IPHI::s1_out);
    int s2_out = idx(IPHI::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle hole exchange channel, this way of connecting has no loop and no swap
                            for ( int w = -POS_INT_RANGE - abs(W / 2); w < POS_INT_RANGE + abs(W / 2); ++w ) {
                                val +=
                                        bubble_GG_ph[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        (state_vec.vertx_xph(W, w_in, w, K, n_in, m, s1_in, s3, s4p, s2_out) *
                                        dfdl.phi_pp_plus_ph(W, w, w_out, K, mp, n_out, s4, s2_in, s1_out, s3p));
                            }

                            // explicit large frequency contribution -- alternative to weight_vec

                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    BETA * (s3 == s3p) * (s4 == s4p) * asympt_GG_ph(W, Lam) *
                                    (state_vec.vertx_xph(W, w_in, w, K, n_in, m, s1_in, s3, s4p, s2_out) *
                                    dfdl.phi_pp_plus_ph(W, w, w_out, K, mp, n_out, s4, s2_in, s1_out, s3p));
                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;
    return val;
}

dcomplex rhs_t::eval_diag_tri_sc_R(const idx_tri_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_pp, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(ITRI::W);
    int w_in = idx(ITRI::w);

    int K = idx(ITRI::K);
    int n_in = idx(ITRI::n_in);
    int n_out = idx(ITRI::n_out);

    int s1_in = idx(ITRI::s1_in);
    int s2_in = idx(ITRI::s2_in);
    int s1_out = idx(ITRI::s1_out);
    int s2_out = idx(ITRI::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle particle channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {
                                val +=
                                        bubble_GG_pp[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        (
                                        state_vec.tri_sc(W, w, K, n_in, m, s1_in, s2_in, s3p, s4p) *
                                        dfdl.phi_ph_plus_xph(W, w, w_in, K, mp, n_out, s3, s4, s1_out, s2_out));
                            }

                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    BETA * (s3 == s3p) * (s4 == s4p) * asympt_GG_pp(W, Lam) *
                                    (
                                    state_vec.tri_sc(W, w, K, n_in, m, s1_in, s2_in, s3p, s4p) *
                                    dfdl.phi_ph_plus_xph(W, w, w_in, K, mp, n_out, s3, s4, s1_out, s2_out));

                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;
    return val;
}

dcomplex rhs_t::eval_diag_tri_d_R(const idx_tri_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(ITRI::W);
    int w_in = idx(ITRI::w);

    int K = idx(ITRI::K);
    int n_in = idx(ITRI::n_in);
    int n_out = idx(ITRI::n_out);

    int s1_in = idx(ITRI::s1_in);
    int s2_in = idx(ITRI::s2_in);
    int s1_out = idx(ITRI::s1_out);
    int s2_out = idx(ITRI::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle particle channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {
                                val -=
                                        bubble_GG_ph[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        (
                                        state_vec.tri_d(W, w, K, n_in, m, s1_in, s3, s4p, s2_out) *
                                        dfdl.phi_sc_plus_m(W, w, w_in, K, mp, n_out, s4, s2_in, s1_out, s3p));
                            }

                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val -=
                                    BETA * (s3 == s3p) * (s4 == s4p) * asympt_GG_ph(W, Lam) *
                                    (
                                    state_vec.tri_d(W, w, K, n_in, m, s1_in, s3, s4p, s2_out) *
                                    dfdl.phi_sc_plus_m(W, w, w_in, K, mp, n_out, s4, s2_in, s1_out, s3p));

                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;
    return val;
}

dcomplex rhs_t::eval_diag_tri_m_R(const idx_tri_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(ITRI::W);
    int w_in = idx(ITRI::w);

    int K = idx(ITRI::K);
    int n_in = idx(ITRI::n_in);
    int n_out = idx(ITRI::n_out);

    int s1_in = idx(ITRI::s1_in);
    int s2_in = idx(ITRI::s2_in);
    int s1_out = idx(ITRI::s1_out);
    int s2_out = idx(ITRI::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle particle channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {
                                val -=
                                        bubble_GG_ph[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        (
                                        state_vec.tri_m(W, w, K, n_in, m, s1_in, s3, s4p, s2_out) *
                                        dfdl.phi_sc_plus_d(W, w, w_in, K, mp, n_out, s4, s2_in, s1_out, s3p));
                            }

                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val -=
                                    BETA * (s3 == s3p) * (s4 == s4p) * asympt_GG_ph(W, Lam) *
                                    (
                                    state_vec.tri_m(W, w, K, n_in, m, s1_in, s3, s4p, s2_out) *
                                    dfdl.phi_sc_plus_d(W, w, w_in, K, mp, n_out, s4, s2_in, s1_out, s3p));

                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;
    return val;
}


// --------- LEFT DIAGRAMS ------------//

dcomplex rhs_t::eval_diag_pp_L(const idx_phi_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_pp, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(IPHI::W);
    int w_in = idx(IPHI::w_in);
    int w_out = idx(IPHI::w_out);

    int K = idx(IPHI::K);
    int n_in = idx(IPHI::n_in);
    int n_out = idx(IPHI::n_out);

    int s1_in = idx(IPHI::s1_in);
    int s2_in = idx(IPHI::s2_in);
    int s1_out = idx(IPHI::s1_out);
    int s2_out = idx(IPHI::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle particle channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2) - (W + 100000) % 2; w < POS_INT_RANGE + abs(W / 2); ++w ) {
                                val +=
                                        bubble_GG_pp[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        (
                                        dfdl.phi_ph_plus_xph(W, w_in, w, K, n_in, m, s1_in, s2_in, s3p, s4p) *
                                        state_vec.vertx_pp(W, w, w_out, K, mp, n_out, s3, s4, s1_out, s2_out)
                                        );
                            }

                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    BETA * (s3 == s3p) * (s4 == s4p) * asympt_GG_pp(W, Lam) *
                                    (
                                    dfdl.phi_ph_plus_xph(W, w_in, w, K, n_in, m, s1_in, s2_in, s3p, s4p) *
                                    state_vec.vertx_pp(W, w, w_out, K, mp, n_out, s3, s4, s1_out, s2_out)
                                    );

                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;
    return val;
}

dcomplex rhs_t::eval_diag_ph_L(const idx_phi_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(IPHI::W);
    int w_in = idx(IPHI::w_in);
    int w_out = idx(IPHI::w_out);

    int K = idx(IPHI::K);
    int n_in = idx(IPHI::n_in);
    int n_out = idx(IPHI::n_out);

    int s1_in = idx(IPHI::s1_in);
    int s2_in = idx(IPHI::s2_in);
    int s1_out = idx(IPHI::s1_out);
    int s2_out = idx(IPHI::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle particle channel
                            for ( int w = -POS_INT_RANGE - abs(W / 2); w < POS_INT_RANGE + abs(W / 2); ++w ) {
                                val +=
                                        -bubble_GG_ph[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        (
                                        2. * dfdl.phi_pp_plus_xph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                        state_vec.vertx_ph(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out) -
                                        dfdl.phi_pp_plus_xph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                        state_vec.vertx_xph(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out) -
                                        dfdl.phi_pp_plus_ph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                        state_vec.vertx_ph(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out));
                            }

                            // explicit large frequency contribution -- alternative to weight_vec
                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    -BETA * (s3 == s3p) * (s4 == s4p) * asympt_GG_ph(W, Lam) *
                                    (
                                    2. * dfdl.phi_pp_plus_xph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                    state_vec.vertx_ph(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out) -
                                    dfdl.phi_pp_plus_xph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                    state_vec.vertx_xph(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out) -
                                    dfdl.phi_pp_plus_ph(W, w_in, w, K, n_in, m, s1_in, s3, s1_out, s4p) *
                                    state_vec.vertx_ph(W, w, w_out, K, mp, n_out, s4, s2_in, s3p, s2_out));

                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;
    return val;
}

dcomplex rhs_t::eval_diag_xph_L(const idx_phi_t& idx, const state_t& state_vec, const state_t& dfdl, const gf_bubble_mat_t& bubble_GG_ph, double Lam) {
    dcomplex val(0.0, 0.0);

#ifdef FORCED_ZEROS
    if ( forced_zero_check(idx) ) // check wether element should be forced to zero
        return val;
#endif

    // Introduce help variables
    int W = idx(IPHI::W);
    int w_in = idx(IPHI::w_in);
    int w_out = idx(IPHI::w_out);

    int K = idx(IPHI::K);
    int n_in = idx(IPHI::n_in);
    int n_out = idx(IPHI::n_out);

    int s1_in = idx(IPHI::s1_in);
    int s2_in = idx(IPHI::s2_in);
    int s1_out = idx(IPHI::s1_out);
    int s2_out = idx(IPHI::s2_out);

    for ( int s3 = 0; s3 < QN_COUNT; ++s3 )
        for ( int s3p = 0; s3p < QN_COUNT; ++s3p )
            for ( int s4 = 0; s4 < QN_COUNT; ++s4 )
                for ( int s4p = 0; s4p < QN_COUNT; ++s4p )
                    for ( int m = 0; m < FFACTOR_COUNT; ++m )
                        for ( int mp = 0; mp < FFACTOR_COUNT; ++mp ) {
                            // Particle hole exchange channel, this way of connecting has no loop and no swap
                            for ( int w = -POS_INT_RANGE - abs(W / 2); w < POS_INT_RANGE + abs(W / 2); ++w ) {
                                val += bubble_GG_ph[W][w][m][mp][s4][s4p][s3][s3p](K) *
                                        (dfdl.phi_pp_plus_ph(W, w_in, w, K, n_in, m, s1_in, s3, s4p, s2_out) *
                                        state_vec.vertx_xph(W, w, w_out, K, mp, n_out, s4, s2_in, s1_out, s3p));
                            }

                            // explicit large frequency contribution -- alternative to weight_vec

                            int w = POS_INT_RANGE + abs(W / 2);
                            val +=
                                    BETA * (s3 == s3p) * (s4 == s4p) * asympt_GG_ph(W, Lam) *
                                    (
                                    dfdl.phi_pp_plus_ph(W, w_in, w, K, n_in, m, s1_in, s3, s4p, s2_out) *
                                    state_vec.vertx_xph(W, w, w_out, K, mp, n_out, s4, s2_in, s1_out, s3p));
                        }

    val *= 1.0 / BETA / 4.0 / PI / PI;
    return val;
}

